#!/usr/bin/python3

import re
from setuptools import setup

with open("requirements.txt") as fp:
    requirements = [l.strip() for l in fp if not re.match("\s*(#|$)", l)]


setup(
    name        = "ctproxy",
    author      = "Anthony Baire",
    author_email= "Anthony.Baire@irisa.fr",
    url         = "https://gitlab.inria.fr/inria-ci/ctproxy/",
    license     = "GNU AGPL3+",
    description = "container tuning proxy",
    packages    = ["ctproxy", "ctproxy.plugins"],
    package_data= {"ctproxy": ["data/install-ca.sh"]},
    keywords    = ["docker", "proxy"],
    classifiers = [
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: GNU Affero General Public License v3",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3.6",
        "Topic :: Internet :: Proxy Servers",
        "Topic :: Internet :: WWW/HTTP",
        ],

    install_requires = requirements,
)

