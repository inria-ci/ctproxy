#!/bin/sh

CN="ctproxy cache CA `date -Is`"
TARGET="/etc/nginx/cache-ca"
WEB_CA_PATH="/etc/nginx/html/cache/ca.crt"
DAYS=14

set -e -x

mkdir -p "`dirname "$TARGET"`"

# generate the new key
certtool --generate-privkey --outfile "$TARGET.key.new"

# make the crt config
#FIXME: should create the CERT a little in the past (to avoid problems with dirfting clocks)
cat >"$TARGET.cfg" <<EOF
cn = "$CN"
expiration_days = $DAYS
ca
cert_signing_key
EOF

# generate the new CA certificate
certtool --generate-self-signed				\
	--verbose \
	--outfile		"$TARGET.crt.new"	\
	--load-privkey		"$TARGET.key.new"	\
	--template		"$TARGET.cfg"


if [ -f "$TARGET.key" ] && [ -f "$TARGET.crt" ]
then
	# generate an intermediate certificate for the clients using the old CA
	certtool --generate-certificate				\
		--verbose \
		--load-ca-certificate	"$TARGET.crt"		\
		--load-ca-privkey	"$TARGET.key"		\
		--outfile		"$TARGET.int.new"	\
		--load-privkey		"$TARGET.key.new"	\
		--template		"$TARGET.cfg"

else
	# make an empty file
	cat >"$TARGET.int.new" </dev/null
fi

rm -- "$TARGET.cfg"


# install the new CA
mv "$TARGET.key.new" "$TARGET.key"
mv "$TARGET.crt.new" "$TARGET.crt"
mv "$TARGET.int.new" "$TARGET.int"

cp "$TARGET.crt"      "$WEB_CA_PATH.new"
mv "$WEB_CA_PATH.new" "$WEB_CA_PATH"

# generate a new server cert
/usr/local/sbin/renew_cache_server_cert

