# vim:ft=python:

import asyncio

from . import connector

class BaseConnector:
    ...

class UnixConnector(BaseConnector):
    def __init__(self, path: str) -> None:
        ...

class ClientResponse:
    status: int
    reason: str
    content_type: str
    charset: str
    content: asyncio.StreamReader
    headers: Mapping[str, str]  # FIXME: multidict
    connection: connector.Connection

    async def __aenter__(self) -> ClientResponse:
        ...
    async def __aexit__(self, *k: object) -> None:
        ...
    async def read(self) -> bytes:
        ...
    async def text(self) -> str:
        ...
    async def json(self) -> object:
        ...
    def raise_for_status(self) -> None:
        ...

class ClientSession:
    def __init__(self, *, connector: BaseConnector) -> None:
        ...

    def request(self, method: str, url: str, **kw) -> ClientResponse:
        ...

    def get(self, url: str, **kw) -> ClientResponse:
        ...

    async def close(self) -> None:
        ...

