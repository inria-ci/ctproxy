# vim:ft=python:

import aiohttp
import asyncio
import logging
import multidict
from typing import Awaitable, Callable, Mapping, Optional

import aiohttp.web_server
import yarl

class Request:
#    headers:    multidict.CIMultiDictProxy
#    match_info: multidict.CIMultiDictProxy
    headers:    Mapping[str, str]
    match_info: Mapping[str, str]
    method: str
    scheme: str
    host: str
    path: str
    url: yarl.URL
    rel_url: yarl.URL
    transport: asyncio.Transport

    async def read(self) -> bytes:
        ...
    async def text(self) -> str:
        ...
    async def json(self) -> object:
        ...


class StreamResponse:
    status: int
    content_type: str
    charset: str
    headers: MutableMapping[str, str]

    def __init__(self, *,
            status: Optional[int] = None,
            headers: Optional[dict] = None,
            ) -> None:
        ...

    def enable_chunked_encoding(self) -> None:
        ...

    async def prepare(self, request: Request) -> None:
        ...

    async def write(self, data: bytes) -> None:
        ...

    async def drain(self) -> None:
        ...

class Response(StreamResponse):
    def __init__(self, *,
            status: Optional[int] = None,
            headers: Optional[dict] = None,
            body: Optional[bytes] = None,
            ) -> None:
        ...

class UrlDispatcher:
    def add_route(self, method: str, path: str,
            handler: Callable[[Request], Awaitable[StreamResponse]]) -> None:
        ...

class Application:
    router: UrlDispatcher

    def __init__(self, *, loop: asyncio.AbstractEventLoop = None) -> None:
        ...
    def make_handler(self, *,
            access_log: Optional[logging.Logger] = None,
            access_log_format: Optional[str] = None,
            ) -> Server:
        ...
    async def shutdown(self) -> None:
        ...
    async def cleanup(self) -> None:
        ...

class Server:
    def __call__(self) -> aiohttp.web_server.RequestHandler:
        ...
    async def shutdown(self) -> None:
        ...

def json_response(data: object = None, *,
            text: Optional[str] = None,
            status: Optional[int] = None,
            headers: Optional[dict] = None,
            ) -> Response:
    ...


