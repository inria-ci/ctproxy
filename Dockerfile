FROM ubuntu:18.04

# configure apt source
#  -> disable multiverse and backports
RUN sed -i '/multiverse/d;/backports/d' /etc/apt/sources.list	&&\
	apt-get -q update

RUN apt-get -qy install		\
	bsdtar			\
	docker.io		\
	gnutls-bin		\
	moreutils		\
	nginx-light		\
	libnginx-mod-http-headers-more-filter	\
	libgnutls28-dev		\
	python3			\
	python3-cachetools	\
	python3-cffi		\
	python3-docker		\
	python3-flake8		\
	python3-lxml		\
	python3-mypy		\
	python3-nose2		\
	python3-nose2-cov	\
	python3-pip		\
	python3-requests	\
	python3-sdnotify	\
	python3-setuptools	\
	python3-yaml		\
	socat			\
	ruby

COPY requirements.txt /tmp/
RUN pip3 install -r /tmp/requirements.txt

# FIXME: should be in requirements.txt instead
RUN pip3 install git+https://gitlab+deploy-token-75:VJGMz2J-q6s1iXVTjdZy@gitlab.inria.fr/abaire/aio_gnutls_transport
RUN pip3 install git+https://gitlab.inria.fr/abaire/config_reader.git@0.1.2

COPY certs/cache-server.crt /usr/local/share/ca-certificates/

RUN mkdir /etc/ctproxy && ln -s /opt/src/certs /etc/ctproxy/ &&\
	umask 0000 && touch /etc/ctproxy/config.yml  /etc/ctproxy/cache.yml&&\
	ln -s /opt/src/modules/cidocker/files/renew_cache_ca /opt/src/modules/cidocker/files/renew_cache_server_cert /usr/local/sbin/ &&\
        ln -s /opt/src/modules/cidocker/files/nginx-ssl.conf /etc/nginx/ &&\
	update-ca-certificates


WORKDIR /opt/src
