#!/usr/bin/python3

from   concurrent import futures
from   contextlib import contextmanager
import io
import json
import re
import shlex
import socket
import tarfile
import textwrap
import threading
import time
from   typing import Iterable, List, Optional, Sequence
import unittest
from   unittest import mock, skipUnless

import docker
from   docker.errors import APIError, NotFound

from .test_common import (log, docker_build, docker_subprocess, docker_tmp_rmi,
        IntegrationTestMixin, ENABLE_LONG_TESTS)

DOCKERHOST    = "dockerhost"
DOCKERHOST_IP = socket.gethostbyname(DOCKERHOST)

INTERCEPTED_DOMAINS="""
        deb.debian.org
        security.debian.org
        cdn-fastly.deb.debian.org
        cdn-aws.deb.debian.org
        security-cdn.debian.org
        repo.maven.apache.org
        repo.spring.io
""".split()

PREFIX      = "test_ctproxy_cache_"
LABEL       = "test.ctproxy.cache"
CACHE_VAR   = "TEST_CTPROXY_CACHE"

def extra_hosts(*, domains=None, ip=None):
    if domains is None:
        domains = INTERCEPTED_DOMAINS
    if ip is None:
        ip = DOCKERHOST_IP
    return [f"{d}:{ip}" for d in domains]


def test_cache_script(impl: str) -> str:
    """Return a script for testing if multiple http:// urls are cached or not
    
    'impl' is the implementation ('python3' or 'wget')

    The returned script shall be called with one or more http/https urls, eg:
        SCRIPT http://some.where https://some.where.else ...

    For each url, it sends a HTTP GET url and returns a line with the content
    of the 'X-Proxy-Cache' header or 'None' if this url is not present.
    """
    if impl=="python3":
        script = """\
            #!/usr/bin/env python3
            import sys, urllib.request
            for url in sys.argv[1:]:
                print(urllib.request.urlopen(url).headers.get("X-Proxy-Cache"))
        """
    elif impl=="wget":
        script = """\
            #!/bin/sh
            for url in $@ ; do 
                out="`wget -O /dev/null -S "$url" 2>&1`"
                if [ $? -ne 0 ] ; then
                    echo "error: failed to get $url"
                    echo "$out" >&2
                    exit 1
                else
                    status="`echo "$out" | sed '/^  X-Proxy-Cache:/!d ; s/^[^:]*: *//'`"
                    if [ -z "$status" ] ; then status=None ; fi
                    echo "$status"
                fi
            done
            """
    else:
        raise AssertionError(f"unknown impl {impl!r}")
    return textwrap.dedent(script)

def test_cache_command(impl: str) -> List[str]:
    """Return a command for testing if multiple http:// urls are cached or not

    see test_cache_script()
    """
    script = test_cache_script(impl)
    if impl == "python3":
        return ["python3", "-c", script]
    elif impl == "wget":
        return ["/bin/sh", "-c", script, "DUMMY_ARGV0"]

    raise AssertionError("not reachable") # pragma: nocover


def make_build_context(dockerfile: str, *, compress: Optional[str],
        prefix: str) -> io.BytesIO:
    """Assemble a context the docker builder

    return a tarfile suitable for 'docker build'

    'dockerfile'    content of the dockerfile
    'compress'      compression algorithm for the generated context
                    ('gzip', 'bzip2', 'xz' or None)
    'prefix'        string to be prepended before all file names
    """
    buf = io.BytesIO()
    tar = tarfile.open(fileobj=buf,
            mode=("w" if compress is None else ("w:"+compress)))

    files = {
            prefix+"Dockerfile": dockerfile.encode(),
            prefix+"test_cache_python3": test_cache_script("python3").encode(),
            prefix+"test_cache_wget":    test_cache_script("wget").encode(),
            }
    for path, content in files.items():
        info = tarfile.TarInfo(path)
        info.mode = 0o755
        info.size = len(content)
        tar.addfile(info, io.BytesIO(content))

    tar.close()
    buf.seek(0)
    return buf


class CacherTestCase(IntegrationTestMixin, unittest.TestCase):
    integration_config = "tests/config-cacher.yml"
    integration_images = "python:3-alpine", "python:3-slim", "busybox:latest"

    @classmethod
    def setUpClass(cls):
        cls.client1 = docker.APIClient("tcp://localhost:2375")
        super().setUpClass()
        try:
            cls.purge_images()
        except:
            super().tearDownClass()
            raise

    @classmethod
    def tearDownClass(cls):
        cls.client1.close()
        del cls.client1
        try:
            cls.purge_images()
        finally:
            super().tearDownClass()

    def setUp(self):
        super().setUp()
        # FIXME: race condition (we need to wait until the DockerCacher gets
        #        its configuration from the cache server)
        time.sleep(.1)

    def tearDown(self):
        super().tearDown()
        self.purge_images()

    @classmethod
    def purge_images(cls):
        # remove all DockerCacher images created by this test
        for img in cls.client.images(filters={"label": f"{LABEL}.digest"}):
            tags = img["RepoTags"]
            assert not tags or tags==['<none>:<none>'] or tags[0].startswith(PREFIX)
            cls.client.remove_image(img["Id"])

    def create_container(self, image="python:3-alpine", impl="python3",
            urls=["http://deb.debian.org", "https://deb.debian.org"], **kw):
        """Create a container and inspect it immediately

        `image`     docker images 
        `urls`      list of urls to be queried
        `impl`      implementation to be used ('python3' or 'wget')
                    Note: the minimal wget does not support the ca-certificates
                    store (thus not suitable for https:// tests)
        `**kw`      additional parameters for create_container()

        The container will issue a python3 command to get the resources listed
        in `urls` and output the cache status (HIT, MISS,...) for each request.
        A cache status of 'None' suggests that the request did not go through
        the cache.
        
        return the result of .inspect_container()
        """
        kw.setdefault("command", test_cache_command(impl) + list(urls))

        cid = self.client1.create_container(image, **kw)["Id"]
        self.add_cleanup("container", cid)

        return self.client1.inspect_container(cid)

    def check_container(self, ctr: dict, expect=True):
        """Run a container and check its cache status

        This command runs container created with self.create_container() and
        checks its output against the value expected in `expect`.

        The container is expected to output a cache_status line (HIT, MISS,
        REVALIDATE, None, ...) for each request it makes. A `None` line is
        assumed to indicate that the request did not get through the cache.

        `ctr`   json struct identyfing the container (as returned by
                cls.create_container())
        `expect` may contain:
                - True      -> all requests are expected to be cached
                - False     -> all requests are expected not to be cached
                - a list of bool indicating, for each request, whether it is
                  expected to be cached

        """
        cid = ctr["Id"]
        client = self.client1

        # run the container
        client.start(cid)
        returncode = client.wait(cid, timeout=2)
        output = client.logs(cid)

        # container must exit with code 0
        self.assertEqual(returncode, 0,
                msg="container failed with output:\n%s"
                % textwrap.indent( output.decode(errors="replace"), "\t"))

        # container output must match the value in `expect`
        result = [r != "None" for r in output.decode().split()]
        if isinstance(expect, bool):
            expect = len(result) * [expect]

        self.assertSequenceEqual(result, expect,
                msg="raw output is: %s" % output)


    def build(self, dockerfile:str, *, compress: Optional[str] = None,
            prefix: str = "", **kw) -> List[str]:
        """Build a docker image

        'dockerfile', 'compress' and 'prefix' are passed to
        make_build_context()
        
        **kw may provide additional arguments to docker_build()
        
        Return a list of image ids
        """

        image_ids = docker_build(self.client1, rm=True, forcerm=True,
                custom_context=True, fileobj=make_build_context(
                    dockerfile, compress=compress, prefix=prefix),
                **kw)
        for iid in image_ids:
            self.add_cleanup("image", iid)
        return image_ids

    @contextmanager
    def assertDaemonEvents(self, pull: Optional[Sequence[str]] = None,
            patch: Optional[Sequence[str]] = None):
        """Return a context manager to monitor events on the docker daemon

        pull:  sequence of images to be pulled
        patch: sequence of images to be patched

        All images must have an explicit tag (REPOSITORY:TAG)
        """

        # list of tuples (orig_name, orig_id or None if pulled)
        patch_lst = []
        # list of images names
        pull_lst = []
        close = False
        def monitor_func():
            for ev in events:
                if close:
                    return
                if ev["status"] == "commit":
                    attr = ev['Actor']['Attributes']
                    parent_id = attr["image"]
                    tag = attr["imageRef"]
                    if tag.startswith(PREFIX):
                        orig = tag[len(PREFIX)+1:]
                        patch_lst.append((orig, parent_id))
                elif ev["status"] == "pull":
                    tag = ev["id"]
                    pull_lst.append(tag)

        if patch is not None:
            expected_patch_lst = [ (img, self.client.inspect_image(img)["Id"])
                    for img in patch]

        events = self.client.events(decode=True,
                # note: 'delete' is monitored so as to have a cancellation
                # point during tearDown
                filters={"event": ["commit", "pull", "delete"]})
        thread = threading.Thread(target=monitor_func, daemon=True)
        thread.start()
        try:
            yield
            if patch is not None:
                self.assertSequenceEqual(expected_patch_lst, patch_lst)
            if pull is not None:
                self.assertSequenceEqual(pull, pull_lst)
        finally:
            close = True

    def test_100_cache_basic(self):
        ctr = self.create_container()
        self.assertSetEqual(set(ctr["HostConfig"]["ExtraHosts"]), set(extra_hosts()))
        self.check_container(ctr)

        ctr = self.create_container(urls=["http://deb.debian.org/"], impl="wget")
        self.assertSetEqual(set(ctr["HostConfig"]["ExtraHosts"]), set(extra_hosts()))
        self.check_container(ctr)

    def test_101_cache_multiple_hosts(self):
        ctr = self.create_container(
                urls=[
                    # cached
                    "http://deb.debian.org",
                    "https://deb.debian.org",
                    # not cached
                    "http://www.debian.org/",
                    "https://www.debian.org/",
                    ])
        self.check_container(ctr, expect=[True, True, False, False])

    def test_102_nocache(self):
        ctr = self.create_container(environment=[f"{CACHE_VAR}=no"])
        self.check_container(ctr, expect=False)
        ctr = self.create_container(environment=[f"{CACHE_VAR}=0"])
        self.check_container(ctr, expect=False)

    def test_103_unsupported_image(self):
        with self.assertDaemonLogs([
            'WARNING:ctproxy:ctproxy DockerCacher script failed because'
            ' image is not supported: busybox:latest\n']):
            ctr = self.create_container(image="busybox", impl="wget")
        self.check_container(ctr, expect=False)

    def test_103a_ca_install_error(self):
        image_id, = docker_build(self.client, fileobj=io.BytesIO(b"""
            FROM busybox
            RUN     touch /etc/debian_version &&\\
                    echo 'error foo foo;exit 28' > /bin/update-ca-certificates &&\\
                    chmod 755 /bin/update-ca-certificates
        """))
        self.add_cleanup("image", image_id)
        with self.assertDaemonLogs([
            'ERROR:ctproxy:ctproxy DockerCacher script error for image'
            f' {image_id} (exit 28)\n',
            '\tpatch ca-certificates\n', '\n',
            '\t/bin/update-ca-certificates: line 1: error: not found\n']):
            ctr = self.create_container(image=image_id, impl="wget")

    def test_104_rmi(self):
        image  = "test_ctproxy_python3_alpine:latest"
        cached = f"{PREFIX}_{image}"
        try:
            self.client1.tag("python:3-alpine", image)

            ctr = self.create_container(image)
            img = self.client1.inspect_image(ctr["Image"])
            self.assertEqual(img["RepoTags"], [cached])

            alp = self.client1.inspect_image(image)
            self.assertEqual(img["Parent"], alp["Id"])

        finally:
            self.client1.remove_image(image, force=True)

        self.assertRaises(NotFound, self.client1.inspect_image, image)
        self.assertRaises(NotFound, self.client1.inspect_image, cached)


    def test_105_already_patched(self):
        image  = "test_ctproxy_already_cached:latest"
        alpine = "python:3-alpine"
        cached_image  = f"{PREFIX}_{image}"
        cached_alpine = f"{PREFIX}_{alpine}"

        alp = self.create_container(alpine)
        try:
            self.client1.tag(alp["Image"], image)

            ctr = self.create_container(image)
            self.assertEqual(ctr["Image"], alp["Image"])

            img = self.client1.inspect_image(image)
            self.assertSetEqual(set(img["RepoTags"]),
                    set([image, cached_alpine, cached_image]))

        finally:
            self.client1.remove_image(image, force=True)


    def test_200_build_cache_enabled(self):
        with self.assertDaemonEvents(pull=[],
                patch=['python:3-alpine', 'python:3-slim']):
            self.build("""
        FROM python:3-alpine
        COPY test_cache_python3 /bin/test_cache
        RUN set -x -e ;\\
            test "`test_cache http://deb.debian.org/`"  != None; \\
            test "`test_cache https://deb.debian.org/`" != None; \\
            test "`test_cache http://www.debian.org/`"  =  None; \\
            test "`test_cache https://www.debian.org/`" =  None;

        FROM python:3-slim
        COPY test_cache_python3 /bin/test_cache
        RUN set -x -e ;\\
            test "`test_cache http://deb.debian.org/`"  != None; \\
            test "`test_cache https://deb.debian.org/`" != None; \\
            test "`test_cache http://www.debian.org/`"  =  None; \\
            test "`test_cache https://www.debian.org/`" =  None;
        """)


    def test_201_build_cache_disabled(self):
        "ensure cache is enabled only if all base images support caching"

        with self.assertDaemonEvents(pull=[], patch=["python:3-alpine"]):
            self.build("""
        FROM python:3-alpine
        COPY test_cache_python3 /bin/test_cache
        RUN set -x -e ;\\
            test "`test_cache http://deb.debian.org/`"  = None; \\
            test "`test_cache https://deb.debian.org/`" = None; \\
            test "`test_cache http://www.debian.org/`"  = None; \\
            test "`test_cache https://www.debian.org/`" = None;

        FROM busybox
        COPY test_cache_wget /bin/test_cache
        RUN set -x -e ;\\
            test "`test_cache http://deb.debian.org/`"  = None; \\
            test "`test_cache https://deb.debian.org/`" = None; \\
            test "`test_cache http://www.debian.org/`"  = None; \\
            test "`test_cache https://www.debian.org/`" = None;

        FROM python:3-slim
        COPY test_cache_python3 /bin/test_cache
        RUN set -x -e ;\\
            test "`test_cache http://deb.debian.org/`"  = None; \\
            test "`test_cache https://deb.debian.org/`" = None; \\
            test "`test_cache http://www.debian.org/`"  = None; \\
            test "`test_cache https://www.debian.org/`" = None;
        """)

    def test_202_build_force_pull(self):

        with self.assertDaemonEvents(
                pull=['python:3-alpine', 'python:3-slim'],
                patch=['python:3-alpine', 'python:3-slim']):
            self.build("""
        FROM python:3-alpine
        COPY test_cache_python3 /bin/test_cache
        RUN set -x -e ;\\
            test "`test_cache http://deb.debian.org/`"  != None; \\
            test "`test_cache https://deb.debian.org/`" != None; \\
            test "`test_cache http://www.debian.org/`"  = None; \\
            test "`test_cache https://www.debian.org/`" = None;

        FROM python:3-slim
        COPY test_cache_python3 /bin/test_cache
        RUN set -x -e ;\\
            test "`test_cache http://deb.debian.org/`"  != None; \\
            test "`test_cache https://deb.debian.org/`" != None; \\
            test "`test_cache http://www.debian.org/`"  = None; \\
            test "`test_cache https://www.debian.org/`" = None;
        """, pull=True)

    def test_202b_build_force_pull_with_nocache(self):

        with self.assertDaemonEvents(
                pull=['python:3-alpine', 'busybox:latest', 'python:3-slim'],
                patch=['python:3-alpine']):
            self.build("""
        FROM python:3-alpine
        COPY test_cache_python3 /bin/test_cache
        RUN set -x -e ;\\
            test "`test_cache http://deb.debian.org/`"  = None; \\
            test "`test_cache https://deb.debian.org/`" = None;

        FROM busybox
        COPY test_cache_wget /bin/test_cache
        RUN set -x -e ;\\
            test "`test_cache http://deb.debian.org/`"  = None; \\
            test "`test_cache https://deb.debian.org/`" = None;

        FROM python:3-slim
        COPY test_cache_python3 /bin/test_cache
        RUN set -x -e ;\\
            test "`test_cache http://deb.debian.org/`"  = None; \\
            test "`test_cache https://deb.debian.org/`" = None;
        """, pull=True)

    def test_203_build_pull_once(self):

        with self.assertDaemonEvents(
                pull=['python:3-alpine'],
                patch=['python:3-alpine']):
            self.build("""
        FROM python:3-alpine
        COPY test_cache_python3 /bin/test_cache
        RUN set -x -e ;\\
            test "`test_cache http://deb.debian.org/`"  != None; \\
            test "`test_cache https://deb.debian.org/`" != None; \\
            test "`test_cache http://www.debian.org/`"  = None; \\
            test "`test_cache https://www.debian.org/`" = None;

        FROM python:3-alpine
        COPY test_cache_python3 /bin/test_cache
        RUN set -x -e ;\\
            test "`test_cache http://deb.debian.org/`"  != None; \\
            test "`test_cache https://deb.debian.org/`" != None; \\
            test "`test_cache http://www.debian.org/`"  = None; \\
            test "`test_cache https://www.debian.org/`" = None;
        """, pull=True)

    def test_204_build_pull_if_needed(self):
        with self.assertDaemonEvents(
                    pull=['python:3-slim'],
                    patch=['python:3-alpine', 'python:3-slim']), \
             docker_tmp_rmi(self.client, "python:3-slim"):
                self.build("""
        FROM python:3-alpine
        COPY test_cache_python3 /bin/test_cache
        RUN set -x -e ;\\
            test "`test_cache http://deb.debian.org/`"  != None; \\
            test "`test_cache https://deb.debian.org/`" != None; \\
            test "`test_cache http://www.debian.org/`"  = None; \\
            test "`test_cache https://www.debian.org/`" = None;

        FROM python:3-slim
        COPY test_cache_python3 /bin/test_cache
        RUN set -x -e ;\\
            test "`test_cache http://deb.debian.org/`"  != None; \\
            test "`test_cache https://deb.debian.org/`" != None; \\
            test "`test_cache http://www.debian.org/`"  = None; \\
            test "`test_cache https://www.debian.org/`" = None;
        """)

    def test_205_build_pull_error(self):
        with self.assertDaemonEvents(pull=[], patch=[]), \
                self.assertRaisesRegex(RuntimeError,
                    "build failed: manifest for python:invalid not found"):
            self.build("""
        FROM python:invalid

        FROM python:3-slim
        COPY test_cache_python3 /bin/test_cache
        RUN set -x -e ;\\
            test "`test_cache http://deb.debian.org/`"  != None; \\
            test "`test_cache https://deb.debian.org/`" != None; \\
            test "`test_cache http://www.debian.org/`"  = None; \\
            test "`test_cache https://www.debian.org/`" = None;
        """, pull=True, debug=False)

    def test_206_build_build_error(self):
        with self.assertDaemonEvents(pull=[], patch=["python:3-alpine"]), \
                self.assertRaisesRegex(RuntimeError,
                "build failed: The command '/bin/sh -c echo \"foo error\" ;"
                " exit 1' returned a non-zero code: 1"):
            self.build("""
        FROM python:3-alpine
        RUN  echo "foo error" ; exit 1
        """, debug=False)


    def test_207_build_buildtime_arguments(self):
        dockerfile = """
        ARG  FOO=3-alpine
        FROM python:$FOO
        """
        with self.assertDaemonEvents(patch=['python:3-alpine']):
            self.build(dockerfile)

        with self.assertDaemonEvents(patch=['python:3-slim']):
            self.build(dockerfile, buildargs={"FOO": "3-slim"})


    def test_208_build_multiline_from(self):
        with self.assertDaemonEvents(patch=['python:3-alpine', 'python:3-slim']):
            self.build("""
        FROM python:3-alpine \\
                AS stage1
        RUN touch /hello-world

        FROM python:3-slim
        COPY --from=stage1 /hello-world /
        """)

    def test_209_build_nocache(self):
        with self.assertDaemonEvents(pull=[], patch=[]):
            self.build("""
        FROM python:3-alpine
        COPY test_cache_python3 /bin/test_cache
        RUN set -x -e ;\\
            test "`test_cache http://deb.debian.org/`"  = None; \\
            test "`test_cache https://deb.debian.org/`" = None; \\
            test "`test_cache http://www.debian.org/`"  = None; \\
            test "`test_cache https://www.debian.org/`" = None;
        """, buildargs={CACHE_VAR: "no"})

    @skipUnless(ENABLE_LONG_TESTS, "long test")
    def test_210_build_long(self):
        # aiohttp.ClientSession has a default timeout: 5 minutes

        executor = futures.ThreadPoolExecutor(2)
        # cached
        fut1 = executor.submit(self.build, """
        FROM python:3-alpine
        RUN sleep 330 ; echo OK
        """)
        # not cached
        fut2 = executor.submit(self.build, """
        FROM busybox:latest
        RUN sleep 330 ; echo OK
        """)
        futures.wait((fut1, fut2), return_when=futures.FIRST_EXCEPTION)

