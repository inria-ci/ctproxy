#!/usr/bin/python3

import collections
from   contextlib import contextmanager
import json
import logging
import os
import re
import secrets
import socket
import subprocess
import sys
import tempfile
import threading
import typing
import unittest


import docker
from   docker.errors import NotFound

unittest.TestCase.__str__ = unittest.TestCase.id

log = logging.getLogger("test_ctproxy")

ENABLE_LONG_TESTS = int(os.environ.get("TEST_ENABLE_LONG_TESTS") or "0")

def docker_build(client: docker.APIClient, *k,
        debug: typing.Optional[bool] = None, **kw) -> typing.List[str]:
    """Perform a docker build and return the image IDs

    The function call client.build(*k, **kw) and processes the output:
    - raises RuntimeError if the build fails
    - outputs the build stream to sys.stderr if:
        - debug is true
        - debug is None and build fails
    """
    image_ids = []
    stream = []
    for chunk in client.build(*k, **kw):
        js=json.loads(chunk)
        stream.append(js.get("stream", ""))
        if "aux" in js:
            image_ids.append(js["aux"]["ID"])
        if "error" in js:
            if debug or debug is None:
                sys.stderr.write("".join(stream))
            raise RuntimeError("build failed: " + js["error"])
    if debug:
        sys.stderr.write("".join(stream))

    assert image_ids
    return image_ids

def docker_subprocess(client: docker.APIClient, args: typing.List[str],
        **kw: object):
    """Create a subprocess invoking the docker client command

    - `args` are the arguments to the docker cli
    - `kw` are the extra args for `subprocess.Popen`
    """
    cmd = ["docker",
           "--host", re.sub("http://|https://", "tcp://",
        client.base_url)]
    if hasattr(client, "ssl_version"):
        cmd.append("--tls")
        if client.verify:
            cmd.append("--tlsverify")
        if isinstance(client.verify, str):
            cmd += ["--tlscacert", client.verify]
        if client.cert:
            crt, key = client.cert
            cmd += ["--tlscert", crt, "--tlskey", key]
        cmd += args
    return subprocess.Popen(cmd, **kw)

@contextmanager
def docker_tmp_rmi(client: docker.APIClient, image):
    repo, tag = image.split(":")
    tmp_tag = secrets.token_hex(16)
    client.tag(image, repo, tmp_tag)
    try:
        client.remove_image(image)
        yield
    finally:
        client.tag(f"{repo}:{tmp_tag}", repo, tag)
        client.remove_image(f"{repo}:{tmp_tag}")

def start_daemon(*k, env=os.environ, **kw):
    with tempfile.TemporaryDirectory() as tmp, \
            socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM) as sock:
        env = dict(env)
        env["NOTIFY_SOCKET"] = sockname = tmp + "/notify"
        sock.settimeout(2)
        sock.bind(sockname)
        try:
            proc = subprocess.Popen(*k, env=env, **kw)
            data=sock.recv(128)
            if data != b"READY=1":
                raise Exception("unexpected sd_notify message: %r" % data)
            return proc
        except:
            try:
                proc.kill()
                proc.join()
            except Exception:
                pass
            raise


class IntegrationTestMixin:
    """Mixin for integration tests

    on setUpClass:
        - set cls.client to docker.APIClient()
        - pull the images listed in cls.integration_images
        - start a ctproxy daemon (using the config file given in
          cls.integration_config)

    on tearDownClass:
        - kill the ctproxy daemon
    """

    # path to the configuration file for the ctproxy daemon
    integration_config = None

    # list of needed images
    integration_images = "busybox:latest",

    # list of images already pulled during this test session
    __pulled_images = set()

    @classmethod
    def setUpClass(cls):
        # global client (direct access to the docker engine)
        cls.client  = docker.APIClient()

        # ensure the docker client is working
        assert cls.client.version().get("Version"), "client not working"

        # pull the images needed for the test
        for name in cls.integration_images:
            # FIXME: should ensure the images are not altered during the test
            if name not in cls.__pulled_images:
                assert ":" in name, "cls.integration_images must have an explicit tag"
                cls.client.pull(name)
                cls.__pulled_images.add(name)

        # start the ctproxy daemon
        cmd = ["python3"]
        if os.path.isfile(".coverage") and os.path.getsize(".coverage")==0:
            # if the ".coverage" file is present and empty, then we run with
            # the coverage module
            cmd += ["-m", "coverage", "run", "-p"]
        cmd += ["-m", "ctproxy", "-q", "-c", cls.integration_config]
        cls.ctproxy_proc = start_daemon(cmd, stderr=subprocess.PIPE)
        threading.Thread(target=cls._daemon_log_thread, daemon=True,
                args=(cls.ctproxy_proc,)).start()

    @classmethod
    def tearDownClass(cls):
        # kill the ctproxy daemon
        proc = getattr(cls, "ctproxy_proc", None)
        if proc is not None:
            try:
                try:
                    # we must do our best to terminate gracefully the daemon so
                    # that the .coverage file is properly written
                    proc.terminate()
                    proc.wait(2)
                except subprocess.TimeoutExpired:
                    log.warning("ctproxy daemon not terminated properly, sending SIGKILL")
                    proc.kill()
                    proc.wait()
            except OSError:
                pass
            del cls.ctproxy_proc

        client = getattr(cls, "client", None)
        if client is not None:
            client.close()
            del cls.client

    @classmethod
    def _daemon_log_thread(cls, proc):
        normalise = lambda txt: re.sub(rb"^[\d:, -]{23} *([A-Z]+) *\[\d+\]", b"\\1", txt)
        with proc.stderr as stderr:
            for line in stderr:
                try:
                    proc._log_capture.append(normalise(line).decode(errors="replace"))
                except AttributeError:
                    sys.stderr.buffer.write(line)

    @contextmanager
    def assertDaemonLogs(self, messages: typing.Iterable[str], msg=None):
        """Test that the ctproxy daemon outputs the given log messages

        Returns a contextmanager that captures the daemon stderr and ensure
        that all messages listed in 'messages' were produced (in any order)
        """
        proc = self.ctproxy_proc
        assert not hasattr(proc, "_log_capture")
        proc._log_capture = []
        try:
            yield
            self.assertSetEqual(set(messages), set(proc._log_capture), msg=msg)
        finally:
            del proc._log_capture


    @classmethod
    def setUp(self):
        self._cleanup_dict=collections.defaultdict(set)

    @classmethod
    def tearDown(self):
        # clean up resources registered with .add_cleanup() during the test
        dirty = False
        def cleanup(kind, lst):
            nonlocal dirty
            for name in lst:
                try:
                    getattr(self.client, f"remove_{kind}")(name)
                except NotFound:
                    pass
                except Exception:
                    dirty = True
                    log.exception("cleanup error for %s %r", kind, name)

        cleanup("container", self._cleanup_dict.pop("container", ()))
        for kind, lst in self._cleanup_dict.items():
            cleanup(kind, lst)
        if dirty:
            raise Exception("cleanup error")
                    

    def add_cleanup(self, kind, name):
        """Arrange a resource to be removed on tear down

        kind: docker resource kind (container, image, ...)
        name: name of the resource

        On tearDown(), self.client.remove_{kind}(name) is called.
        """
        self._cleanup_dict[kind].add(name)



