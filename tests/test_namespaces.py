#!/usr/bin/python3

from   contextlib import contextmanager
import io
import json
import logging
import os
import random
import re
import ssl
import subprocess
import sys
import tarfile
import time
import typing
import unittest
from   unittest import mock, skipIf

import docker
from   docker.errors   import APIError, NotFound

from .test_common import log, docker_subprocess, IntegrationTestMixin

REGISTRY = "ci-registry.localhost:444"

SSL_VERSION = ssl.PROTOCOL_TLS.PROTOCOL_TLSv1_2 # because docker.APIClient uses 1.0 by default

# FIXME: registry operations are broken when running inside the boot2docker because
# 'dockerd' is statically linked (therefore libnss-myhostname is ineffective).
# This could be fixed by using dnsmasq instead. For the moment, we just disable
# the relevant tests.
NO_REGISTRY = int(os.environ.get("TEST_NO_REGISTRY") or 0)


class NamespacesTestCase(IntegrationTestMixin, unittest.TestCase):
    integration_config = "tests/config-namespaces.yml"
    integration_images = ("busybox:latest", "busybox:musl", "alpine:latest",
            "alpine/git:latest")

    @contextmanager
    def subTest(self, msg=None, **kw):
        with super().subTest(msg, **kw):
            try:
                yield
            except BaseException as e:
                lst = []
                if msg:
                    lst.append(str(msg))
                if kw:
                    lst.append(repr(kw))
                cls = AssertionError if isinstance(e, AssertionError) else Exception
                raise cls("subtest error [%s]" % " ".join(lst))


    @classmethod
    def setUpClass(cls):

        # generate certificates for user1 and user2
        subprocess.check_call(["./gen_cert", "user1", "user2"], stdout=subprocess.DEVNULL)

        super().setUpClass()

        # client for user 1
        cls.client1 = docker.APIClient(f"tcp://localhost:2376",
                tls=docker.tls.TLSConfig(ca_cert="certs/server.crt",verify=True,
                    client_cert=("certs/users/user1.crt", "certs/users/user1.key"),
                    ssl_version=SSL_VERSION))

        # client for user 2
        cls.client2 = docker.APIClient(f"tcp://localhost:2376",
                tls=docker.tls.TLSConfig(ca_cert="certs/server.crt", verify=True,
                    client_cert=("certs/users/user2.crt", "certs/users/user2.key"),
                    ssl_version=SSL_VERSION))

        # ensure that all certificates are valid (especially that the have not
        # expired)
        def check_cert(path, ca=None):
            result = subprocess.run(["certtool", "--verify", "--infile", path,
                "--load-ca-certificate", (ca or path)],
                stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            if result.returncode:
                sys.stderr.buffer.write(result.stdout)
                raise AssertionError("invalid certificate %r (may require regeneration)" % path)
        check_cert("certs/registry.crt")
        check_cert("certs/server.crt")
        check_cert("certs/users-ca.crt")
        check_cert("certs/users/user1.crt", ca="certs/users-ca.crt")
        check_cert("certs/users/user2.crt", ca="certs/users-ca.crt")

        # ensure the docker clients are working
        assert cls.client1.version().get("Version"), "client1 not working"
        assert cls.client2.version().get("Version"), "client2 not working"

    @classmethod
    def tearDownClass(cls):
        try:
            cls.reset()
        finally:
            super().tearDownClass()

    @classmethod
    def reset(cls):
        # remove containers
        for user in "user1", "user2":
            for ctr in cls.client.containers(all=True, filters={"label": ["fr.inria.ci.ctproxy.prefix=%s." % user]}):
                log.info("remove container %r", ctr["Id"])
                cls.client.remove_container(ctr["Id"], force=True)

        # remove images
        for img in cls.client.images():
            for rt in img["RepoTags"] or ():
                if re.match(r"user[12]-x[^/:]+\.%s/" % re.escape(REGISTRY), rt):
                    log.info("remove image %r", rt)
                    cls.client.remove_image(rt, force=True)
        # remove anonymous images
        for client in cls.client1, cls.client2:
            for iid in client.images():
                img = client.inspect_image(iid)
                # NOTE This assertion is somewhat important. Normally we should only have 
                # anonymous images there. If it fails, this mean that we may
                # have an issue in the namespaces implementation and may may be
                # trying to delete an arbitrary image.
                assert img["RepoTags"] == [], "image is not anonymous: %s" % img["RepoTags"]
                client.remove_image(iid)

        # remove volumes
        for vol in cls.client.volumes()["Volumes"] or ():
            name = vol["Name"]
            if re.match(r"user[12]\.", name):
                log.info("remove volume %r", name)
                cls.client.remove_volume(name, force=True)
    
        # create tags:
        #   - user1     -> alpine:latest + busybox:latest + busybox:musl + alpine/git:latest
        #   - user2     -> alpine:latest

        cls.client.tag("busybox:latest",    "user1-xregistry-z1-ydocker-yio-x.%s/library/busybox" % REGISTRY)
        cls.client.tag("busybox:musl",      "user1-xregistry-z1-ydocker-yio-x.%s/library/busybox" % REGISTRY, "musl")
        cls.client.tag("alpine:latest",     "user1-xregistry-z1-ydocker-yio-x.%s/library/alpine" % REGISTRY)
        cls.client.tag("alpine/git:latest", "user1-xregistry-z1-ydocker-yio-x.%s/alpine/git" % REGISTRY)
        cls.client.tag("alpine:latest",     "user2-xregistry-z1-ydocker-yio-x.%s/library/alpine" % REGISTRY)

    def setUp(self):
        super().setUp()
        self.reset()

    def resolve_user_name(self, client: docker.APIClient):
        """Return the name of the of the user associated with this docker client"""
        if client is self.client1:
            return "user1"
        elif client is self.client2:
            return "user2"
        else:
            return str(client)

    def check_container(self, client: docker.APIClient, target: str, expect: typing.Union[dict,str,None], *, user=None):
        """Test if a container is present.

        `client`    a docker client
        `target`    the container to be inspected
        `expect`    what to expect:
                        `None`          -> container must not be present
                        `"..."`         -> container must be present with this id
                        `{"Id": ...}`   -> container must be present with this id
        `user`      user name for logging purpose
                    (guessed with `resolve_user_name if not provided`)
        """
        if user is None:
            user = self.resolve_user_name(client)

        with self.subTest("%s inspect %r" % (user, target)):
            if expect is None:
                self.assertRaises(NotFound, client.inspect_container, target)
            else:
                if isinstance(expect, dict):
                    expect = expect["Id"]
                self.assertEqual(client.inspect_container(target)["Id"], expect)

    def check_image(self, client: docker.APIClient, target: str, expect: typing.Union[dict,str,None], *, user=None):
        """Test if an image is present.

        `client`    a docker client
        `target`    the image to be inspected
        `expect`    what to expect:
                        `None`          -> image must not be present
                        `"..."`         -> image must be present with this id
                        `{"Id": ...}`   -> image must be present with this id
        `user`      user name for logging purpose
                    (guessed with `resolve_user_name if not provided`)
        """
        if user is None:
            user = self.resolve_user_name(client)

        with self.subTest("%s inspect %r" % (user, target)):
            if expect is None:
                self.assertRaises(NotFound, client.inspect_image, target)
            else:
                if isinstance(expect, dict):
                    expect = expect["Id"]
                self.assertEqual(client.inspect_image(target)["Id"], expect)

    def check_image_list(self, client: docker.APIClient, expect: dict, *, user=None, **kw):
        """Test the content of an image list (as reported by « docker images »)

        `client`    a docker client
        `**kw`      extra parameters for client.images()
        `expect`    dict of images to be expected:
                        { ID1: {key: val, ...}, ID2: {key: val, ...}, ...}
        `user`      user name for logging purpose
                    (guessed with `resolve_user_name if not provided`)

        The test passes if the result of client.images() verifies all conditions:
          - it contains the same images id as `expect` (no more, no less)
          - for each image the expected items (`{key: val}`) are equal
        """
        if user is None:
            user = self.resolve_user_name(client)

        with self.subTest("%s images %r" % (user, kw)):
            lst = client.images(**kw)
            for img in lst:
                # ensure image is in `expect`
                self.assertIn(img["Id"], expect, msg="unexpected image")

                # compare all fields in `expect`
                for key, val in expect.pop(img["Id"]).items():
                    self.assertEqual(img[key], val, msg=key)
            
            self.assertDictEqual(expect, {}, "missing images")

    def create_container(self, image="busybox", *, client=None, **kw):
        """Create a container and inspect it immediately
        
        `client`    docker client (default is self.client1)
        `image`     docker images 
        `**kw`      extra parameters for client.create_container()

        return the result of .inspect_container()
        """
        client = self.client1 if client is None else client
        cid = client.create_container(image, **kw)
        return client.inspect_container(cid)


    def make_build_context(self, dockerfile, *, compress=None, prefix=""):
        buf = io.BytesIO()
        tar = tarfile.open(fileobj=buf,
                mode=("w" if compress is None else ("w:"+compress)))

        files = {
                prefix+"Dockerfile": dockerfile.encode(),
                prefix+"foo": b"bar\n",
                prefix+"hello": b"world\n",
                }
        for path, content in files.items():
            info = tarfile.TarInfo(path)
            info.size = len(content)
            tar.addfile(info, io.BytesIO(content))

        tar.close()
        buf.seek(0)
        return buf

    def build(self, dockerfile, *, client=None, prefix="", compress=None, **kw):
        if client is None:
            client = self.client1

        buf = (b"".join(client.build(custom_context=True,
            fileobj=self.make_build_context(dockerfile, prefix=prefix, compress=compress),
            **kw))).decode()


        stream   = []
        error    = []
        image_id = None
        decoder = json.JSONDecoder()
        while buf:
            js, index = decoder.raw_decode(buf)
            buf = buf[index:]

            if "stream" in js:
                stream.append(js["stream"])
            if "error" in js:
                error.append(js["error"])
            aux = js.get("aux")
            if aux:
                if "ID" in aux:
                    image_id = aux["ID"]

        return "".join(stream), "".join(error), image_id






        
    # --------------------- tests --------------------------------------------

    def test_000_image_inspect(self):

        with self.subTest("preamble"):
            # inspect the original busybox/alpine images
            bbx = self.client.inspect_image("busybox")
            alp = self.client.inspect_image("alpine")
            agi = self.client.inspect_image("alpine/git")

        # NOTE: all RepoDigests are empty because the initial tags are created
        # manually in self.reset() (the image was not pulled from a registry)

        with self.subTest("user1 inspect busybox"):
            img = self.client1.inspect_image("busybox:latest")
            self.assertEqual(img["Id"], bbx["Id"])
            self.assertEqual(img["RepoTags"], ["busybox:latest"])   # must be demangled
            self.assertEqual(img["RepoDigests"], [])

        with self.subTest("user1 inspect alpine"):
            img = self.client1.inspect_image("alpine:latest")
            self.assertEqual(img["Id"], alp["Id"])
            self.assertEqual(img["RepoTags"], ["alpine:latest"])    # must be demangled
            self.assertEqual(img["RepoDigests"], [])

        with self.subTest("user2 inspect busybox"), \
                self.assertRaisesRegex(NotFound,
                        "image not found: 'busybox:latest'"):
            self.client2.inspect_image("busybox:latest")

        with self.subTest("user2 inspect alpine"):
            img = self.client2.inspect_image("alpine:latest")
            self.assertEqual(img["Id"], alp["Id"])
            self.assertEqual(img["RepoTags"], ["alpine:latest"])    # must be demangled
            self.assertEqual(img["RepoDigests"], [])

        bbx_alg, bbx_id = re.match(r"([a-z][a-z0-9]*:)([0-9a-f]+)\Z", bbx["Id"]).groups()
        alp_alg, alp_id = re.match(r"([a-z][a-z0-9]*:)([0-9a-f]+)\Z", alp["Id"]).groups()

        for (target, user1_expect, user2_expect) in (
                # inspect by tag
                ("busybox",             bbx,    None),
                ("alpine",              alp,    alp ),
                ("alpine/git",          agi,    None),
                ("alpine/git:latest",   agi,    None),
                ("busy",                None,   None),
                ("busybox:latest",      bbx,    None),
                ("busybox:void",        None,   None),
                # inspect by  "alg:id"
                (bbx["Id"],             bbx,    None),
                (alp["Id"],             alp,    alp),
                # inspect by  "alg:partial_id"
                (bbx_alg + bbx_id[:12], bbx,    None),
                (alp_alg + alp_id[:12], alp,    alp),
                # inspect by   "id"
                (bbx_id,                bbx,    None),
                (alp_id,                alp,    alp),
                # inspect by   "partial_id"
                (bbx_id[:14],           bbx,    None),
                (alp_id[:14],           alp,    alp),
                # malformatted image name
                # -> should report 404 (same behaviour as docker)
                ("foo:bar:foo:bar",     None,   None),
                ):
            self.check_image(self.client1, target, user1_expect)
            self.check_image(self.client2, target, user2_expect)


    def test_001_image_rm(self):

        with self.subTest("preamble"):
            # inspect the original busybox/alpine images
            bbx = self.client.inspect_image("busybox")
            bbm = self.client.inspect_image("busybox:musl")
            alp = self.client.inspect_image("alpine")
            agi = self.client.inspect_image("alpine/git")

        for (target, user1_expect, user2_expect) in (
                ("busybox",             bbx,    None),
                (bbx["Id"],             bbx,    None),
                ("busybox:musl",        bbm,    None),
                (bbm["Id"],             bbm,    None),
                ("alpine",              alp,    alp ),
                (alp["Id"],             alp,    alp),
                ("alpine/git",          agi,    None),
                (agi["Id"],             agi,    None),
                ):
            self.check_image(self.client1, target, user1_expect)
            self.check_image(self.client2, target, user2_expect)

        # remove busybox:latest on client1
        self.client1.remove_image("busybox")

        for (target, user1_expect, user2_expect) in (
                ("busybox",             None,   None),
                (bbx["Id"],             None,   None),
                ("busybox:musl",        bbm,    None),
                (bbm["Id"],             bbm,    None),
                ):
            self.check_image(self.client1, target, user1_expect)
            self.check_image(self.client2, target, user2_expect)
        
        # remove alpine:latest on client1
        self.client1.remove_image("alpine")

        for (target, user1_expect, user2_expect) in (
                ("alpine",              None,    alp ),
                (alp["Id"],             None,    alp),
                ("alpine/git",          agi,    None),
                (agi["Id"],             agi,    None),
                ):
            self.check_image(self.client1, target, user1_expect)
            self.check_image(self.client2, target, user2_expect)

        # remove -f
        with self.assertRaisesRegex(APIError, "force=true not allowed"):
            self.client1.remove_image("alpine/git", force=True)

        # remove unknown image
        with self.assertRaisesRegex(NotFound,
                # image name in the error message must be demangled
                "image not found: 'foo:bar'"):
            self.client1.remove_image("foo:bar")
        

    def test_002_image_tag(self):

        with self.subTest("preamble"):
            # inspect the original busybox/alpine images
            bbx = self.client.inspect_image("busybox")
            bbm = self.client.inspect_image("busybox:musl")
            alp = self.client.inspect_image("alpine")

        # client1: tag id of busybox:musl as alpine:latest
        self.client1.tag(bbm["Id"], "alpine", "latest")

        for (target, user1_expect, user2_expect) in (
                ("alpine",              bbm,    alp ),
                (alp["Id"],             None,   alp),
                ("busybox:musl",        bbm,    None),
                (bbm["Id"],             bbm,    None),
                ):
            self.check_image(self.client1, target, user1_expect)
            self.check_image(self.client2, target, user2_expect)


    def test_003_image_list(self):

        with self.subTest("preamble"):
            # inspect the original busybox/alpine images
            bbx = self.client.inspect_image("busybox")
            bbm = self.client.inspect_image("busybox:musl")
            alp = self.client.inspect_image("alpine")
            agi = self.client.inspect_image("alpine/git")

            # add a second tag for alpine:latest
            self.client2.tag("alpine", "alpine", "foo")
    
        # full list
        self.check_image_list(self.client1, {
            bbx["Id"]: { "RepoDigests": ["<none>@<none>"], "RepoTags": ["busybox:latest"] },
            bbm["Id"]: { "RepoDigests": ["<none>@<none>"], "RepoTags": ["busybox:musl"] },
            alp["Id"]: { "RepoDigests": ["<none>@<none>"], "RepoTags": ["alpine:latest"] },
            agi["Id"]: { "RepoDigests": ["<none>@<none>"], "RepoTags": ["alpine/git:latest"] },
            })

        self.check_image_list(self.client2, {
            alp["Id"]: { "RepoDigests": ["<none>@<none>"], "RepoTags": ["alpine:foo", "alpine:latest"] },
            })

        # partial list
        self.check_image_list(self.client1,
                filters = {"reference": "busybox"},
                expect = {
            bbx["Id"]: { "RepoDigests": ["<none>@<none>"], "RepoTags": ["busybox:latest"] },
            bbm["Id"]: { "RepoDigests": ["<none>@<none>"], "RepoTags": ["busybox:musl"] },
            })
        
        # NOTE: dockerd accepts filters formatted either as a list or as a dict
        #       (but the API says its a string!) We need to test both cases
        #       explicitely (because docker-py uses lists only)
        #
        with mock.patch("docker.utils.convert_filters", json.dumps):
            # list
            self.check_image_list(self.client1,
                    filters = {"reference": ["alpine"]},
                    expect = {
                alp["Id"]: { "RepoDigests": ["<none>@<none>"], "RepoTags": ["alpine:latest"] },
                })
            # dict
            self.check_image_list(self.client1,
                    filters = {"reference": {"alpine": True}},
                    expect = {
                alp["Id"]: { "RepoDigests": ["<none>@<none>"], "RepoTags": ["alpine:latest"] },
                })

    def test_004_image_history(self):

        with self.subTest("preamble"):
            # inspect the original busybox/alpine images
            alp = self.client.inspect_image("alpine")

        hist = self.client1.history("alpine")
        self.assertIsInstance(hist, list)
        self.assertEqual(hist[0]["Id"], alp["Id"])
        self.assertEqual(hist[0]["Tags"], ["alpine:latest"])    # must be demangled

    def test_005_image_import(self):
        # import url -> not allowed
        for url in "http://localhost/nowhere", "https://localhost/nowhere":
            self.assertRaisesRegex(
                    APIError, "URL imports not supported",
                    self.client1.import_image, url)

        # import archive
        bio = io.BytesIO()
        tf = tarfile.TarFile(mode="w", fileobj=bio)
        content = io.BytesIO(b"hello hello blah blah")
        info = tarfile.TarInfo("hello")
        info.size = len(content.getvalue())
        tf.addfile(info, content)
        tf.close()
        bio.seek(0)

        iid = json.loads(self.client1.import_image(bio, "hello", "foo"))["status"]
        self.check_image(self.client1, "hello:foo", iid)
        self.check_image(self.client1, "hello",     None)
        self.check_image(self.client2, "hello:foo", None)
        self.check_image(self.client, "user1-xregistry-z1-ydocker-yio-x.%s/library/hello:foo" % REGISTRY, iid)

        cid = self.client1.create_container("hello:foo", ["/bin/sh"])
        tf = tarfile.TarFile(mode="r", fileobj=
                io.BytesIO(self.client1.get_archive(cid, "/hello")[0].read()))

        self.assertEqual(tf.extractfile("hello").read(), b"hello hello blah blah")

    # TODO: test with a private registry
    @skipIf(NO_REGISTRY, "registry not available")
    def test_006_image_pull_dockerhub(self):
        with self.subTest("preamble"):
            # inspect the original busybox/alpine images
            bbx = self.client.inspect_image("busybox")
            bbm = self.client.inspect_image("busybox:musl")
            alp = self.client.inspect_image("alpine")

            # overwrite alpine:latest with the id of busybox:latest
            self.client1.tag(bbx["Id"], "alpine", "latest")
            # remove busybox:latest and busybox:musl tags
            self.client1.remove_image("busybox")
            self.client1.remove_image("busybox:musl")

            for (target, user1_expect, user2_expect) in (
                    ("busybox",             None,   None),
                    ("busybox:musl",        None,   None),
                    ("alpine",              bbx,    alp ),
                    (bbx["Id"],             bbx,    None),
                    (bbm["Id"],             None,   None),
                    (alp["Id"],             None,   alp ),
                    ):
                self.check_image(self.client1, target, user1_expect)
                self.check_image(self.client2, target, user2_expect)

        with self.subTest("pull busybox:latest"):
            self.client1.pull("busybox:latest")

            for (target, user1_expect, user2_expect) in (
                    ("busybox",             bbx,    None),
                    ("busybox:musl",        None,   None),
                    ("alpine",              bbx,    alp ),
                    (bbx["Id"],             bbx,    None),
                    (bbm["Id"],             None,   None),
                    (alp["Id"],             None,   alp ),
                    ):
                self.check_image(self.client1, target, user1_expect)
                self.check_image(self.client2, target, user2_expect)
         
        with self.subTest("pull busybox:musl"):
            self.client1.pull("busybox:musl")

            for (target, user1_expect, user2_expect) in (
                    ("busybox",             bbx,    None),
                    ("busybox:musl",        bbm,   None),
                    ("alpine",              bbx,    alp ),
                    (bbx["Id"],             bbx,    None),
                    (bbm["Id"],             bbm,    None),
                    (alp["Id"],             None,   alp ),
                    ):
                self.check_image(self.client1, target, user1_expect)
                self.check_image(self.client2, target, user2_expect)
         
        with self.subTest("pull alpine:latest"):
            self.client1.pull("alpine:latest")

            for (target, user1_expect, user2_expect) in (
                    ("busybox",             bbx,    None),
                    ("busybox:musl",        bbm,   None),
                    ("alpine",              alp,    alp ),
                    (bbx["Id"],             bbx,    None),
                    (bbm["Id"],             bbm,    None),
                    (alp["Id"],             alp,    alp ),
                    ):
                self.check_image(self.client1, target, user1_expect)
                self.check_image(self.client2, target, user2_expect)




    def test_100_container_create(self):
        with self.subTest("preamble"):
            # inspect the original busybox/alpine images
            bbx = self.client.inspect_image("busybox")

            # we will use the tag foo/busybox:bar for the container creation
            # (a tag that does not exist in self.client and self.client2 domain)
            self.client1.tag("busybox", "foo/busybox", "bar")

            self.check_image(self.client1, "foo/busybox:bar", bbx)
            self.check_image(self.client2, "foo/busybox:bar", None)
            self.check_image(self.client,  "foo/busybox:bar", None)

        with self.subTest("create anonymous container"):
            # create the container
            cid = self.client1.create_container("foo/busybox:bar")["Id"]

            # inspect by id
            ctr = self.client1.inspect_container(cid)
            self.assertEqual(ctr["Image"], bbx["Id"])
            self.assertRegex(ctr["Name"], r"\A/[a-z]+_[a-z]+\Z") # name autogenerated by docker
            name = ctr["Name"].strip("/")

            # inspect by name
            self.assertEqual(self.client1.inspect_container(name)["Id"], cid)

            # container must not be usable by client2
            self.assertRaises(NotFound, self.client2.inspect_container, cid)
            self.assertRaises(NotFound, self.client2.inspect_container, name)

            # name has no prefix
            self.assertEqual(self.client.inspect_container(cid)["Name"], "/"+name)
            self.assertEqual(self.client.inspect_container(name)["Id"], cid)
            self.assertRaises(NotFound, self.client.inspect_container, "user1."+name)

        with self.subTest("create named container"):
            name = "hello-hello-foo"

            # create the container
            cid = self.client1.create_container("foo/busybox:bar", name=name)["Id"]

            # inspect by id
            ctr = self.client1.inspect_container(cid)
            self.assertEqual(ctr["Image"], bbx["Id"])
            self.assertEqual(ctr["Name"], "/"+name)

            # inspect by name
            self.assertEqual(self.client1.inspect_container(name)["Id"], cid)
            self.assertRaises(NotFound, self.client1.inspect_container, "user1."+name)

            # container must not be usable by client2
            self.assertRaises(NotFound, self.client2.inspect_container, cid)
            self.assertRaises(NotFound, self.client2.inspect_container, name)

            # name must be prefixed by user name
            self.assertEqual(self.client.inspect_container(cid)["Name"], "/user1."+name)
            self.assertEqual(self.client.inspect_container("user1."+name)["Id"], cid)
            self.assertRaises(NotFound, self.client.inspect_container, name)


    def test_100b_container_create_validate_label(self):
        # allowed labels
        labels = {"foo": "bar", "hello": "world", "fr.inria.ci.ctproxy": "foo"}
        ctr = self.create_container(labels=dict(labels))
        self.assertEqual(ctr["Config"]["Labels"], labels)

        # disallowed labels
        with self.assertRaisesRegex(APIError, "403.*label not allowed: fr.inria.ci.ctproxy.prefix"):
            self.create_container(labels={"foo": "bar", "fr.inria.ci.ctproxy.prefix": "foo"})


    def test_100f_container_create_validate_binds(self):
        ctr = self.create_container(host_config=self.client1.create_host_config(
            binds={
                'foo': {'bind': '/mnt/foo', 'mode': 'rw'},
                'bar': {'bind': '/mnt/bar', 'mode': 'ro'},
                }))
        self.assertSetEqual(set(ctr["HostConfig"]["Binds"]), {
                "foo:/mnt/foo:rw",
                "bar:/mnt/bar:ro"
                })
        self.assertEqual(set(m["Name"] for m in ctr["Mounts"]), {
                "foo", "bar"
            })

        real = self.client.inspect_container(ctr["Id"])
        self.assertSetEqual(set(real["HostConfig"]["Binds"]), {
                "user1.foo:/mnt/foo:rw",
                "user1.bar:/mnt/bar:ro"
                })
        self.assertEqual(set(m["Name"] for m in real["Mounts"]), {
                "user1.foo", "user1.bar"
            })


    def test_100g_container_create_validate_network_mode(self):

        # allowed
        def test_mode(mode, expect, expect_real):
            ctr = self.create_container(host_config=self.client1.create_host_config(
                network_mode=mode))
            real = self.client.inspect_container(ctr["Id"])
            self.assertEqual(ctr ["HostConfig"]["NetworkMode"], expect)
            self.assertEqual(real["HostConfig"]["NetworkMode"], expect_real)
            return ctr
        cid = test_mode(
                  "default",    "default",   "default")["Id"]
        ctr = test_mode(
                  "bridge",     "bridge",   "bridge")
        test_mode("none",       "none",     "none")
        test_mode("dummy",      "dummy",    "user1.dummy")
        test_mode("container:dummy", "container:dummy", "container:user1.dummy")

        test_mode("container:"+cid[:12],
                  "container:"+cid,
                  "container:"+cid)
        
        self.assertRegex(ctr["Name"], r"\A/[a-z]+_[a-z]+\Z")
        test_mode("container:"+ctr["Name"][1:],
                  "container:"+ctr["Id"],
                  "container:"+ctr["Id"])


    def test_100h_container_create_validate_links(self):

        ### link by name ###

        # user1: create foo1 and link bar1 to it
        foo1 = self.create_container(name="foo")

        bar1 = self.create_container(name="bar",
                host_config=self.client1.create_host_config(
                    links = [("foo", "bar1-foo")]))

        # user2: attempt to create bar2 linked to foo1
        with self.assertRaisesRegex(NotFound, "404.*container not found: 'foo'"):
            bar2 = self.create_container("alpine", name="bar", client=self.client2,
                    host_config=self.client2.create_host_config(
                        links = [("foo", "bar2-foo")]))

        # user1: create foo2 and link bar2 to it
        foo2 = self.create_container("alpine", name="foo", client=self.client2)

        bar2 = self.create_container("alpine", name="bar", client=self.client2,
                host_config=self.client1.create_host_config(
                    links = [("foo", "bar2-foo")]))

        self.assertEqual(bar1["HostConfig"]["Links"],["/foo:/bar/bar1-foo"])
        self.assertEqual(bar2["HostConfig"]["Links"],["/foo:/bar/bar2-foo"])

        real_bar1 = self.client.inspect_container(bar1["Id"])
        real_bar2 = self.client.inspect_container(bar2["Id"])

        self.assertEqual(real_bar1["HostConfig"]["Links"],["/user1.foo:/user1.bar/bar1-foo"])
        self.assertEqual(real_bar2["HostConfig"]["Links"],["/user2.foo:/user2.bar/bar2-foo"])

        ### link by id ###

        baz1 = self.create_container(name="baz",
                host_config=self.client1.create_host_config(
                    links = [(foo1["Id"][:12], "baz1-foo")]))

        with self.assertRaisesRegex(NotFound, "404.*container not found"):
            baz2 = self.create_container("alpine", client=self.client2,
                    host_config=self.client2.create_host_config(
                        links = [(foo1["Id"][:12], "baz2-foo")]))

        real_baz1 = self.client.inspect_container(baz1["Id"])
        self.assertEqual(baz1["HostConfig"]["Links"],["/foo:/baz/baz1-foo"])
        self.assertEqual(real_baz1["HostConfig"]["Links"],["/user1.foo:/user1.baz/baz1-foo"])

        ### link with no alias ###

        bam1 = self.create_container(name="bam", host_config={"Links": ["foo"]})

        real_bam1 = self.client.inspect_container(bam1["Id"])
        self.assertEqual(bam1["HostConfig"]["Links"],["/foo:/bam/foo"])
        self.assertEqual(real_bam1["HostConfig"]["Links"],["/user1.foo:/user1.bam/foo"])


    def test_100i_container_create_validate_volume_from(self):

        # user1: create foo1 and link bar1 to it
        foo1 = self.create_container(name="foo", host_config=self.client1.create_host_config(
            binds={'foo-vol': {'bind': '/mnt/foo', 'mode': 'rw'}}))


        # by name
        bar1 = self.create_container(
                host_config=self.client1.create_host_config(
                    volumes_from="foo"))

        # by id
        bar2 = self.create_container(
                host_config=self.client1.create_host_config(
                    volumes_from=foo1["Id"][:12]))

        real_bar1 = self.client.inspect_container(bar1["Id"])
        real_bar2 = self.client.inspect_container(bar2["Id"])

        self.assertEqual(bar1     ["HostConfig"]["VolumesFrom"], [foo1["Id"]])
        self.assertEqual(bar2     ["HostConfig"]["VolumesFrom"], [foo1["Id"]])
        self.assertEqual(real_bar1["HostConfig"]["VolumesFrom"], [foo1["Id"]])
        self.assertEqual(real_bar2["HostConfig"]["VolumesFrom"], [foo1["Id"]])


        # user2: attempt by name
        with self.assertRaisesRegex(NotFound, "404.*container not found: 'foo'"):
            self.create_container("alpine", client=self.client2,
                host_config=self.client1.create_host_config(
                    volumes_from="foo"))

        with self.assertRaisesRegex(NotFound, "404.*container not found"):
            self.create_container("alpine", client=self.client2,
                host_config=self.client1.create_host_config(
                    volumes_from=foo1["Id"][:12]))


    #TODO: test demangling of NetworkMode
    def test_101_container_ps(self):

        with self.subTest("preamble"):
            alpine_iid = self.client.inspect_image("alpine")["Id"]

            bbx = self.create_container("busybox",      name="foo-bbx", stdin_open=True,
                    labels={"foo": "bar", "hello": "world"},
                    host_config=self.client1.create_host_config(
                        binds={
                            'foo-rw': {'bind': '/mnt/foo', 'mode': 'rw'},
                            'bar-ro': {'bind': '/mnt/bar', 'mode': 'ro'},
                            }
                        ))
            bbm = self.create_container("busybox:musl", name="foo-bbm",
                    host_config=self.client1.create_host_config(
                        links=[("foo-bbx", "alias-bbx")]))
            alp = self.create_container(alpine_iid,     name="foo-alp")

            alp2= self.create_container("alpine",       name="bar-alp", stdin_open=True,
                    labels={"hello": "world"},
                    client=self.client2)

            self.client1.remove_image("alpine")

            self.client1.start("foo-bbx")
            self.client2.start("bar-alp")

        #### ps ####
        ps1 = self.client1.containers()
        self.assertEqual(len(ps1), 1)
        ctr = ps1[0]

        self.assertEqual(ctr["Id"],     bbx["Id"])
        self.assertSetEqual(set(ctr["Names"]),  {'/foo-bbm/alias-bbx', '/foo-bbx'})
        self.assertEqual(ctr["Image"],  "busybox")
        self.assertEqual(ctr["Labels"], {'foo': 'bar', 'hello': 'world'})

        mounts = {m["Name"]: m for m in ctr["Mounts"]}
        self.assertSetEqual(set(mounts), {"bar-ro", "foo-rw"})
        self.assertEqual(mounts["bar-ro"]["Mode"], "ro")
        self.assertEqual(mounts["bar-ro"]["Destination"], "/mnt/bar")


        ps2 = self.client2.containers()
        self.assertEqual(len(ps2), 1)
        ctr = ps2[0]
        self.assertEqual(ctr["Id"],     alp2["Id"])
        self.assertEqual(ctr["Names"],  ['/bar-alp'])
        self.assertEqual(ctr["Image"],  "alpine")
        self.assertEqual(ctr.get("Mount", {}), {})
        self.assertEqual(ctr.get("Labels",{}), {"hello":"world"})


        #### ps --all ####
        ps1 = self.client1.containers(all=True)

        self.assertEqual(len(ps1), 3)
    
        # bbx
        self.assertEqual(ps1[-1]["Id"], bbx["Id"])

        # bbm
        ctr = ps1[-2]
        self.assertEqual(ctr["Id"], bbm["Id"])
        self.assertEqual(ctr["Image"], "busybox:musl")
        self.assertEqual(ctr["Labels"], {})
        self.assertEqual(ctr["Names"], ["/foo-bbm"])

        # alp
        ctr = ps1[-3]
        self.assertEqual(ctr["Id"], alp["Id"])
        self.assertEqual(ctr["Image"], alpine_iid)
        self.assertEqual(ctr["ImageID"], alpine_iid)
        self.assertEqual(ctr["Labels"], {})
        self.assertEqual(ctr["Names"], ["/foo-alp"])

        #### ps --all with filters ####

        # ps() list all containers and returns a set of names
        ps = lambda cli, flt: {
                [ name for name in ctr["Names"]
                    # filter links
                    if "/" not in name[1:]][0]
                for ctr in cli.containers(all=True,filters=flt)}

        # no filter
        self.assertEqual(ps(self.client1, {}),
                {'/foo-alp', '/foo-bbm', '/foo-bbx'})

        # filter by name
        self.assertEqual(ps(self.client1, {"name": "bbm"}),
                {'/foo-bbm'})   # must not return bbx (because of the link in the bbm config)

        self.assertEqual(ps(self.client1, {"name": "^/foo-b"}),
                {'/foo-bbx', '/foo-bbm'}) # regex must be searched on demangled name

        # filter by label
        self.assertEqual(ps(self.client1, {"label": ["hello=world"]}), {"/foo-bbx"})
        self.assertEqual(ps(self.client2, {"label": ["hello=world"]}), {"/bar-alp"})

        # NOTE: dockerd accepts filters formatted either as a list or as a dict
        #       (but the API says its a string!) We need to test both cases
        #       explicitely (because docker-py uses lists only)
        #
        with mock.patch("docker.utils.convert_filters", json.dumps):
            self.assertEqual(ps(self.client1, {"label": {"hello=world": True}}), {"/foo-bbx"})
            self.assertEqual(ps(self.client2, {"label": {"hello=world": True}}), {"/bar-alp"})


    def test_102_container_delete(self):
        with self.subTest("preamble"):
            foo1 = self.create_container(name="foo")
            bar1 = self.create_container(name="bar")
            anon1= self.create_container()
            foo2 = self.create_container("alpine", client=self.client2, name="foo")
            anon2= self.create_container("alpine", client=self.client2, )

        # forbidden
        self.assertRaises(NotFound, self.client1.remove_container, anon2["Id"])
        self.assertRaises(NotFound, self.client1.remove_container, anon2["Id"][:12])
        self.assertRaises(NotFound, self.client2.remove_container, anon1["Id"])
        self.assertRaises(NotFound, self.client2.remove_container, anon1["Id"][:12])
        self.assertRaises(NotFound, self.client2.remove_container, "bar")

        # allowed
        self.client1.remove_container("foo")
        self.check_container(self.client1, "foo",       None)
        self.check_container(self.client1, foo1["Id"],  None)
        self.check_container(self.client2, "foo",       foo2)
        self.check_container(self.client2, foo2["Id"],  foo2)
        self.client2.remove_container("foo")
        self.check_container(self.client2, "foo",       None)
        self.check_container(self.client2, foo2["Id"],  None)

        self.check_container(self.client1, anon1, anon1)
        self.check_container(self.client2, anon2, anon2)
        self.client1.remove_container(anon1["Id"])
        self.check_container(self.client1, anon1, None)
        self.check_container(self.client2, anon2, anon2)
        self.client2.remove_container(anon2["Id"][:12])
        self.check_container(self.client2, anon2, None)


    def test_103_container_cmd(self):
        with self.subTest("preamble"):
            foo1 = self.create_container(name="foo", stdin_open=True)

        self.client1.start("foo")
        self.assertEqual(self.client1.inspect_container("foo")["State"]["Status"], "running")

        self.assertRaisesRegex(NotFound, "404.*container not found: 'foo'",
                self.client2.stop, "foo")
        self.assertRaisesRegex(NotFound, "404.*container not found",
                self.client2.stop, foo1["Id"])
        self.assertEqual(self.client1.inspect_container("foo")["State"]["Status"], "running")
        self.client1.kill("foo")
        self.assertEqual(self.client1.inspect_container("foo")["State"]["Status"], "exited")

        # unknown command
        self.client1.get(self.client1.base_url + "/containers/foo/changes").raise_for_status()
        self.assertEqual(
            self.client1.get(self.client1.base_url + "/containers/foo/unknown").status_code, 403)

    def test_104_container_exec(self):

        with self.subTest("preamble"):
            foo1 = self.create_container(name="foo", stdin_open=True)
            self.client1.start("foo")

        # create
        self.assertRaises(NotFound, self.client2.exec_create, "foo",      ["/bin/true"])
        self.assertRaises(NotFound, self.client2.exec_create, foo1["Id"], ["/bin/true"])

        eid = self.client1.exec_create("foo", ["/bin/sh", "-c", "sleep .5 ; echo hello foo-foo-foo"])["Id"]

        # inspect
        self.assertRaises(NotFound, self.client2.exec_inspect, eid)
        self.assertRaises(NotFound, self.client2.exec_inspect, eid[:12])
        self.assertRaises(NotFound, self.client1.exec_inspect, eid[:12])

        exe = self.client1.exec_inspect(eid)
        self.assertEqual(exe["ID"], eid)
        self.assertEqual(exe["ContainerID"], foo1["Id"])

        # start
        self.assertRaises(NotFound, self.client2.exec_start, eid,
                # FIXME: docker-py bug: without detach=True, the requests
                # session is left in an inconsistent state and the setUp of the
                # next testcase fails (timeout)
                detach=True)
        self.assertEqual(self.client1.exec_start(eid), b"hello foo-foo-foo\n")

        # unknown command
        self.client1.get(self.client1.base_url + "/exec/%s/json" % eid).raise_for_status()
        self.assertEqual(
            self.client1.get(self.client1.base_url + "/exec/%s/unknown" % eid).status_code, 403)


    def test_105_container_commit(self):

        with self.subTest("preamble"):
            foo1 = self.create_container(name="foo", command=["sh", "-c", "echo hello-foo > /bar"])
            self.client1.start("foo")

        # user2 not allowed to commit
        self.assertRaises(NotFound, self.client2.commit, "foo")
        self.assertRaises(NotFound, self.client2.commit, foo1["Id"])

        # commit with tag
        img1 = self.client1.commit("foo", "mycommit")
        self.check_image(self.client1, "mycommit:latest", img1)
        self.check_image(self.client2, "mycommit:latest", None)
        self.check_image(self.client1, img1["Id"], img1)
        self.check_image(self.client2, img1["Id"], None)

        # commit without tag
        img2 = self.client1.commit("foo")
        self.assertNotEqual(img1["Id"], img2["Id"])
        self.check_image(self.client1, img2["Id"], img2)
        self.check_image(self.client2, img2["Id"], None)

    def test_106_container_attach(self):
        with self.subTest("preamble"):
            foo = self.create_container(name="foo", stdin_open=True,
                    command=["sh", "-c", 'while read line ; do echo "received: $line" ; done'])
            self.client1.start("foo")

        # NOTE: using the 'docker' command because docker-py is broken:
        #   - does not support half duplex close (over ssl)
        #   - APIClient.attach_socket() has two bugs (TODO: report them)
        #       - does not send upgrade headers
        #       - returned SocketIO object is not writable
        with docker_subprocess(self.client1, ["attach", "foo"],
                stdin=subprocess.PIPE, stdout=subprocess.PIPE, bufsize=0
                ) as proc:
            proc.stdin.write(b"hello 12345678\n")
            self.assertEqual(proc.stdout.read(128), b"received: hello 12345678\n")
            proc.stdin.write(b"hello 4567890\n")
            self.assertEqual(proc.stdout.read(128), b"received: hello 4567890\n")


    def test_106a_container_attach_eof_on_stdin(self):
        with self.subTest("preamble"):
            foo = self.create_container(name="foo", stdin_open=True,
                    command=["sh", "-c", 'sleep .2 ; echo hello ; sleep .2 ; echo world'])
            self.client1.start("foo")

        # NOTE: using the 'docker' command (see test_106)
        with docker_subprocess(self.client1, ["attach", "foo"],
                stdin=subprocess.PIPE, stdout=subprocess.PIPE, bufsize=0
                ) as proc:
            self.assertEqual(proc.stdout.read(128), b"hello\n")
            proc.stdin.close()
            self.assertEqual(proc.stdout.read(128), b"world\n")


    def test_106b_container_attach_eof_on_stdout(self):
        with self.subTest("preamble"):
            foo = self.create_container(name="foo", stdin_open=True,
                    command=["sh", "-c", 'exec 1>&- 2>&- ; read code ; echo "not closed" ; exit "$code"'])
            self.client1.start("foo")

        # NOTE: using the 'docker' command (see test_106)
        with docker_subprocess(self.client1, ["attach", "foo"],
                stdin=subprocess.PIPE, stdout=subprocess.PIPE, bufsize=0
                ) as proc:
            time.sleep(2)
            proc.stdin.write(b"73\n")
            self.assertEqual(proc.stdout.read(128), b"") # must not display "not closed"
            self.assertEqual(proc.wait(), 73)


    @skipIf(NO_REGISTRY, "registry not available")
    def test_200_build(self):
        with self.subTest("preamble"):
            bbx = self.client1.inspect_image("busybox")

        # build "myimg:blah" from busybox
        self.check_image(self.client1, "myimg:blah", None)
        stream, error, iid = self.build("""
            FROM busybox
            COPY . /
            RUN  grep -q bar /foo
            """, tag="myimg:blah")

        self.assertEqual(error, "")
        self.assertIn("\nSuccessfully built", stream)
        self.assertIn("Step 1/3 : FROM busybox\n", stream)  # demangled
        self.assertNotEqual(iid, None)

        self.check_image(self.client1, "myimg:blah", iid)
        self.check_image(self.client2, "myimg:blah", None)
        self.check_image(self.client1, iid, iid)
        self.check_image(self.client2, iid, None)

        hist = self.client1.history(iid)
        self.assertEqual(hist[0]["Id"], iid)
        self.assertEqual(hist[2]["Id"], bbx["Id"])

        myimg_blah = iid


        # build "myimg2:latest" from "myimg:blah"
        stream, error, iid = self.build("""
            FROM myimg:blah
            RUN  echo hello > /myimg2
            """, tag="myimg2")

        self.assertEqual(error, "")
        self.assertIn("\nSuccessfully built", stream)
        self.assertIn("Step 1/2 : FROM myimg:blah\n", stream)  # demangled
        self.assertNotEqual(iid, None)

        self.check_image(self.client1, "myimg2", iid)
        self.check_image(self.client2, "myimg2", None)
        self.check_image(self.client1, iid, iid)
        self.check_image(self.client2, iid, None)

        hist = self.client1.history(iid)
        self.assertEqual(hist[0]["Id"], iid)
        self.assertEqual(hist[1]["Id"], myimg_blah)

        myimg2 = iid


        # build anonymous image from "myimg2"
        stream, error, iid = self.build("""
            FROM myimg2
            RUN  echo hello > /anonymous
            """)

        self.assertEqual(error, "")
        self.assertIn("\nSuccessfully built", stream)
        self.assertIn("Step 1/2 : FROM myimg2\n", stream)  # demangled
        self.assertNotEqual(iid, None)

        self.check_image(self.client1, iid, iid)
        self.check_image(self.client2, iid, None)

        hist = self.client1.history(iid)
        self.assertEqual(hist[0]["Id"], iid)
        self.assertEqual(hist[1]["Id"], myimg2)

        anon = iid


        # attempt to have client2 build images based on client1 images

        def test_build_client2(base, errormsg):
            self.check_image(self.client2, base, None)
            stream, error, iid = self.build("FROM %s\n" % base, client=self.client2)

            self.assertIn("Step 1/1 : FROM %s\n" % base, stream)    # demangled
            self.assertIn(errormsg, error)
            self.assertIs(iid, None)

        test_build_client2("myimg:blah", "pull access denied for myimg,")
        test_build_client2("myimg2",     "pull access denied for myimg2,")


    @skipIf(NO_REGISTRY, "registry not available")
    def test_201_build_from_image_id(self):
        with self.subTest("preamble"):
            ctr = self.create_container()
            anon = self.client1.commit(ctr["Id"])["Id"]
            without_digest = anon.strip("sha256:")

        def test_build_client1(base, expect):
            stream, error, iid = self.build("""
                FROM %s
                RUN  echo %r > /base
                """ % (base, base))

            self.assertEqual(error, "")
            self.assertIn(f"Step 1/2 : FROM {expect}\n", stream)  # demangled
            self.assertIn("\nSuccessfully built", stream)
            self.assertNotIn(iid, (None, anon))

            self.check_image(self.client1, iid, iid)
            self.check_image(self.client2, iid, None)

        test_build_client1(anon,                anon)
        test_build_client1(without_digest,      anon)
        test_build_client1(without_digest[:12], anon)


        def test_build_client2(base, errormsg):
            self.check_image(self.client2, base, None)
            stream, error, iid = self.build("FROM %s\n" % base, client=self.client2)

            self.assertIn("Step 1/1 : FROM %s\n" % base, stream)    # demangled
            self.assertIn(errormsg, error)
            self.assertIs(iid, None)

        test_build_client2(anon, "pull access denied for sha256,")
        test_build_client2(without_digest,   "pull access denied for %s," % without_digest)
        test_build_client2(without_digest[:12],   "pull access denied for %s," % without_digest[:12])


    @skipIf(NO_REGISTRY, "registry not available")
    def test_202_build_compressed_context(self):
        with self.subTest("preamble"):
            bbx = self.client1.inspect_image("busybox")

        for algo in "gz", "bz2", "xz":
            tag = "myimg:"+algo
            with self.subTest(tag):
                self.check_image(self.client1, tag, None)
                stream, error, iid = self.build("""
                    FROM busybox
                    COPY . /
                    RUN grep -q bar /foo
                    RUN touch /img-%s 
                    """ % algo, tag=tag, compress=algo)

                self.assertEqual(error, "")
                self.assertIn("\nSuccessfully built", stream)
                self.assertIn("Step 1/4 : FROM busybox\n", stream)  # demangled
                self.assertIn("Step 4/4 : RUN touch /img-%s\n" % algo, stream)
                self.assertNotEqual(iid, None)

                self.check_image(self.client1, iid, iid)
                self.check_image(self.client1, tag, iid)


    @skipIf(NO_REGISTRY, "registry not available")
    def test_203_build_with_fancy_dockerfile_path(self):

        # build must work also if the Dockerfile is at "./Dockerfile"
        stream, error, iid = self.build("""
            FROM busybox
            COPY . /
            RUN  grep -q bar /foo
            """, prefix="./")

        self.assertEqual(error, "")
        self.assertNotEqual(iid, None)
        self.check_image(self.client1, iid, iid)

        # but it should fail if the user attempts to escape the directory
        self.assertRaisesRegex(APIError,
                "400.*unable to extract dockerfile 'Dockerfile' from the tar archive",
                self.build, "FROM busybox", prefix="../")

    @skipIf(NO_REGISTRY, "registry not available")
    def test_204_build_from_unknown_image(self):
        with self.subTest("preamble"):
            bbx = self.client.inspect_image("busybox")
            self.client1.remove_image("busybox")
            self.assertRaises(NotFound, self.client1.inspect_image, "busybox")

        stream, error, iid = self.build("FROM busybox")

        self.assertEqual(error, "")
        self.assertIn("\nSuccessfully built", stream)
        self.assertIn("Step 1/1 : FROM busybox\n", stream)  # demangled
        self.assertNotEqual(iid, None)

        self.check_image(self.client1, iid,         bbx["Id"])
        self.check_image(self.client1, "busybox",   bbx["Id"])


    def test_300_volumes(self):

        def create_volume(client, name, nonce):
            cid = self.create_container("alpine", command=["sh", "-c", "echo %s >>/mnt/nonce" % nonce],
                    client=client,
                    host_config=client.create_host_config(
                        binds={
                            name: {'bind': '/mnt/', 'mode': 'rw'},
                            }))["Id"]

            client.start(cid)
            self.assertEqual(client.wait(cid, timeout=1), 0)
            client.remove_container(cid)

        def test_volume(client, name, expected_nonce):
            cid = None
            try:
                cid = self.create_container("alpine", command=["cat", "/mnt/nonce"],
                        client=client,
                        host_config=client.create_host_config(
                            binds={
                                name: {'bind': '/mnt/', 'mode': 'rw'},
                                }))["Id"]
                client.start(cid)
                returncode = client.wait(cid, timeout=1)
                if expected_nonce is None:
                    self.assertNotEqual(returncode, 0)
                else:
                    self.assertEqual(returncode, 0)
                    actual_nonce = client.logs(cid).strip().decode()
                    self.assertEqual(actual_nonce, expected_nonce)
            finally: # because self.client containers are not automatically removed by .reset()
                if cid:
                    client.remove_container(cid)

        rnd1 = str(random.random())
        rnd2 = str(random.random())
        rnd3 = str(random.random())

        create_volume(self.client1, "foo", rnd1)
        create_volume(self.client2, "foo", rnd2)
        create_volume(self.client1, "bar", rnd3)
        test_volume(self.client1, "foo", rnd1)
        test_volume(self.client2, "foo", rnd2)
        test_volume(self.client1, "bar", rnd3)
        test_volume(self.client2, "bar", None)
        test_volume(self.client,  "user1.foo", rnd1)
        test_volume(self.client,  "user2.foo", rnd2)
        test_volume(self.client,  "user1.bar", rnd3)
        test_volume(self.client,  "user2.bar", None)

        self.client2.remove_volume("bar")

        # inspect volumes
        def inspect_volume(client, name, expect):
            if expect is None:
                self.assertRaises(NotFound, client.inspect_volume, name)
            else:
                vol = client.inspect_volume(name)
                self.assertEqual(vol["Name"], expect)
                return vol

        foo1 = inspect_volume(self.client1, "foo", "foo")
        foo2 = inspect_volume(self.client2, "foo", "foo")
        bar1 = inspect_volume(self.client1, "bar", "bar")
        inspect_volume(self.client2, "bar", None)
        raw_foo1 = inspect_volume(self.client, "user1.foo", "user1.foo")
        raw_foo2 = inspect_volume(self.client, "user2.foo", "user2.foo")
        raw_bar1 = inspect_volume(self.client, "user1.bar", "user1.bar")
        inspect_volume(self.client, "user2.bar", None)

        def strip_name(vol):
            tmp = dict(vol)
            del tmp["Name"]
            return tmp

        self.assertEqual(strip_name(foo1), strip_name(raw_foo1))
        self.assertEqual(strip_name(foo2), strip_name(raw_foo2))
        self.assertEqual(strip_name(bar1), strip_name(raw_bar1))
        self.assertNotEqual(strip_name(foo1), strip_name(foo2))

        # list volumes
        def test_list(client, expect, **kw):
            sort_by_name = lambda lst: sorted(lst, key=lambda x:x["Name"])
            self.assertEqual(
                    sort_by_name(client.volumes(**kw)["Volumes"] or []),
                    sort_by_name(expect))
        test_list(self.client1, [foo1, bar1])
        test_list(self.client2, [foo2])
        test_list(self.client1, [foo1], filters={"name":["foo"]})
        test_list(self.client1, [bar1], filters={"name":["bar"]})
        test_list(self.client1, [],     filters={"name":["^user"]})
        test_list(self.client1, [bar1], filters={"name":["r"]})
        test_list(self.client1, [foo1], filters={"name":["^foo"]})

        # remove volumes
        self.assertRaises(NotFound, self.client2.remove_volume, "bar")
        self.client1.remove_volume("foo")
        inspect_volume(self.client1, "foo", None)
        inspect_volume(self.client2, "foo", "foo")
        inspect_volume(self.client1, "bar", "bar")
        self.client2.remove_volume("foo")
        inspect_volume(self.client2, "foo", None)
        self.client1.remove_volume("bar")
        inspect_volume(self.client1, "bar", None)
        self.assertRaises(NotFound, self.client1.remove_volume, "foo")
        self.assertRaises(NotFound, self.client2.remove_volume, "foo")
        self.assertRaises(NotFound, self.client1.remove_volume, "bar")


# TODO 'build' tests fail on ci4ci-container-builder.ci because the docker
#      engine is too old (1.13) -> does not include a {aux: {"ID": "....."}} in
#      the stream
#
# unit tests
# TODO test with a dockerfile that is a symbolic link
#           - to a path inside the context
#           - to a path outside the context
# TODO test with lowercase FROM
