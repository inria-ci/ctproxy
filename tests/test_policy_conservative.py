#!/usr/bin/python3

import re
import unittest
from   unittest import mock

import docker
from   docker.errors import APIError

from .test_common import log, docker_subprocess, IntegrationTestMixin

class ConservativePolicyTestCase(IntegrationTestMixin, unittest.TestCase):
    integration_config = "tests/config-policy-conservative.yml"
    integration_images = "busybox:latest",

    @classmethod
    def setUpClass(cls):
        cls.client1 = docker.APIClient("tcp://localhost:2375")
        super().setUpClass()


    def create_container(self, image="busybox", *, json=None, **kw):
        """Create a container and inspect it immediately

        `image`     docker images 
        
        
        Extra parameters (one of):
        - `**kw`        (hign-level) parameters for client.create_container()
        - `json, **kw`  (low-level ) raw json container config + extra
                                     parameters to requests.post()

        return the result of .inspect_container()
        """
        client = self.client1
        if json is None:
            # use docker-py
            cid = client.create_container(image, **kw)["Id"]
        else:
            assert isinstance(json, dict)
            # send raw request
            cfg = dict(json)
            cfg["Image"] = image
            resp = client.post(client.base_url + "/containers/create",
                    headers={"Content-Type": "application/json"},
                    json=cfg, **kw)
            try:
                resp.raise_for_status()
            except Exception as e:
                raise APIError(e, resp, explanation=resp.text)
            cid = resp.json()["Id"]
        self.add_cleanup("container", cid)
        return client.inspect_container(cid)


    def test_100c_container_create_validate_unknown_config_key(self):
        # allowed keys
        ctr = self.create_container(json={
            "Tty":          True,   # allowed
            "OpenStdin":    True,   # allowed
            "PortSpecs":    "FOO",  # ignored (removed by ctproxy)
            "name":         "BAR",  # ignored (removed by ctproxy)
            })
        self.assertTrue(ctr["Config"]["Tty"])
        self.assertTrue(ctr["Config"]["OpenStdin"])

        # unknown keys
        with self.assertRaisesRegex(APIError, "403.*unknown config keys: 'INVALID_FOO'"):
            self.create_container(json={"Tty": True, "OpenStdin": True, "INVALID_FOO": "BAR"})


    def test_100d_container_create_validate_host_config_unknown_key(self):
        # allowed
        ctr = self.create_container(host_config={"CapDrop": ["all"]})
        self.assertEqual(ctr["HostConfig"]["CapDrop"], ["all"])

        # disallowed
        with self.assertRaisesRegex(APIError, "403.*host config parameter not allowed: INVALID_FOO=BAR"):
            ctr = self.create_container(host_config={
                "CapDrop": ["all"], "INVALID_FOO": "BAR"})

    def test_100e_container_create_validate_host_config_mandatory_values(self):
        MANDATORY_VALUES = {
            'BlkioDeviceReadBps':   [],
            'BlkioDeviceReadIOps':  [],
            'BlkioDeviceWriteBps':  [],
            'BlkioDeviceWriteIOps': [],
            'BlkioWeight': 0,
            'BlkioWeightDevice':    [],
            'CapAdd': [],                   # dangerous
            'Cgroup': '',                   # dangerous
            'CgroupParent': '',             # dangerous
            'ContainerIDFile': '',          # FIXME: is it written on server-side?
            'CpuCount': 0,
            'CpuPercent': 0,
            'CpuPeriod': 0,
            'CpuQuota':  0,
            'CpuRealtimePeriod': 0,
            'CpuRealtimeRuntime': 0,
            'CpuShares': 0,
            'CpusetCpus': '',
            'CpusetMems': '',
            'DeviceCgroupRules': None,
            'Devices': [],                  # dangerous
            'DiskQuota': 0,
            'IOMaximumBandwidth': 0,
            'IOMaximumIOps': 0,
            'IpcMode': '',                  # dangerous
            'Isolation': '',                # dangerous
            'KernelMemory': 0,
            'LogConfig': {'Type': '', 'Config': {}},
            'Memory': 0,
            'MemoryReservation': 0,
            'MemorySwap': 0,
            'MemorySwappiness': -1,
            'NanoCpus': 0,
            'PidMode': '',                  # dangerous
            'Privileged': False,            # dangerous
            'SecurityOpt': [],              # dangerous
            'ShmSize':   0,
            'UTSMode': '',                  # dangerous
            'UsernsMode': '',               # dangerous
            'VolumeDriver': '',             # dangerous
        }

        # allowed
        hc = dict(MANDATORY_VALUES)
        hc["CapDrop"]   = ["all"]
        ctr = self.create_container(host_config=hc)
        self.assertEqual(ctr["HostConfig"]["CapDrop"], ["all"])

        # disallowed
        for key, mandatory_value in MANDATORY_VALUES.items():
            with self.subTest(key):
                # test bad type
                test_value = True if isinstance(mandatory_value, str) else "FOO"
                with self.assertRaisesRegex(APIError, "400.*invalid type for key.*"+key):
                    self.create_container(host_config={key: test_value})

                # test bad value
                if isinstance(mandatory_value, bool):
                    test_value = not mandatory_value
                elif isinstance(mandatory_value, int):
                    test_value = mandatory_value + 601
                elif isinstance(mandatory_value, dict):
                    test_value = dict(mandatory_value)
                    test_value["FOO"] = "BAR"
                elif isinstance(mandatory_value, list):
                    test_value = list(mandatory_value)
                    test_value.append("FOO")
                elif isinstance(mandatory_value, str):
                    test_value = "FOO"
                elif mandatory_value is None:
                    continue
                else:
                    raise NotImplementedError()
                with self.assertRaisesRegex(APIError, "403.*host config parameter not allowed: " + key):
                    self.create_container(host_config={key: test_value})



    def test_100f_container_create_validate_binds(self):
        # allowed
        self.add_cleanup("volume", "test-foo")
        self.add_cleanup("volume", "test-bar")
        ctr = self.create_container(host_config=self.client1.create_host_config(
            binds={
                'test-foo': {'bind': '/mnt/foo', 'mode': 'rw'},
                'test-bar': {'bind': '/mnt/bar', 'mode': 'ro'},
                }))
        self.assertSetEqual(set(ctr["HostConfig"]["Binds"]), {
                "test-foo:/mnt/foo:rw",
                "test-bar:/mnt/bar:ro"
                })
        self.assertEqual(set(m["Name"] for m in ctr["Mounts"]), {
                "test-foo", "test-bar"
            })

        # forbidden
        # NOTE: VolumeDriver is tested by test_100d
        with self.assertRaisesRegex(APIError, "403.*volume not allowed: '/tmp/foo'"):
            self.create_container(host_config=self.client1.create_host_config(
                binds={'/tmp/foo': {'bind': '/mnt/foo', 'mode': 'rw'}}))


    def test_100g_container_create_validate_network_mode(self):

        # allowed
        def test_mode(mode, expect):
            ctr = self.create_container(host_config=self.client1.create_host_config(
                network_mode=mode))
            real = self.client.inspect_container(ctr["Id"])
            self.assertEqual(ctr ["HostConfig"]["NetworkMode"], expect)
            self.assertEqual(real["HostConfig"]["NetworkMode"], expect)
            return ctr

        self.add_cleanup("network", "test-dummy")
        cid = test_mode("default", "default")
        ctr = test_mode("bridge",  "bridge")
        test_mode("none", "none")
        test_mode("test-dummy", "test-dummy")
        test_mode("container:dummy", "container:dummy")

        # forbidden
        with self.assertRaisesRegex(APIError, "403.*network mode not allowed: 'host'"):
            self.create_container(host_config=self.client1.create_host_config(
                network_mode="host"))


    def test_100h_container_create_validate_links(self):

        ### malformatted link ###
        with self.assertRaisesRegex(APIError, "400.*malformatted link: 'foo:bar:baz'"):
            self.create_container(host_config={"Links": ["foo:bar:baz"]})


    def test_100j_container_create_validate_port_bindings(self):
        # allowed
        ctr = self.create_container(ports=["80", (53, "udp")],
                host_config=self.client1.create_host_config(
                    port_bindings = {"80": None, "53/udp": None}
            ))

        self.assertEqual(ctr["HostConfig"]["PortBindings"], {
            '53/udp': [{'HostIp': '', 'HostPort': ''}],
            '80/tcp': [{'HostIp': '', 'HostPort': ''}]})

        # forbidden
        for pb in (
                {"80/tcp": 1234},
                {"80/tcp": ("127.0.0.1", None)},
                {"80/tcp": ("127.0.0.1", 1234)},
                {"53/udp": 1234},
                {"53/udp": ("127.0.0.1", None)},
                {"53/udp": ("127.0.0.1", 1234)},
                ):
            with self.assertRaisesRegex(APIError, "403.*invalid port binding.*must be anonymous"):
                self.create_container(ports=["80", (53, "udp")],
                        host_config=self.client1.create_host_config(
                            port_bindings = pb))


    def test_100k_container_create_validate_networking_config(self):

        MANDATORY_ENDPOINT_VALUES = (
                ('MacAddress',          ""),
                ('IPv6Gateway',         ""),
                ('Gateway',             ""),
                ('EndpointID',          ""),
                ('NetworkID',           ""),
                ('GlobalIPv6Address',   ""),
                ('IPAddress',           ""),
                ('IPPrefixLen',         0),
                ('GlobalIPv6PrefixLen', 0),
                ('IPAMConfig',          None),
                ('Aliases',             None),
                ('DriverOpts',          None),
                )

        # allowed
        self.create_container(networking_config={"EndpointsConfig": {
            "default": dict(MANDATORY_ENDPOINT_VALUES)}})

        # unknown key
        with self.assertRaisesRegex(APIError, "403.*unknown NetworkingConfig key: 'Foo'"):
            self.create_container(networking_config={"Foo": "BAR"})

        # bad endpoint names
        for name in ("host", "container:foo"):
            with self.assertRaisesRegex(APIError, "403.*network mode not allowed|400.*bad endpoint name"):
                self.create_container(networking_config={
                    "EndpointsConfig": {name: {}}
                    })

        # bad endpoint attributes
        #   - invalid values
        #   - unknown keys
        DUMMY_VALUES = {"": "DUMMY", 0: 42, None: "DUMMY"}
        for key, val in MANDATORY_ENDPOINT_VALUES + (("UnknownKey", ""),):
            with self.subTest(key), \
                 self.assertRaisesRegex(APIError, "403.*endpoint parameter not allowed:.*%s" % re.escape(key)):
                self.create_container(networking_config={
                    "EndpointsConfig": {"foo": {key: DUMMY_VALUES[val]}}
                    })

        # links
        with self.assertRaisesRegex(APIError, "400.*not supported: Links inside EndpointConfig"):
            self.create_container(networking_config={
                    "EndpointsConfig": {"foo": {"Links": ["DUMMY"]}}
                    })


