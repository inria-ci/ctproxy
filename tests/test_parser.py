#!/usr/bin/python3


from typing import List, Mapping, Optional, Tuple
import unittest

from ctproxy.parser import DockerfileCommand, DockerfileParser, ParseError
from . import test_common


class DockerfileParserTestCase(unittest.TestCase):

    def check_parser(self, dockerfile: str,
            expected_commands: List[Tuple[str, ...]],
            *, args: Optional[Mapping[str, str]] = None) -> DockerfileParser:
        parser = DockerfileParser(dockerfile.splitlines(),
                ({} if args is None else args))
        commands = [(c.name, *c.args) for c in parser.commands]
        self.assertSequenceEqual(expected_commands, commands)
        return parser

    def test_basic(self):
        p = self.check_parser("""
        FROM busybox
        ARG FOO=bar
        RUN hello world

        FROM alpine
        ARG BAR
        RUN hello
        """, [
            ("FROM", "busybox"),
            ("ARG", "FOO=bar"),
            ("FROM", "alpine"),
            ("ARG", "BAR"),
        ])

        self.assertEqual(p.lines, ['',
                    '        FROM busybox',
                    '        ARG FOO=bar',
                    '        RUN hello world',
                    '',
                    '        FROM alpine',
                    '        ARG BAR',
                    '        RUN hello',
                    '        '])
        self.assertSequenceEqual(p.commands, [
            DockerfileCommand(name='FROM', args=('busybox',), scope=0, start_line=1, end_line=2),
            DockerfileCommand(name='ARG',  args=('FOO=bar',), scope=1, start_line=2, end_line=3),
            DockerfileCommand(name='FROM', args=('alpine',),  scope=0, start_line=5, end_line=6),
            DockerfileCommand(name='ARG',  args=('BAR',),     scope=2, start_line=6, end_line=7),
            ])


    def test_continuation(self):
        p = self.check_parser("""
        FROM busybox\\   
:latest     \\
            AS stage1
        RUN hello world
        # continuation in comments must be ignored \\
        ARG FOO
        RUN unterminated escape \\""", [
            ("FROM", "busybox:latest", "AS", "stage1"),
            ("ARG", "FOO"),
            ])

        self.assertEqual(p.lines, [
            '',
            '        FROM busybox\\   ',
            ':latest     \\',
            '            AS stage1',
            '        RUN hello world',
            '        # continuation in comments must be ignored \\', 
            '        ARG FOO',
            '        RUN unterminated escape \\',
            ])
        self.assertSequenceEqual(p.commands, [
            DockerfileCommand(name='FROM', args=('busybox:latest', 'AS', 'stage1'), scope=0, start_line=1, end_line=4),
            DockerfileCommand(name='ARG', args=('FOO',), scope=1, start_line=6, end_line=7)
            ])

    def test_escape_directive(self):
        p = self.check_parser("""# escape=`
        FROM busybox`  
:latest     `
            AS stage1
        ARG FOO\\
        ARG BAR
        """, [
            ("FROM", "busybox:latest", "AS", "stage1"),
            ("ARG", "FOO\\"),
            ("ARG", "BAR"),
            ])

        p = self.check_parser("""# escape= \\
        FROM busybox\\ 
            AS stage1
            """, [
                ('FROM', 'busybox', 'AS', 'stage1')
                ])

        self.assertRaisesRegex(
                ParseError, r"""line 1: invalid ESCAPE '\+'. Must be ` or \\""",
                self.check_parser, "# escape=+", [])

    def test_arg_error(self):
        self.assertRaisesRegex(
                ParseError, "line 1: ARG requires exactly one argument",
                self.check_parser, "ARG", [])

        self.assertRaisesRegex(
                ParseError, "line 2: ARG requires exactly one argument",
                self.check_parser, "\nARG FOO= BAR", [])


    def test_arg_expansion_plus_escape_and_quotes(self):
        self.check_parser(r"""
            ARG FOO1=bar
            ARG FOO2=\$ba\ r
            ARG FOO3=yi$FOO1
            ARG FOO4=yo${FOO1}
            ARG FOO5='single: $FOO1'
            ARG FOO6="double: $FOO1"
            ARG FOO7="double: ${FOO1}"
            ARG FOO1=replaced
            FROM busybox:$FOO1
            """, [
                ('ARG', 'FOO1=bar'),
                ('ARG', 'FOO2=$ba r'),
                ('ARG', 'FOO3=yibar'),
                ('ARG', 'FOO4=yobar'),
                ('ARG', 'FOO5=single: $FOO1'),
                ('ARG', 'FOO6=double: bar'),
                ('ARG', 'FOO7=double: bar'),
                ('ARG', 'FOO1=replaced'),
                ('FROM', 'busybox:replaced'),
                ])

        # token w/ an unterminated quote
        self.check_parser("ARG FOO=bar\\\\\n\n", [
            ('ARG', 'FOO=bar')
            ])

    def test_arg_expansion_bash_modifiers(self):
        self.check_parser(r"""
        ARG FOO
        ARG BAR1="hello ${FOO:-world}"
        ARG BAR2="hello ${FOO:+world}"
        ARG FOO=bar
        ARG BAR1="hello ${FOO:-world}"
        ARG BAR2="hello ${FOO:+world}"
        """, [
            ('ARG', 'FOO'),
            ('ARG', 'BAR1=hello world'),
            ('ARG', 'BAR2=hello '),
            ('ARG', 'FOO=bar'),
            ('ARG', 'BAR1=hello bar'),
            ('ARG', 'BAR2=hello world'),
            ])


    def test_default_args(self):
        self.check_parser(r"""
        ARG FOO
        ARG BAR=$FOO
        ARG FOO=hello
        ARG BAR=$FOO
        """, [
            ('ARG', 'FOO'),
            ('ARG', 'BAR=world'),
            ('ARG', 'FOO=hello'),
            ('ARG', 'BAR=world'),
            ], args={"FOO": "world"})


        # undeclared arg
        self.check_parser("ARG BAR=$FOO",
                [("ARG", "BAR=")],
                args={"FOO": "must be ignored"})


    def test_arg_scopes(self):
        self.check_parser(r"""
        ARG FOO=bar
        FROM busybox:$FOO
        ARG FOO=invalid
        FROM alpine:$FOO
        """, [
            ('ARG', 'FOO=bar'),
            ('FROM', 'busybox:bar'),
            ('ARG', 'FOO=invalid'),
            ('FROM', 'alpine:bar'),
            ])

