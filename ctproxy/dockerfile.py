#!/usr/bin/python3

import contextlib
import io
import json
import logging
import os
import subprocess
import sys
import tempfile

import aiohttp

import ctproxy
from .bases import ErrorResponse
from .utils import Subprocess

DEVNULL = subprocess.DEVNULL

log = logging.getLogger("dockerfile")

@contextlib.contextmanager
def handle_subprocess_error(msg: str):
    """return a context manager that handle subprocess errors
    
    On exit it catches CalledProcessError raises ErrorResponse(400, msg)
    instead.
    """
    try:
        yield
    except subprocess.CalledProcessError as e:
        log.error("subprocess %r exited with code %d", e.cmd, e.returncode)
        raise ErrorResponse(400, msg)


async def save_request_body(content: aiohttp.StreamReader, filename: str,
        *, decompress=False) -> None:
    """Receive and save the request body into a file
    
    `content`       the aiohttp Request.content
    `filename`      path of the target file
    `decompress`    enable auto-decompression
    
    If `decompress` is true, then the function attempts to detect and
    uncompress the followind compression formats: gzip, bzip2 and xz (but
    still accepts uncompressed content).
    """
    with open(filename, "wb") as fp, \
            handle_subprocess_error("context read/uncompress error"):

        # read the 8 leading bytes (to check the magic numbers)
        lead = await content.read(8)
        cmd = "cat",
        if decompress:
            # detect the compression format
            if lead.startswith(b'\x1f\x8b'):
                cmd = "gzip", "-dc"
            elif lead.startswith(b'BZh'):
                cmd = "bzip2", "-dc"
            elif lead.startswith(b'\xfd7zXZ\x00'):
                cmd = "xz", "-dc"

        # run the decompression command
        async with Subprocess(*cmd, stdin=subprocess.PIPE, stdout=fp,
                stderr=None) as proc:
            fp.close()

            proc.stdin.write(lead)
        
            async for data in content.iter_chunked(8192):
                proc.stdin.write(data)
                await proc.stdin.drain()

            proc.stdin.close()

async def extract_dockerfile(tmp_context: str, dockerfile: str, xtmp: str):
    """Extract a dockerfile form the build context

        `tmp_context`   path to the (optionally compressed).tar archive of the
                        context
        `dockerfile`    path to the dockerfile from the archive root
        `xtmp`          temporary directory were the dockerfile will be extracted

        return a "r+" file object on the extracted dockerfile
    """

    # extract the dockerfile
    #
    # we use bsdtar instead of gnu tar because it is more robust in dealing
    # with the path of archive members (for extracting the dockerfile)
    with handle_subprocess_error(
            f"unable to extract dockerfile {dockerfile!r} from the tar archive"):
        async with Subprocess("bsdtar", "xf", tmp_context,
                "--", dockerfile,
                stdin=DEVNULL, stdout=DEVNULL, stderr=None, cwd=xtmp):
            pass


    # find the dockerfile and ensure it does not escape the archive
    # extraction path using a symbolic link
    dockerfile_realpath = os.path.realpath(os.path.join(xtmp, dockerfile))
    xtmp_realpath = os.path.realpath(xtmp)
    if (os.path.commonpath((xtmp_realpath, dockerfile_realpath)) != xtmp_realpath
        or os.path.islink(dockerfile_realpath)
        or not os.path.isfile(dockerfile_realpath)):
        raise  ErrorResponse(400, "dockerfile not found")

    # open the dockerfile for editing
    return open(dockerfile_realpath, "r+")


async def append_dockerfile(tmp_context: str, dockerfile: str, xtmp: str):
    """Append a dockerfile to the build context

        `tmp_context`   path to the (optionally compressed).tar archive of the
                        context
        `dockerfile`    path to the dockerfile from the archive root
        `xtmp`          temporary directory were the context is extracted
    """
    
    # append the new dockerfile to the archive
    with handle_subprocess_error("context tar error"):
        async with Subprocess("bsdtar", "rf", tmp_context,
                "--", dockerfile,
                stdin=DEVNULL, stdout=DEVNULL, stderr=None, cwd=xtmp):
            pass


class DockerfilePatcher:
    # context of the current request
    ctx: "ctproxy.Context"

    # path to the dockerfile (inside the context)
    dockerfile: str

    # build arguments (provided in the request)
    buildargs: dict

    # temporary directory
    tmp: tempfile.TemporaryDirectory

    # path of the temporary context tarball
    tmp_context: str

    # path of the temporary extrated files
    xtmp: str

    # temporary dockerfile
    tmp_dockerfile: io.TextIOBase

    def __init__(self, ctx: "ctproxy.Context"):
        self.ctx = ctx

    async def __aenter__(self) -> io.TextIOBase:
        self.dockerfile = self.ctx.params.get("dockerfile", "Dockerfile")
        if self.ctx.params.get("remote"):
            # TODO suport remote dockerfiles
            raise ErrorResponse(404, "remote dockerfile not supported")

        try:
            self.buildargs = json.loads(self.ctx.params.get("buildargs", "{}"))
            if not isinstance(self.buildargs, dict):
                raise ValueError()
        except ValueError:
            raise ErrorResponse(400, "malformatted buildargs")

        self.tmp = tempfile.TemporaryDirectory()
        try:
            self.tmp_context = os.path.join(self.tmp.name, "context.tar")

            # receive the context and store it in the tmp_dir
            await save_request_body(self.ctx.request.content, self.tmp_context,
                    decompress=True)

            self.xtmp = os.path.join(self.tmp.name, "x")
            os.mkdir(self.xtmp)
            self.tmp_dockerfile = await extract_dockerfile(
                    self.tmp_context, self.dockerfile, self.xtmp)

            return self.tmp_dockerfile
        except:
            await self.__aexit__(*sys.exc_info())
            raise

    async def __aexit__(self, exc, val, tb):
        try:
            if exc is None:
                self.tmp_dockerfile.close()
                await append_dockerfile(
                        self.tmp_context, self.dockerfile, self.xtmp)

                # replace the request body
                self.ctx.data = open(self.tmp_context, "rb")
        finally:
            self.tmp.cleanup()




