#!/usr/bin/python3

from   typing import Iterable, Optional, Tuple

from   aiohttp.web import Response, StreamResponse, json_response

class HttpRequestHandler:
    children: Tuple["HttpRequestHandler", ...]

    def __init__(self, children: Iterable["HttpRequestHandler"]) -> None:
        self.children = tuple(children)

    async def handle_request(self, ctx: "Context") -> Optional[StreamResponse]:
        return await self.recurse(ctx)

    async def recurse(self, ctx: "Context") -> Optional[StreamResponse]:
        for child in self.children:
            response = await child.handle_request(ctx)
            if response is not None:
                return response
        return None

class ErrorResponse(Exception):
    args: Tuple[int, str]

    def __init__(self, status: int, message: str) -> None:
        super().__init__(status, message)

    def to_json_response(self) -> Response:
        return json_response({"message": self.args[1]}, status=self.args[0])
