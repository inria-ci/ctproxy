#!/usr/bin/env python3

import asyncio
import base64
import codecs
import hashlib
import io
import json
import logging
import os
import random
import re
import socket
import ssl
import struct
import subprocess
import tarfile
import tempfile
import textwrap
import time
from   types        import MappingProxyType
import typing.re
from   typing       import Any, Awaitable, BinaryIO, Callable, Dict, Iterable, List, Mapping, NamedTuple, Optional, Sequence, Set, Tuple, Type, Union
from   urllib.parse import unquote, urlencode, urlparse, ParseResult
import weakref

log = logging.getLogger("ctproxy")

try:
    # use gnutls by default
    from   aio_gnutls_transport     import GnutlsEventLoopPolicy, ssl
    asyncio.set_event_loop_policy(GnutlsEventLoopPolicy())

except ImportError as e:
    # fall back to openssl
    log.warning("unable to import the aio_gnutls_transport module (%s)", e)
    log.warning("falling back to the ssl module ('docker attach' might not be served correctly)")
    import ssl

import aiohttp
from   aiohttp      import ClientResponse
import aiohttp.web
from   aiohttp.web  import Request, Response, json_response, StreamResponse
if tuple(map(int, re.match("(\d+)\.(\d+)", aiohttp.__version__).groups())) < (3, 4):
    #
    # Workaround for aiohttp < 3.4.0
    #
    # The HTTP request parser is broken, it drops the payload when the request
    # contains: "Connection: upgrade" and "Upgrade: tcp", which renders the
    # /exec/{id}/start endpoint unusable
    # 
    # Fortunately only the cython implementation is broken, therefore we can
    # revert to the pure python implementation.
    #
    # TODO: remove after migrating to aiohttp>=3.4.0
    #
    import aiohttp.http_parser
    import aiohttp.web_protocol
    aiohttp.http_parser.HttpRequestParser  = aiohttp.http_parser.HttpRequestParserPy
    aiohttp.http.HttpRequestParser         = aiohttp.http_parser.HttpRequestParserPy
    aiohttp.web_protocol.HttpRequestParser = aiohttp.http_parser.HttpRequestParserPy

from   async_exit_stack import AsyncExitStack
import cachetools
import config_reader
from   config_reader import Mapping as CfgDict
from   multidict import MultiDict

from .bases import ErrorResponse, HttpRequestHandler
from .dockerfile import DockerfilePatcher
from .namespaces import DockerNamespacesHandler
from .parser import DockerfileParser, ParseError
from .utils import coerce, try_set_result, parse_bool
from . import plugins

T = typing.TypeVar("T")

# higest docker API version supported by us
API_VERSION = "1.32"

#FIXME: ensure yarl query_string decode bug not present

# returns (version, command, target)
def docker_split_path(method: str, path: str) -> Tuple[
        str, Tuple[str,...], Optional[str]]:
    version, subpath = re.match(r"^/((?:v\d+\.\d+/)?)(.*)\Z", path).groups()
    lst = subpath.split("/")
    if method == "DELETE" and len(lst) > 1:
        # delete is special because it ends with the target
        command = "DELETE", lst[0], "TARGET"
        target  = "/".join(lst[1:])
    elif len(lst) == 2 and method == "GET" and lst[0] == "volumes":
        command = "GET", lst[0], "TARGET"
        target = lst[1]
    elif len(lst) < 3:
        # the command does not have a target
        command = method, *lst
        target = None
    elif lst[0] == "images":
        # urls targetting an image are special because the id may contain slashes
        command = method, "images", "TARGET", lst[-1]
        target  = "/".join(lst[1:-1])
    else:
        # other urls targetting a resource
        command = method, lst[0], "TARGET", *lst[2:]
        target = lst[1]

    return version, command, target


# returns (method, path)
def docker_make_path(version: str, command: Tuple[str,...],
        target: Optional[str]) -> Tuple[str, str]:
    assert version.endswith("/") or version == ""

    method, *lst = command
    if target is not None:
        assert lst[1] == "TARGET"
        lst[1] = target
    return method, f"/{version}{'/'.join(lst)}"


def create_streams(transport: asyncio.Transport) -> Tuple[asyncio.StreamReader, asyncio.StreamWriter]:
    reader = asyncio.StreamReader()
    protocol = asyncio.StreamReaderProtocol(reader)
    protocol.connection_made(transport)
    transport.set_protocol(protocol)
    writer = asyncio.StreamWriter(transport, protocol, reader,
            asyncio.get_event_loop())
    return reader, writer

async def forward_stream(reader: asyncio.StreamReader, writer: asyncio.StreamWriter) -> None:
    try:
        while True:
            data = await reader.read(8192)
            if not data:
                if writer.can_write_eof():
                    writer.write_eof()
                else:
                    # NOTE: half-duplex close is not implemented in:
                    #   - the python ssl module
                    #   - python3-aioopenssl
                    #   - the nginx protocol switching implementation
                    #     (Connection: Upgrade) with or without ssl
                    #
                    # This breaks all API endpoints doing: "Upgrade: tcp"
                    #   eg: 'docker exec CONTAINER sh -c "sleep 1 ; date"'
                    #       -> terminates immediately without any output
                    #
                    # We close the transport here to be consistent with nginx's
                    # behaviour but this is not an acceptable solution
                    writer.close()
                return
            writer.write(data)
            await writer.drain()
    except:
        import traceback
        traceback.print_exc()


def merge_docker_stream(raw: bytes, streams: Tuple[int,...] = (1,2)) -> bytes:
    """Return a combined output of stdout+stderr from a docker stream

    The stream format is described at:
    https://docs.docker.com/engine/api/v1.32/#operation/ContainerAttach

    This function demultiplexes the stream and merges the streams listed in
    streams (stdout(1) and stderr(2) by default)
    """
    result = []
    offset = 0
    while offset + 8 < len(raw):
        stream_id, length = struct.unpack(">bxxxL", raw[offset:offset+8])
        offset += 8
        if stream_id in streams:
            result.append(raw[offset:offset+length])
            offset += length
    return b"".join(result)



class ConfigReloader:
    """A class for notifying "config reload" events to multiple listeners

    New listeners have to be registered with .register().

    When .reload() is called, the class will ensure that every callback will be
    executed.

    Optionally listeners can provide a timeout to force reloading the config at
    regular intervals (even if .reload() is never called).
    - the first timeout is provided as a parameter of .register()
    - subsequent timeouts are provided as the return value of the callback

    Notes:
    - .reload() returns immediately. The execution of callbacks is performed
      asynchronously
    - The class ensures that the same callback is never called multiple times
      concurrently. If a callback is already running during a reload event,
      then it a second execution will be scheduled after it has finished.
    """
    _events: Set[asyncio.Event]

    def __init__(self):
        self._events = weakref.WeakSet()

    def register(self, 
            callback: Callable[[], Awaitable[Optional[float]]],
            timeout: Optional[float] = None,
            ) -> "ConfigReloader.Listener":
        """Register a new listener
        
        `callback` is the coroutine function to be executed when a .reload() is
                   called. The callback may optionally return a timeout for the
                   next reload event.
        `timeout` is the max time for waiting the first reload event

        The returned value is a context manager that can optionally be used for
        managing the unregistration of the listener. If unused then the
        callback is registered forever.

        Examples:
        - register a listener forever:
            reloader.register(callback)

        - register listener just for the duration of a 'with' block:
            async with reloader.register(callback):
                # listener active
                ...
            # listener inactive
        """
        lst = self.Listener()
        lst._callback = callback
        lst._event = asyncio.Event()
        lst._task = asyncio.ensure_future(lst._task_func(lst._event, timeout))
        self._events.add(lst._event)
        return lst

    def reload(self) -> None:
        """Trigger a callback of all registered listeners"""
        for ev in self._events:
            ev.set()


    class Listener:
        _callback: Callable[[], Awaitable[Optional[float]]]
        _event: asyncio.Event
        _task:  asyncio.Task

        async def __aenter__(self) -> None:
            pass

        async def __aexit__(self, *k: object):
            event = self._event
            del self._event
            event.set()
            await self._task

        async def _task_func(self, event: asyncio.Event,
                timeout: Optional[float]) -> None:
            while True:
                if timeout is None:
                    await event.wait()
                else:
                    try:
                        await asyncio.wait_for(event.wait(), timeout)
                    except asyncio.TimeoutError:
                        pass

                event.clear()
                if not hasattr(self, "_event"):
                    return
                try:
                    timeout = await self._callback()
                except Exception:
                    log.exception("unhandled exception in the config reloader")

class Context:
    request: Request
    data: Union[bytes, BinaryIO]
    js: object
    scheme: str
    method: str
    # formatted as: HOST[:PORT]  (may contain a port number)
    host: str
    path: str
    params: Dict[str, str]      # FIXME: may need a multidict ?
    auth_cert: Optional[str]    # PEM format, if signature is verified

    response_headers: Dict[str, str]

    # NOTE: it is not 100% guaranteed that the filter will be applied. They may
    # not be applied if:
    #   - the response from the upstream server is not chunked
    #   - the chunk is not a well-formatted UTF-8 json object
    #   - any of the filter raises an exception
    # (in the two latter cases, an exception will be written in the logs)
    #
    # (called in reverse order)
    json_chunked_response_filters: List[Callable[[object], None]]

    # List of callbacks to be run after the request is fully processed
    # (called in reverse order)
    _deferred: List[Callable[[], None]]

    # FIXME: should remove the .proxy_redirect attrib and automatically do the
    # redirect in the HttpReverseProxy
    # (SRC, DST) -> replace SRC with DST in the 'Location' headers
    proxy_redirect: Optional[tuple]

    client_session:     aiohttp.ClientSession
    client_ssl_context: ssl.SSLContext

    docker_parsed_url: ParseResult
    docker_version: str     # ends with / (matches: "v[0-9.]+/")
    docker_command: Tuple[str, ...]
    docker_target: Optional[str]

    user: Optional[plugins.User]

    def __init__(self, request: Request) -> None:
        self.request = request
        self.scheme = request.scheme
        self.method = request.method
        self.host = request.host
        self.path = request.path
        self.json_chunked_response_filters = []
        self.response_headers = {}
        self.proxy_redirect = None

        # NOTE: params is coerced into a dict (to remove ambiguities in case a
        # parameter is duplicated)
        self.params = dict(request.rel_url.query)
        self._deferred = []

        # get the TLS client certificate
        #
        self.auth_cert = None
        obj = request.transport.get_extra_info("ssl_object")
        if obj is not None:
            # this is a TLS socket
            der = obj.getpeercert(True)
            if der is not None:
                # user presented a valid certificate
                self.auth_cert = ssl.DER_cert_to_PEM_cert(der).strip()

    async def json(self) -> object:
        assert not hasattr(self, "data")
        if not hasattr(self, "js"):
            self.js = await self.request.json()
        return self.js

    def docker_request(self, *command: str, target: Optional[str] = None,
            **kw: object) -> aiohttp.ClientResponse:
        method, path = docker_make_path(self.docker_version, command, target)
        return self.client_session.request(method,
                f"""{self.docker_parsed_url.scheme}://{
                     self.docker_parsed_url.netloc}{
                     self.docker_parsed_url.path}{path}""",
                ssl=self.client_ssl_context, **kw)

    def __enter__(self) -> "Context":
        """Start processing the request

        Returns self
        """
        return self

    def __exit__(self, *k: object) -> None:
        """Finish processing the request

        runs the callbacks registered with `.defer()`
        """
        for callback in reversed(self._deferred):
            try:
                callback()
            except:
                log.exception("unhandled exception in deferred callback")

    def defer(self, callback: Callable[[], None]) -> None:
        """Schedule a callback to be run after the request is fully processed

        Note: multiple callback may be scheduled for the same request, they
        will be processed in the LIFO order.
        """
        self._deferred.append(callback)

    def apply_json_chunked_response_filters(self, data: bytes) -> bytes:
        if self.json_chunked_response_filters:
            # try to apply the chunked response filters
            # (but fallback to the original chunk in case
            #  of failure)
            try:
                js = json.loads(data, encoding="utf-8")
                for func in reversed(self.json_chunked_response_filters):
                    func(js)
                data = json.dumps(js).encode()
            except Exception as e:
                log.exception("unhandled exception the the json chunked response filters, chunk will not be filtered")
        return data

class HttpReverseProxy(HttpRequestHandler):

    client_session: aiohttp.ClientSession

    forwarded_headers: Tuple[str, ...]

    def __init__(self, children: Iterable["HttpRequestHandler"], *,
            client_session: aiohttp.ClientSession,
            forwarded_headers: Tuple[str, ...]) -> None:

        self.forwarded_headers = forwarded_headers
        self.client_session = client_session
        super().__init__(children)

    def _forward_headers(self, headers, proxy_redirect: Optional[tuple] = None):
        dct = {}
        for key in self.forwarded_headers:
            val = headers.get(key)
            if val is not None:
                dct[key] = val
        location = headers.get("Location")
        if location and proxy_redirect is not None:
            mo = re.match(r"(https?)://([A-Za-z0-9.-]+)(?::(\d+))?((?:[/?].*)?)\Z", location)
            if mo:
                def get_port(scheme, port):
                    return (80 if scheme == "http" else 443) if port is None else port
                s,h,p = proxy_redirect[0]
                scheme, host, port, tail = mo.groups()
                if s==scheme and h==host and get_port(s,p)==get_port(scheme, port):
                    # redirect !
                    s,h,p = proxy_redirect[1]
                    p = "" if p is None else f":{p}"
                    dct["Location"] = f"{s}://{h}{p}{tail}"
        return dct

    async def handle_request(self, ctx: Context) -> Optional[StreamResponse]:
        ctx.client_session = self.client_session
        ctx.client_ssl_context = None

        response = await self.recurse(ctx)
        if response is not None:
            return response
        if ctx.request.host == ctx.host and ctx.request.path == ctx.path:
            raise RuntimeError("reverse proxy url not set")


        assert ctx.request.path.startswith("/")
        url = "%s://%s%s" % (ctx.scheme, ctx.host, ctx.path)
        if hasattr(ctx, "js"):
            assert not hasattr(ctx, "data")
            js = ctx.js
            data = None
        elif hasattr(ctx, "data"):
            js = None
            data = ctx.data
        else:
            js = None
            data = await ctx.request.read()
        extra = {}

        async with ctx.client_session.request(ctx.method, url,
                params=ctx.params, ssl=ctx.client_ssl_context, 
                headers = self._forward_headers(ctx.request.headers),
                data=data, json=js, **extra) as cli_resp:

            headers = self._forward_headers(cli_resp.headers, ctx.proxy_redirect)
            headers.update(ctx.response_headers)
            if cli_resp.status == 101:
                # connection upgraded
                log.debug("upgrade response")

                response = Response(status=cli_resp.status, headers=headers)
                await response.prepare(ctx.request)

                # get the asyncio transports of both connections
                #TODO: ensure that we did not miss anything in the buffers
                cli_transport = cli_resp.connection.protocol.transport
                srv_transport = ctx.request.protocol.transport

                try:
                    # disconnect the aiohttp asyncio protocolt
                    cli_resp.connection.protocol.transport = None
                    ctx.request.protocol.transport = None

                    # create stream reader/writer pairs
                    cli_reader, cli_writer = create_streams(cli_transport)
                    srv_reader, srv_writer = create_streams(srv_transport)

                    # forward the traffic in both directions
                    await asyncio.gather(
                            forward_stream(cli_reader, srv_writer),
                            forward_stream(srv_reader, cli_writer),
                            return_exceptions=True)
                finally:
                    cli_transport.close()
                    srv_transport.close()
            else:
                try:
                    content_length = int(cli_resp.headers.get("Content-Length"))
                except Exception:
                    content_length = None

                if (    cli_resp.status == 204
                        or (content_length is not None
                            and content_length < 64*1024)
                        or ctx.method == "HEAD"):
                    # direct response (when payload is known to be short)
                    log.debug("direct response")
                    response = Response(status=cli_resp.status, headers=headers,
                            body = await cli_resp.read())
                    
                else:
                    # streamed response
                    response = StreamResponse(status=cli_resp.status)
                    for key, val in headers.items():
                        response.headers[key] = val

                    if cli_resp.headers.get("Transfer-Encoding") == "chunked":
                        # streamed response (chunked)
                        log.debug("streamed response (chunked)")

                        response.enable_chunked_encoding()
                        await response.prepare(ctx.request)

                        async for data, _ in cli_resp.content.iter_chunks():
                            await response.write(
                                ctx.apply_json_chunked_response_filters(data))

                    else:
                        # streamed response (not chunked)
                        # FIXME: it would be useful to forward "Content-Length"
                        #        if present in the response
                        log.debug("streamed response (not chunked)")

                        await response.prepare(ctx.request)

                        async for data in cli_resp.content.iter_chunked(8192):
                            await response.write(data)

        return response


class DockerProxy(HttpRequestHandler):

    docker_client: plugins.DockerClient

    def __init__(self, children: Iterable["HttpRequestHandler"], *,
            docker_client: plugins.DockerClient) -> None:

        self.docker_client = docker_client
        super().__init__(children)


    async def handle_request(self, ctx: Context) -> Optional[StreamResponse]:

        try:
            (ctx.docker_version, ctx.docker_command, ctx.docker_target
                    ) = docker_split_path(ctx.request.method, ctx.path)
        except ValueError:
            return Response(status=404)

        log.info("docker version=%r command=%r target=%r", ctx.docker_version, ctx.docker_command, ctx.docker_target)

        ctx.client_session, ctx.client_ssl_context, ctx.docker_parsed_url = (
                self.docker_client.route_request(
                    ctx.docker_command, ctx.docker_target))
        del ctx.method
        del ctx.scheme
        del ctx.host
        del ctx.path

        try:
            response = await self.recurse(ctx)
        except ErrorResponse as e:
            return e.to_json_response()
        if response is not None:
            return response

        ctx.method, path = docker_make_path(
            ctx.docker_version, ctx.docker_command, ctx.docker_target)
        ctx.scheme = ctx.docker_parsed_url.scheme
        ctx.host   = ctx.docker_parsed_url.netloc
        ctx.path   = ctx.docker_parsed_url.path + path
        return None

class DockerAuth(HttpRequestHandler):

    async def handle_request(self, ctx: Context) -> Optional[StreamResponse]:
        ctx.user = None
        for child in self.children:
            response = await child.handle_request(ctx)
            if response is not None:
                return response
            if ctx.user is not None:
                return None

        raise ErrorResponse(401, "unauthorized")



class DockerCacher(HttpRequestHandler):
    prefix: str
    label_digest: str
    proxy_host: str
    proxy_ip: str
    cache_var: str

    cache_ca: Optional[bytes]
    cache_domains: Tuple[str, ...]
    install_archive: Optional[bytes]
    install_archive_digest: Optional[str]

    def __init__(self, children: Iterable["HttpRequestHandler"], *,
            proxy_host: str, label: str, prefix: str, cache_var: str,
            reloader: ConfigReloader) -> None:

        pattern = r"[a-z0-9][a-z0-9._-]*\Z"
        if not re.match(pattern, prefix):
            raise ValueError(f"Malformatted cache prefix {prefix!r} (must match regex {pattern}")

        self.prefix = prefix

        # label for patched images
        # -> stores the relevant digest
        self.label_digest = label + ".digest"

        self.proxy_host = proxy_host
        self.proxy_ip = socket.gethostbyname(proxy_host)
        self.cache_var = cache_var

        self.reset_cache_config()

        # LRU dict of images that we did not manage to patch
        # {image_id: digest) 
        self.unpatchable = cachetools.LRUCache(256)

        reloader.register(self.reload, 0)

        super().__init__(children)

    def reset_cache_config(self) -> None:
        self.cache_ca = None
        self.cache_domains = ()
        self.install_archive = None
        self.install_archive_digest = None

    @staticmethod
    def _registry_auth_key(image: str) -> str:
        """Return the X-Registry-Config key for a given image
        
        see https://docs.docker.com/engine/api/v1.40/#operation/ImageBuild
        """
        head, *tail = image.split("/")
        if tail and ("." in head or ":" in head):
            # 3rd party registry
            return head
        else:
            # official registry
            return "https://index.docker.io/v1/"


    async def reload(self) -> float:
        log.info("reloading cache server config")
        try:
            async with aiohttp.request("GET",
                    "https://%s/cache/ca.crt" % self.proxy_host) as rep:
                rep.raise_for_status()
                new_ca = await rep.read()
                if new_ca != self.cache_ca:
                    log.info("cache CA certificate updated")
                    self.cache_ca = new_ca
                    # TODO: parse the CA and compute the expiration date
                    #       and remove it automatically when it expires
                    self.make_install_archive()

            async with aiohttp.request("GET",
                    "https://%s/cache/domains.txt" % self.proxy_host) as rep:
                rep.raise_for_status()
                new_domains = tuple((await rep.text()).splitlines())
                if new_domains != self.cache_domains:
                    log.info("cache domain list updated: %s", new_domains)
                    self.cache_domains = new_domains

        except Exception as e:
            log.error("failed to reload the cache server configuration (%s), caching will be disabled", e)
            #log.exception("cache_config_loader_task")

            # FIXME: maybe we should make multiple attempts before
            #        disabling the cache completely
            self.reset_cache_config()

            # retry every 5 minutes
            return 300
        else:
            # reload the config every hour
            return 3600

    def make_install_archive(self) -> None:
        """Create the tar archive to be used for patching docker images
    
        contains:
        - the x509 certificate (ca/ca.crt)
        - the install script (ca/install-ca.sh)

        archive is stored as self.install_archive
        digest  is stored as self.install_archive_digest

        digest is computed from both files, thus the image will be recreated if
        any of them is updated
        """

        hasher = hashlib.sha256()

        def addfile(tar: tarfile.TarFile, arcname: str, mode: int,
                content: bytes) -> None:
            tarinfo = tarfile.TarInfo(arcname)
            tarinfo.mode = mode
            tarinfo.size = len(content)
            hasher.update(arcname.encode())
            hasher.update(content)
            tar.addfile(tarinfo, io.BytesIO(content))

        buf = io.BytesIO()
        tar = tarfile.open(fileobj=buf, mode="w")

        # load our CA certificate
        addfile(tar, "ca/ctproxy-ca.crt", 0o644, self.cache_ca)

        # load our install-ca script
        with open(os.path.join(os.path.dirname(__file__), "data",
                                "install-ca.sh"), "rb") as fp:
            addfile(tar, "ca/install-ca.sh", 0o755, fp.read())

        tar.close()

        self.install_archive = buf.getvalue()
        self.install_archive_digest = hasher.hexdigest()

        log.info("transparent cache digest is %s", self.install_archive_digest)

    def nocache_requested(self, *,
            container_config: Optional[dict] = None,
            build_args: Optional[Dict[str, str]] = None) -> bool:
        """Return True if user requests to disable transparent caching"""

        assert container_config is not None or build_args is not None

        check = lambda val: val.strip().lower() in ("0", "no")

        if container_config is not None:
            for env_var in container_config.get("Env") or ():
                key, val = env_var.split("=", 1)
                if key == self.cache_var and check(val):
                    return True
        if build_args is not None and check(
                build_args.get(self.cache_var, "")):
            return True
        return False

    async def handle_request(self, ctx: Context) -> Optional[StreamResponse]:
        #TODO: handle 'images' and filter TARGET
        if ctx.docker_command == ("POST", "containers", "create"):
            # container creation
            # -> patch the image on-the-fly

            log.info("######## create ########")
            js = await ctx.json()

            if self.nocache_requested(container_config=js):
                # cache disabled by user
                log.info(" -> NOCACHE")
                return None

            if self.install_archive is None:
                log.warning("the cache CA certificate is not available, created container will not use the cache")
                return None
                
            if not isinstance(js["Image"], str):
                return Response(status=400)

            js["Image"], enable_cache = await self.prepare_image(ctx, js["Image"])
            log.info(" -> image %r", js["Image"])

            if enable_cache:
                hc = js.setdefault("HostConfig", {})
                # add extra hosts in the container config
                # FIXME: we shoud avoid duplicates
                eh = [f"{host}:{self.proxy_ip}" for host in self.cache_domains
                        ] + (hc.get("ExtraHosts") or [])
                hc["ExtraHosts"] = eh

        elif ctx.docker_command == ("DELETE", "images", "TARGET"):
            # image deletion
            # -> remove possibly cached images
            log.info("######## rmi ########")

            img, image = await self.inspect_image(ctx, ctx.docker_target)
            for image in set((self.cached_image(img["Id"]), self.cached_image(image))):
                try:
                    async with ctx.docker_request("DELETE", "images", image,
                            params={"force": "true"}) as rep:
                        if rep.status != 404:
                            await self.expect_status("rmi", rep, 200)
                        log.info("delete %s (%d %s)", image, rep.status, rep.reason)
                except Exception:
                    log.exception("failed to delete image %r", image)

        elif ctx.docker_command == ("POST", "build"):
            return await self.handle_build(ctx)

        return None

    class _Aborted(Exception):
        pass

    async def handle_build(self, ctx: "Context") -> StreamResponse:

        async with DockerfilePatcher(ctx) as dockerfile:

            response = StreamResponse(status=200)
            response.content_type = "application/json"
            response.enable_chunked_encoding()
            await response.prepare(ctx.request)
            write_chunk = lambda js: response.write(json.dumps(js).encode())

            pull = parse_bool(ctx.params.pop("pull", ""))
            registry_config = json.loads(base64.b64decode(
                ctx.request.headers.get("X-Registry-Config", "e30=")))
            async def pull_image(image: str) -> None:
                if not pull:
                    # pull is implicit
                    async with ctx.docker_request("GET", "images", image, "json") as rep:
                        if rep.status != 404:
                            # image not absent
                            # -> no pull
                            return None
 
                ##  do the pull  ##

                # prepare the auth header
                registry_auth = registry_config.get(
                        self._registry_auth_key(image))
                hdrs = {} if registry_auth is None else {
                        "X-Registry-Auth": base64.b64encode(
                            json.dumps(registry_auth).encode()).decode()}

                await write_chunk({"stream": f"Pulling {image}\n"})
                async with ctx.docker_request("POST", "images", "create",
                        params={"fromImage": image}, headers=hdrs) as rep:
                    if rep.status != 200: 
                        # report the error
                        data = await rep.json()
                        await write_chunk({"errorDetail": data,
                            "error": data.get("message", "pull failed")})
                        raise self._Aborted
                    else:
                        # forward the output
                        success = False
                        async for data, _ in rep.content.iter_chunks():
                            await response.write(data)
                            js = json.loads(data.decode())
                            if "error" in js:
                                raise self._Aborted
                            elif js.get("status", "").startswith("Status:"):
                                success = True
                        if success:
                            return None
                        raise self._Aborted("Unexpected EOF on pull")

            try:
                # process the dockerfile
                content, cache_enabled = await self.process_dockerfile(
                        ctx, pull_image, dockerfile.read())
                await write_chunk({
                    "stream": "#######################################\n"
                              "#### transparent cache is %-8s ####\n"
                              "#######################################\n" % (
                                  "enabled" if cache_enabled else "disabled")})
                dockerfile.seek(0)
                dockerfile.truncate()
                dockerfile.write(content)
            except self._Aborted as e:
                if e.args:
                    msg = f"build failed: {e}"
                    await write_chunk({"errorDetail": {"message": msg}, "error": msg})
                return response

        # forward the request
        async with ctx.docker_request("POST", "build", params=ctx.params,
                data=ctx.data, headers={"Content-Type": "application/x-tar"},
                ) as rep:
            if rep.status != 200:
                # report the error
                data = await rep.json()
                await write_chunk({"errorDetail": data,
                    "error": data.get("message", "build failed")})
            else:
                # forward the output
                async for data, _ in rep.content.iter_chunks():
                    await response.write(
                            ctx.apply_json_chunked_response_filters(data))
        return response


    async def process_dockerfile(self, ctx: "Context",
            pull_image: Callable[[str], None], content: str) -> Tuple[str, bool]:
        """Process a dockerfile to use transparent caching

            `content`       content of the dockerfile
            `pull_image`    coroutine to be called for pulling a base image
            `ctx`           current request context

            Return a tuple (dockerfile, cache_enabled) and patches the context
            if cache is enabled.
        """

        lines = content.splitlines()

        # parse the build-time arguments
        args = json.loads(ctx.params.get("buildargs", "{}"))
        if not isinstance(args, dict):
            raise self._Aborted("buildargs must be a JSON map")

        # parse the dockerfile
        parser = DockerfileParser(lines, args)

        # pull the base images and patch them
        enable_cache = False if self.nocache_requested(build_args=args) else None
        already_pulled = set()
        for cmd in parser.commands:
            if cmd.name != "FROM":
                continue

            # parse the image name and add ':latest' tag if not present
            # (otherwise we would pull all images from the repository)
            repo, tag = re.match("(.*?)(?::([^:/]*))?\Z", cmd.args[0], re.S).groups()
            image = f"{repo}:{tag or 'latest'}"

            # pull the image
            if image not in already_pulled:
                await pull_image(image)
                already_pulled.add(image)

            if enable_cache == False:
                continue

            # patch the image
            image, enable_cache = await self.prepare_image(ctx, image)
            if enable_cache:
                # patch the 'FROM' line in the dockerfile
                lines[cmd.start_line] = f"FROM {image} {' '.join(cmd.args[1:])}"
                for i in range(cmd.start_line+1, cmd.end_line):
                    lines[i] = ""

        # caching is enabled only if *all* images support caching
        # (in case of a multi-stage build)
        if enable_cache:
            # add extra hosts in the build parameters
            # FIXME: we should avoid duplicates
            eh = [f"{host}:{self.proxy_ip}" for host in self.cache_domains
                    ] + ctx.request.query.getall("extrahosts", [])

            # FIXME: Context.params should always be a MultiDict
            ctx.params = MultiDict(ctx.params)
            ctx.params.popall("extrahosts", None)
            ctx.params.extend(("extrahosts", x) for x in eh)

            # demangle the image names in the response
            pattern = f"FROM {re.escape(self.prefix)}[i_]"
            replacement = "FROM "
            @ctx.json_chunked_response_filters.append
            def demangle(msg: object) -> None:
                if isinstance(msg, dict):
                    stream = msg.get("stream")
                    if stream:
                        msg["stream"] = re.sub(pattern, replacement, stream)

            # replace the dockerfile
            lines.append("")
            content = "\n".join(lines)

        return content, bool(enable_cache)


    # FIXME: should fallback to uncached mode
    async def expect_status(self, stage: str, response: ClientResponse, status: int):
        message = "ctproxy DockerCacher %s error" % stage
        if response.status != status:
            log.error("%s: %s", message,
                    (await response.read()).strip().decode(errors="replace"))
            raise ErrorResponse(500, message)

    async def inspect_image(self, ctx: Context, image: str) -> Tuple[object, str]:
        """Inspect an image and return a fully qualified image name

        return a tuple (<JS>, <IMAGE>) where:
        - <JS> is the inspection result
        - <IMAGE> is either:
            - an image id: "sha256:<ID>"
            - a fully qualified name "<REPO>:<TAG>"

        """
        async with ctx.docker_request("GET", "images", image, "json") as rep:
            if rep.status == 404:
                raise ErrorResponse(404, f"No such image: {image}")

            await self.expect_status("inspect", rep, 200)
            img = await rep.json()

        orig_image = image
        if ":" not in image:
            image += ":latest"
        if all(i != image for i in img["RepoTags"]):
            image = img["Id"]

        log.info("matched image: %s -> %s", orig_image, image)
        return img, image

    def cached_image(self, image: str) -> str:
        """Return the 'cache' image associated to a docker image
        
        `image` is either formatted as "<REPO>:<TAG>" or "sha256:<ID>"

        Return an image formatted as "<REPO>:<TAG>"
        """
        assert ":" in image
        if image.startswith("sha256:"):
            return f"{self.prefix}i{image[7:]}:latest"
        else:
            return f"{self.prefix}_{image}"

    async def prepare_image(self, ctx: Context, image: str) -> Tuple[str, bool]:
        """Prepare a docker image before run

        `image` is the image we want to run, formatted as "<REPO>:<TAG>" or "sha256:<ID>"
        
        return a tuple (<new_image>, <enable_cache>), where:
         - 'new_image' is the image to be actually run
            - an image starting with {self.prefix} if successfully patched
            - a raw image id otherwise
         - 'enable_cache' is true if the cache CA is successfully installed
        """

        # inspect image
        img, image = await self.inspect_image(ctx, image)
        cached = self.cached_image(image)
        async with ctx.docker_request("GET", "images", cached, "json") as rep:
            if rep.status == 404:
                cac = None
            else:
                await self.expect_status("images", rep, 200)
                cac = await rep.json()

        # return true if image is already patched with the same digest
        def is_patched(js):
            if js:
                labels = js["Config"]["Labels"]
                if labels:
                    return labels.get(self.label_digest) == self.install_archive_digest
            return False
        # return true if we already tried to patch this image but failed
        def is_unpatchable(js):
            return self.unpatchable.get(js["Id"]) == self.install_archive_digest

        # Create the 'cached' tag directly from 'img' 
        #
        # (used when image is already patched or does not need to be patched)
        async def create_tag():
            if cac and cac["Id"] == img["Id"]:
                log.info("image %s (%s) is up-to-date", cached, cac["Id"])
            else:
                log.info("tag %s (%s) as %s", img["Id"], image, cached)
                repo, tag = cached.rsplit(":", 1)
                async with ctx.docker_request("POST", "images", img["Id"], "tag",
                        params = {"repo": repo, "tag": tag}) as rep:
                    await self.expect_status("tag", rep, 201)

        if is_patched(img):
            log.info("image %r is already patched", image)
            await create_tag()
            return cached, True

        elif is_unpatchable(img):
            log.info("image %r is known to be unpatchable", image)
            return img["Id"], False

        elif is_patched(cac) and cac["Parent"] == img["Id"]:
            log.info("patched image %r is already built", cached)
            return cached, True

        else:
            # patch the image

            #TODO: detect if this image is already being patched (by another
            #      request) and wait for the result

            # create the tmp_container for running the install-ca.sh script
            async def create_tmp_container() -> str:
                async with ctx.docker_request("POST", "containers", "create", json={
                    "Image": img["Id"],
                    "Entrypoint": [],
                    "Cmd": ["/tmp/ca/install-ca.sh"],
                    "User": "0:0",
                    }) as rep:
                    await self.expect_status("create", rep, 201)
                    cid = (await rep.json())["Id"]
                    log.info("created %s", cid)

                # copy the CA install archive into the container
                async with ctx.docker_request("PUT", "containers", cid, "archive",
                        params={"path": "/tmp"}, data=self.install_archive) as rep:
                    await self.expect_status("archive", rep, 200)

                return cid

            # commit the tmp container as and tag it as 'cached'
            #
            # note: this function reverts the config done in create_tmp_container()
            async def commit_tmp_container(cid):
                repo, tag = cached.rsplit(":", 1)
                entrypoint = img["Config"]["Entrypoint"]
                async with ctx.docker_request("POST", "commit", json={}, params={
                    "container": cid, "comment": "add ctproxy cache CA certificate",
                    "repo": repo, "tag": tag,
                    "changes": f"""
                        USER {json.dumps(img["Config"]["User"])}
                        CMD {json.dumps(img["Config"]["Cmd"])}
                        ENTRYPOINT {json.dumps(entrypoint) if entrypoint else []}
                        LABEL {self.label_digest}={json.dumps(self.install_archive_digest)}
                    """}) as rep:
                    await self.expect_status("commit", rep, 201)
                    committed_id = (await rep.json())["Id"]
                    log.info("committed %r -> %s", cached, committed_id)

            # cleanup the tmp container
            #
            # note: never fails
            async def remove_tmp_container(cid):
                log.info("remove %s", cid)
                try:
                    async with ctx.docker_request("DELETE", "containers", cid) as rep:
                        await self.expect_status("rm", rep, 204)
                except Exception:
                    log.exception("failed to delete image %s". cid)

            log.info("patch image %r as %r", image, cached)
            cid = None
            committed_id = None
            try:
                # create a tmp container
                cid = await create_tmp_container()

                # run the container
                async with ctx.docker_request("POST", "containers", cid, "start") as rep:
                    await self.expect_status("start", rep, 204)

                # wait for its termination
                async with ctx.docker_request("POST", "containers", cid, "wait") as rep:
                    await self.expect_status("wait", rep, 200)
                    code = (await rep.json())["StatusCode"]

                if not code:
                    # success!

                    # commit the new image
                    await commit_tmp_container(cid)
                    return cached, True
                else:
                    # install-ca script failed

                    # get the container logs
                    async with ctx.docker_request("GET", "containers", cid, "logs",
                            params={"stdout": 1, "stderr": 1}) as rep:
                        raw_logs = await rep.read() if rep.status==200 else b""

                    if code == 242 and (merge_docker_stream(raw_logs, (1,))
                            == b"IMAGE NOT SUPPORTED\n"):
                        # image is based on an unsupported os
                        log.warning("ctproxy DockerCacher script failed because image is not supported: %s",
                                image)
                    else:
                        # os is supported, but the script failed
                        # -> log the container output
                        logs = merge_docker_stream(raw_logs
                                ).decode(errors="replace")
                        log.error("ctproxy DockerCacher script error for image %s (exit %r)\n%s",
                                image, code, textwrap.indent(logs, "\t"))

                    # remember that we did not manage to patch this image
                    self.unpatchable[img["Id"]] = self.install_archive_digest

                    return img["Id"], False
            finally:
                # remove the tmp container
                if cid is not None:
                    await remove_tmp_container(cid)

        assert 0, "not reachable"


class CTproxy:
    # future to be set when server shutdown is requested
    _shutdown_requested: "Optional[asyncio.Future[None]]"

    # default docker network for the created containers
    network: str

    # event loop running this server
    loop: asyncio.AbstractEventLoop

    # ssl context if serving over TLS
    ssl_context: Optional[ssl.SSLContext]

    # listeners
    listeners: List[plugins.Listener]

    # plugins
    plugins: List[plugins.Plugin]

    # notification channel for broadcasting SIGHUP signals
    reloader: ConfigReloader

    # global ClientSession object for making arbitrary HTTP requests
    client_session: aiohttp.ClientSession

    def __init__(self, cfg: CfgDict) -> None:
        self._shutdown_requested = None

        self.loop = asyncio.get_event_loop()
        self.reloader = ConfigReloader()

        self.plugins = []
        def load_plugin(kind, cfg, **kw):
            plg_type = cfg.get("type", type=str, required=True)
            plg = plugins.new(kind, plg_type, self.reloader, cfg, **kw)
            self.plugins.append(plg)
            return plg

        # HTTP client session
        # 
        # we use DummyCookieJar to discard cookies (this session object is
        # shared by multiple users)
        self.client_session = aiohttp.ClientSession(loop=self.loop,
                cookie_jar=aiohttp.DummyCookieJar(),
                **plugins._DEFAULT_CLIENT_SESSION_TIMEOUT_ARGS)

        ######## common config ########
    
        # docker client plugin
        docker_client = load_plugin(plugins.DockerClient,
                cfg.get("docker_client", type=dict, required=True))

        def tls_listener_config(cfg: Optional[CfgDict]) -> Optional[ssl.SSLContext]:
            if cfg is None:
                return None

            client_ca = cfg.get("client_ca", type=str)
            ssl_context = ssl.create_default_context(
                    ssl.Purpose.CLIENT_AUTH, cafile=client_ca)

            # verify client certificates if a CA is provided
            ssl_context.verify_mode = (
                    ssl.VerifyMode.CERT_NONE
                    if client_ca is None
                    else ssl.VerifyMode.CERT_OPTIONAL)
            ssl_context.load_cert_chain(
                    certfile=cfg.get("cert", type=str, required=True),
                    keyfile =cfg.get("key",  type=str, required=True))
            return ssl_context

        ######## TODO: namespaces ########
#
#        # default docker network
#        self.network = cfg.get("network", type=str, default="bridge")
#
#        log.info("default docker network: %s", self.network)

        ######## HTTP/HTTPS cache ########

        def configure_cacher(cfg: CfgDict) -> Optional[DockerCacher]:
            cache = cfg.get("cache", type=dict)
            if cache is None:
                return None
            else:
                log.info("transparent HTTP/HTTPS cache enabled")
                return DockerCacher((), reloader=self.reloader, 
                    proxy_host = cache.get("proxy_host", type=str,
                        required=True),
                    label      = cache.get("label",      type=str,
                        default="ctproxy.cache"),
                    prefix     = cache.get("prefix",     type=str,
                        default="ctproxy_cache_"),
                    cache_var  = cache.get("cache_var",  type=str,
                        default="CTPROXY_CACHE"),
                    )

        cacher = configure_cacher(cfg)

        ######## policies ########

        global_policies = [load_plugin(plugins.Policy, c)
                    for c in cfg.get("policies", type=list, default=[])]

        ######## namespaces ########

        def configure_namespaces(cfg: CfgDict) -> Optional[DockerNamespacesHandler]:

            # HTTP headers to be forwarded to the registry by the reverse proxy
            FORWARDED_HEADERS = ("Accept", "Authorization", "Content-Length", "Content-Range",
                    "Content-Type", "Date", "Docker-Content-Digest",
                    "Docker-Distribution-Api-Version", "Docker-Upload-UUID",
                    "Location", "Range", "Server", "User-Agent",
                    "Www-Authenticate")

            nscfg = cfg.get("namespaces")
            if nscfg is None:
                return None, ()

            # namspace handler (that will handle the engine API requests)
            namespaces_handler = DockerNamespacesHandler(nscfg)
            log.info("namespaces enabled (using *%s:%s)",
                    namespaces_handler.base_registry_domain,
                    namespaces_handler.base_registry_port)

            # registry listener (that will handle the registry API requests)
            handler = namespaces_handler.make_registry_request_handler()
            handler = HttpReverseProxy([handler],
                    forwarded_headers=FORWARDED_HEADERS,
                    client_session=self.client_session)
            app = aiohttp.web.Application()
            app.router.add_route("*", "/{path:.*}",
                    lambda req: self.handle_http_request(req, handler))
            ssl_context = tls_listener_config(nscfg.get("tls", type=dict))
            listener = load_plugin(plugins.Listener,
                    CfgDict({"type": "tcp", "bind": nscfg.get("bind", type=str)}),
                    app=app, app_handler=app.make_handler(), ssl_context=ssl_context)
            return namespaces_handler, (listener,)

        namespaces_handler, registry_listener = configure_namespaces(cfg)

        ######## listeners configuration ########

        def configure_listener(lcfg: CfgDict) -> plugins.Listener:

            # HTTP headers to be forwarded to the engine by the reverse proxy
            FORWARDED_HEADERS = ("Accept", "Api-Version", "Authorization",
                    "Connection", "Content-Type", "Date",
                    "Docker-Experimental", "Ostype", "Server", "Upgrade",
                    "User-Agent", "X-Registry-Auth",
                    )

            # nginx backend
            as_nginx_backend = lcfg.get("as_nginx_backend", type=bool, default=False)

            # tls config
            ssl_context = tls_listener_config(lcfg.get("tls", type=dict))

            # auth config
            auths = [load_plugin(plugins.Auth, c)
                    for c in lcfg.get("auth", type=list) or ()]

            # policies config
            local_policies = [load_plugin(plugins.Policy, c)
                    for c in lcfg.get("policies", type=list, default=[])]

            # create the aiohttp handler for this listener
            handlers = []
            if auths:
                handlers.append(DockerAuth(auths))

            # global policies are applied first
            # (this way, when a user applies a restrictive policy, there will
            #  be a strong guarantee that this policy is applied everywhere)
            handlers.extend(global_policies)
            handlers.extend(local_policies)
            if auths and namespaces_handler is not None:
                handlers.append(namespaces_handler)
            if cacher is not None:
                handlers.append(cacher)

            handlers = [DockerProxy(handlers, docker_client=docker_client)]
            http_handler = HttpReverseProxy(handlers,
                    forwarded_headers=FORWARDED_HEADERS,
                    client_session=self.client_session)


            # aiohttp application
            app = aiohttp.web.Application()

            app.router.add_route("*", "/{version:(?:v[0-9.]+/)?}{path:.*}", 
                    lambda request: self.handle_http_request(
                        request, http_handler))

            app_handler = app.make_handler(access_log=log,
                    access_log_format = '%a - %{X-User}o %t %Tfs "%r" %s %b "%{Referrer}i" "%{User-Agent}i"')

            # log the listener config
            params = []
            if ssl_context is not None:
                params.append("tls")
            for name in "allow_user", "allow_group":
                val = lcfg.get("name")
                if val is not None:
                    params.append(f"{name}={val}")
            if lcfg.get("allow_other"):
                params.append("allow_other")
            params.append("auth=[%s]" % ",".join(a.__class__.__name__ for a in auths))
            log.info("listen on %s (%s)",
                    lcfg.get("bind"), ",".join(params))

            return load_plugin(plugins.Listener, lcfg,
                    ssl_context=ssl_context, app=app, app_handler=app_handler)


        self.listeners = registry_listener + tuple(
                configure_listener(lcfg)
                for lcfg in cfg.get("listeners", type=list, required=True))

    async def handle_http_request(self, request: Request,
            http_handler: HttpRequestHandler) -> StreamResponse:
        with Context(request) as ctx:
            response = await http_handler.handle_request(ctx)
            if response is None:
                raise RuntimeError("no response")
            return response


    def shutdown(self) -> None:
        log.info("shutdown requested")
        try_set_result(self._shutdown_requested, None)

    def reload(self) -> None:
        log.info("received SIGHUP")
        self.reloader.reload()


    async def run(self) -> None:
        assert self._shutdown_requested is None, "run() must not be called multiple times"

        self._shutdown_requested = asyncio.Future()
        log.info("starting")
        async with AsyncExitStack() as stack:
            for plugin in self.plugins:
                await stack.enter_async_context(plugin)

            try:
                await asyncio.gather(
                    *(l.create_server() for l in self.listeners))
                log.info("server ready")
                try:
                    import sdnotify
                    sdnotify.SystemdNotifier().notify("READY=1")
                except ModuleNotFoundError:
                    log.warning("systemd not notified (python package sdnotify not found)")
                await self._shutdown_requested
            finally:
                log.info("shutting down")
                # initiate the shutdown
                # (close all listening sockets and stop accepting new requests)
                await asyncio.gather(*(l.shutdown() for l in self.listeners))

                # finish the shutdown
                # (wait until all pending requests are completed)
                async def finish_shutdown():
                    await asyncio.gather(*(l.finish_shutdown() for l in self.listeners))
                    await self.client_session.close()
                finish = asyncio.ensure_future(finish_shutdown())
                try:
                    await asyncio.wait_for(asyncio.shield(finish), 0.5)
                except asyncio.TimeoutError:
                    # if shutdown is not finished after 0.5 second, then we
                    # continue as a background process to finish serving these
                    # requests
                    # NOTE: this requires using "KillMode=process" in the systemd unit
                    pid = os.fork()
                    if pid:
                        count = sum(l.active_connections() for l in self.listeners)
                        log.info("we still have %d active connection(s), will continue as pid %d until all pending requests are completed", count, pid)
                        os._exit(0)
                    else:
                        await finish

        log.info("shutdown complete")

    '''
    async def handle_not_supported(self, request: Request) -> Response:
        quotes = "There it is!", "The Bridge of Death!", "Oh, great.", "Look!  There's the old man from scene twenty-four!", "What is he doing here?", "He is the keeper of the Bridge of Death.  He asks each traveler five questions", "Three questions.", "Three questions.  He who answers the five questions", "Three questions.", "Three questions may cross in safety.", "What if you get a question wrong?", "Then you are cast into the Gorge of Eternal Peril.", "Oh, I won't go.", "Who's going to answer the questions?", "Sir Robin!", "Yes?", "Brave Sir Robin, you go.", "Hey!  I've got a great idea.  Why doesn't Launcelot go?", "Yes.  Let me go, my liege.  I will take him single-handed.  I shall make a feint to the north-east that s--", "No, no.  No.  Hang on!  Hang on!  Hang on!  Just answer the five questions", "Three questions.", "Three questions as best you can.  And we shall watch... and pray.", "I understand, my liege.", "Good luck, brave Sir Launcelot.  God be with you.", "Stop!  Who would cross the Bridge of Death must answer me these questions three, ere the other side he see.", "Ask me the questions, bridgekeeper.  I am not afraid.", "What is your name?", "My name is Sir Launcelot of Camelot.", "What is your quest?", "To seek the Holy Grail.", "What is your favorite color?", "Blue.", "Right.  Off you go.", "Oh, thank you.  Thank you very much.", "That's easy!", "Stop!  Who approacheth the Bridge of Death must answer me these questions three, ere the other side he see.", "Ask me the questions, bridgekeeper.  I'm not afraid.", "What is your name?", "Sir Robin of Camelot.", "What is your quest?", "To seek the Holy Grail.", "What is the capital of Assyria?", "I don't know that!  Auuuuuuuugh!", "Stop!  What is your name?", "Sir Galahad of Camelot.", "What is your quest?", "I seek the Grail.", "What is your favorite color?", "Blue.  No yel-- auuuuuuuugh!", "Hee hee heh.  Stop!  What is your name?", "It is Arthur, King of the Britons.", "What is your quest?", "To seek the Holy Grail.", "What is the air-speed velocity of an unladen swallow?", "What do you mean?  An African or European swallow?", "Huh?  I-- I don't know that!  Auuuuuuuugh!", "How do know so much about swallows?", "Well, you have to know these things when you're a king, you know."
        num = random.randrange(0, len(quotes))
        return error_response(404, "not supported [%s]" % quotes[num])

    ######################################################################
    ##                                                                  ##
    ##  ping endpoint (server discovery)                                ##
    ##                                                                  ##
    ######################################################################
    async def handle_ping(self, request: Request) -> Response:
        return Response(status=200, body=b"OK", headers={
            "API-Version":  API_VERSION,
            "Docker-Experimental": "false",
            })
'''
