#!/usr/bin/python3

import asyncio
import base64
import codecs
import contextlib
import json
import logging
import os
import re
import tempfile
import time
import typing
from   typing import Callable, Dict, Iterable, List, Mapping, MutableMapping, \
                     Optional, Set, Tuple

import aiohttp
from   aiohttp.web import Request, Response, StreamResponse, json_response
import requests.utils

import ctproxy
from   ctproxy import CfgDict
from   .bases import ErrorResponse, HttpRequestHandler
from   .dockerfile import DockerfilePatcher
from   .utils import coerce
from . import ssl

log = logging.getLogger("namespaces")

def forward_headers(dct: Mapping[str,str]) -> Dict[str,str]:
    return {key: dct[key]
            for key in ("Authorization", "User-Agent") if key in dct}


_routes = {}
def route(*command: str):
    def wrapper(func):
        assert command not in _routes, "duplicated route"
        _routes[command] = func
        return func
    return wrapper


class DockerRegistryRequestHandler(HttpRequestHandler):

    def __init__(self, demangle_func: Callable[[str],
                Optional[Tuple[str, str, Optional[str]]]]) -> None:
        super().__init__(())
        self.demangle_registry_name = demangle_func
        self._client_ssl_context = ssl.create_default_context()

    async def handle_request(self, ctx: "Context") -> Optional[StreamResponse]:

        if not ctx.path.startswith("/v2/"):
            # not a registry request
            raise ErrorResponse(404, "not found")

        # demangle the hostname and redirect to the real registry
        try:
            raw_host, *k = ctx.host.split(":", 1)
            raw_port = int(k[0]) if k else None

            prefix, host, port = self.demangle_registry_name(raw_host)
        except (TypeError, ValueError):
            return Response(status=400, reason="Malformatted Registry")

        # redirect to the real registry
        orig = ctx.scheme, raw_host, raw_port
        dest = "https", host, port
        ctx.proxy_redirect = (dest, orig)
        ctx.scheme = "https"
        ctx.client_ssl_context = self._client_ssl_context
        ctx.host = host if port is None else f"{host}:{port}"
        log.info("redirected to %s://%s%s", ctx.scheme, ctx.host, ctx.path)
        return None


class DockerNamespacesHandler(HttpRequestHandler):

    # base domain name where our registry endpoint (/v2/) is reachable
    # This variable must start with a dot (eg:  ".DOMAIN")
    base_registry_domain: str
    # tcp port where our registry endpoint (/v2/) is reachable
    base_registry_port: int

    # regex patterns for demangling registry names in a stream
    _base_registry_demangle_pattern: str
    _base_registry_demangle_text_pattern: str

    # lifetime in seconds for anonymous images (created without a tag), after
    # this delay the image is no longer visible by its owner
    anonymous_image_lifetime: int

    # dict of the anonymous docker images
    #
    # key:    (prefix, image_id)
    # value:  monotonic_timestamp     (expiration time of the image)
    #
    # FIXME: we need to place a tag on each anonymous image because deleting
    # them cause some side effects:
    #   - when deleting by tag, dockerd automatically deletes the image if it
    #     has no other tags
    #   - when deleting by id, dockerd automatically untag the image if it has
    #     a single tag
    #
    # FIXME: anynonymous images need to be removed when they expire
    #
    anonymous_images: Dict[Tuple[str, str], float]


    # the container label used for saving its ownership (by storing the prefix
    # of the namespace associated to the container)
    label_prefix: str

    def __init__(self, cfg: CfgDict) -> None:
        """Initialise the namespaces handler

        The config dict may contain three 

        - `base_registry` is the local domain appended to mangled names + an
          optional port number. The string  is formatted as "DOMAIN[:PORT]" and
          the default port is 443.
          Mangled names will look like "xxxxxxxxxxxxxxx.DOMAIN:PORT".

        - `label_prefix` (optional) is the container label used for saving its
          ownership (by storing the prefix of the namespace associated to the
          container), default is 'fr.inria.ci.ctproxy.prefix'

        - `anonymous_image_lifetime` (optional) lifetime in seconds of untagged
          images created by /commit or /build, after this delay the image is no
          longer visible by its owner, default is 3600s
        """
        def parse_registry(txt: str) -> Tuple[str, int]:
            try:
                host, port = re.match(r"([0-9a-z-][0-9a-z.-]*)(?::(\d+))?\Z",
                        txt, re.I).groups()
                return ("."+host), (443 if port is None else int(port))
            except Exception:
                raise ValueError("malformatted registry %r" % txt)

        # suffix for mangling docker image names (DOMAIN[:PORT])
        domain, port = parse_registry(cfg.get("base_registry", type=str, required=True))
#        if port != 443:
#            raise ValueError("registry listener with port!=443 is not supported")
        self.base_registry_domain = domain
        self.base_registry_port = port

        # prepare the regexes for demangling registry names
        self._base_registry_demangle_pattern = (
                r"([a-zA-Z0-9_-]+%s):%s(/(?:library/)?)(.*)\Z" % (
                re.escape(domain), port))
        self._base_registry_demangle_text_pattern = r"((?://)?)\b([a-zA-Z0-9_-]+%s)(?::%s)?((?:/(?:library/)?)?)" % (
                re.escape(domain), port)
        
        # label storing the ns prefix for each container
        self.label_prefix = cfg.get("label_prefix", type=str,
                default="fr.inria.ci.ctproxy.prefix")

        # lifetime of untagged images
        self.anonymous_image_lifetime = cfg.get("anonymous_image_lifetime",
                type=int, default=3600)

        self.anonymous_images = {}

    def make_registry_request_handler(self) -> DockerRegistryRequestHandler:
        return DockerRegistryRequestHandler(self.demangle_registry_name)

    def mangle_image_name(self, ctx: "Context", presented_name: str) -> str:
        """Mangle an image name
        
        Note:
         - the presented_name may contain a tag
         - may raise 400/403 if the image name is malformatted
        """
        split = ctx.user.split_prefix(presented_name)
        if split is None:
            # this may happen if the name does not contain a dot
            raise ErrorResponse(403, "name not allowed: %r" % presented_name)

        # `name` is the image name within the namespace
        prefix, name = split

        # parse the image name to extract the registry and make it fully qualified
        #
        # note: grammar available at https://github.com/moby/moby/blob/master/vendor/github.com/docker/distribution/reference/reference.go

        mo = re.match(r"([a-zA-Z0-9.-]+)(?::(\d+))?((?:/[^:]*)?)((?::[^:]+)?)\Z", name)
        if not mo:
            raise ErrorResponse(400, "malformatted repository: %r" % name)

        domain, port, path, tag = mo.groups()
        if port is None and "." not in domain:
            # implicit reference to the official registry

            # -> make it explicit
            # NOTE: docker folks are morons, why the hell do they prefix
            # repository names on the official registry with "library/" but
            # only when it does not contain a slash ?
            # Examples:
            #   "docker pull foo"           pulls library/foo
            #   "docker pull foo/bar"       pulls foo/bar
            #   "docker pull foo/bar/baz"   pulls foo/bar/baz
            if path:
                path = "".join(("/", domain, path))
            else:
                path = "/library/" + domain
            # -> replace with the real host
            domain = "registry-1.docker.io"

        return f"{self.mangle_registry_name(prefix, domain, port)}{path}{tag}"

    # Translate the domain so that the request is sent to ourselves
    # 
    # The translated hostnames contains
    #   - the name of the project (prefix)
    #   - the orignal registry name mangled by replacing {-,.} with {-z,-y}
    #   - the registry port number
    # so that:
    #  - every original domain+user is effectively mapped to a different domain
    #    (because the authentication scope in the docker registry API is the
    #    hostname)
    #  - all the translated domains can be covered by a single X.509
    #    certificate (using a wildcard)
    #  - the translation is bijective and stateless
    #
    # FIXME: this will work fine except for very long registry names. RFC 1035
    #        allows domain names up to 255 bytes but length of each label
    #        (parts between dots) is limited to 63 bytes.
    # FIXME: this leaks information to the target registry (project name)
    #
    def mangle_registry_name(self, prefix: str, host: str, port: Optional[str]) -> str:
        """Mangle a registry name
        
        input:  (prefix, host, port)
        output: mangled registry with port number

        NOTE: `port` is a string or None
        """
        assert re.match(r"[A-Za-z0-9-]+\.\Z", prefix)
        assert re.match(r"[A-Za-z0-9.-]+\Z", host)
        assert port is None or re.match(r"\d+\Z", port)

        mangled = ("%s_%s_%s" % (prefix[:-1], host, port or "")
                ).replace("-", "-z").replace(".", "-y").replace("_", "-x")
    
        return f"{mangled}{self.base_registry_domain}:{self.base_registry_port}"

    def demangle_registry_name(self, mangled_registry: str) -> Optional[
            Tuple[str, str, Optional[str]]]:
        """Demangle a registry name
        
        input:  mangled registry (without port number)
        output: (prefix, host, port)

        NOTE: `port` is a string or None
        """

        if not mangled_registry.endswith(self.base_registry_domain):
            return None

        mangled = mangled_registry[:-len(self.base_registry_domain)]

        if re.search(r"-(?:[^zyx]|\Z)", mangled):
            return None

        plain = mangled.replace('-x', '_').replace('-y', '.').replace('-z', '-')

        mo = re.match(r"([A-Za-z0-9-]+)_([A-Za-z0-9.-]+)_(\d*)?\Z", plain)
        if mo is None:
            return None

        namespace, host, port = mo.groups()
        return (namespace + "."), host, (port or None)


    def demangle_text(self, text: str) -> str:
        """Demangle image names within a text

        This function is for demangling the UI output
        """
        def do_demangle(mo: typing.re.Match[str]) -> str:
            # demangle the registry name
            head, mangled, tail = mo.groups()
            dem = self.demangle_registry_name(mangled)
            if (dem is None) or head or not tail:
                # do nothing if unable to demangle or if the text does not look
                # like a docker image (i.e. looks a url (head is "//") or is
                # not followed with an image name)
                return mo.group(0)

            prefix, host, port = dem
            # strip the registry name if using the official registry
            if host == "registry-1.docker.io":
                return ""
            else:
                return host + ((":" + port) if port else "") + tail

        return re.sub(self._base_registry_demangle_text_pattern,
                do_demangle, text)


    def demangle_response(self, ctx: "Context", fields: Tuple[str, ...]) -> None:
        """Demangle the images in the chunked JSON response

        This function adds a handler to `ctx.json_chunked_response_filters`,
        which try (to its best) to demangle registry names found in the
        streamed response returned to the user.

        It assumes that chunks are dictionnaries and will attempt to demangle
        names found in the fields listed in `fields`.
        """

        @ctx.json_chunked_response_filters.append
        def demangle(msg: object) -> None:
            if isinstance(msg, dict):
                for key in fields:
                    val = msg.get(key)
                    if key == "errorDetail":
                        if isinstance(val, dict):
                            message = val.get("message")
                            if isinstance(message, str):
                                val["message"] = self.demangle_text(message)
                    elif isinstance(val, str):
                        msg[key] = self.demangle_text(val)


    def demangle_image_name(self, ctx: "Context", image: str) -> Optional[str]:
        """Demangle an image name

        `image` may optionally contain a tag
        
        return None if the image is an invalid name or if this image is not
        visible in this context
        """

        mo = re.match(self._base_registry_demangle_pattern, image)
        if mo is None:
            return None

        host, lib, tail = mo.groups()

        dem = self.demangle_registry_name(host)
        if dem is None:
            return None
        prefix, host, port = dem

        # strip the registry name if using the official registry
        if host == "registry-1.docker.io":
            demangled = tail
        else:
            demangled = "".join((host, ((":" + port) if port else ""), lib, tail))

        return ctx.user.join_prefix(prefix, demangled)


    def demangle_image_list(self, ctx: "Context", images: List[str]
            ) -> List[str]:
        """Demangle a list of images (works also with digests)

        NOTE: images not visibles in this context are filtered out

        return a new list
        """
        return list(filter(bool,
            (self.demangle_image_name(ctx, i) for i in (images or ()))))


    def demangle_image_repotags_repodigests(self, ctx: "Context",
            img: Dict[str, object], monotonic_ts: Optional[float] = None
            ) -> bool:
        """Demangle the tags/digests in an image descriptor
        
        `img`           is a dict describing an image (as returned by the
                        docker API endpoints GET /images/json and by
                        GET/images/xxxx/json)

        `monotonic_ts`  is the reference time (if not provided, the
                        function uses the current time)

        This function demangles the dict values for "RepoDigests" and
        "RepoTags" and filters out any name that is not visible in this
        context.

        It returns True if the image is visible in this context (either
        because it has at least one remaining tag/digests or because it is
        listed in the `anonymous_images` dict).
        """

        img["RepoDigests"] = self.demangle_image_list(ctx, img.get("RepoDigests"))
        img["RepoTags"   ] = self.demangle_image_list(ctx, img.get("RepoTags"   ))

        have_digests = bool(img["RepoDigests"])
        have_tags    = bool(img["RepoTags"   ])

        return (have_digests or have_tags
                or self.have_anonymous_image(ctx, img["Id"], monotonic_ts))


    def add_anonymous_image(self, ctx: "Context", image_id: str) -> None:
        # FIXME: for unmangled users (eg: admin) users, the prefix will not be set
        #        -> need to derive it from the container
        prefix = ctx.user.get_prefix()
        if prefix is not None:
            self.anonymous_images[prefix, image_id] = (
                    time.monotonic() + self.anonymous_image_lifetime)

    def del_anonymous_image(self, ctx: "Context", image_id: str) -> None:
        prefix = ctx.user.get_prefix()
        if prefix is not None:
            self.anonymous_images.pop((prefix, image_id), None)

    def have_anonymous_image(self, ctx: "Context", image_id: str,
                monotonic_ts: Optional[float] = None) -> bool:
        """Check if an image is a known anonymous image in this context

        `image_id` is a full image id (eg:   "sha256:0a465b17c89...)

        `monotonic_ts` is the reference time (if not provided, the function
        uses the current time)

        return True if the image is visible
        """
        # FIXME: unmangled users (eg: admin) users will not see anything here
        return bool(
                self.anonymous_images.get((ctx.user.get_prefix(), image_id), 0.0)
                > (monotonic_ts or time.monotonic()))
    

    def mangle_resource_name(self, ctx: "Context", name: str) -> str:
        """Mangle a resource name

        This apply to all docker resources (containers, networks, volumes),
        except images (which have their own mangling scheme).

        The mangling scheme is user-dependent
        """
        result = ctx.user.mangle_resource_name(name)
        if result is None:
            raise ErrorResponse(403, "name not allowed: %r" % name)
        return result


    def demangle_resource_name(self, ctx: "Context", raw: str) -> Optional[str]:
        """Demangle a resource name

        This apply to all docker resources (containers, networks, volumes),
        except images (which have their own mangling scheme).

        return None if this resource has an invalid name or if it is not
        visible in the user owning this context
        """
        return ctx.user.demangle_resource_name(raw)


    def demangle_resource_list(self, ctx: "Context", lst: List[str]) -> List[str]:
        """Demangle a list of resources

        NOTE: resources not visibles in this context are filtered out

        return a new list
        """
        return list(filter(bool, map(ctx.demangle_resource_name, (lst or ()))))


    def demangle_container_name(self, ctx: "Context", name: str) -> Optional[str]:
        """Demangle a container name (as presented in containers/json)

        (name looks like: /CONTAINER_NAME[/LINK_NAME])

        return the demangled name or None if unsuccessful

        NOTE: names autogenerated by docker ([a-z]+_[a-z]+) are returned as is
        """
        first, tail = re.match(r"/([^/]+)(.*)\Z", name).groups()
        demangled = self.demangle_resource_name(ctx, first)
        if demangled:
            # was a mangled name
            return "".join(("/", demangled, tail))
        elif re.match(r"[a-z]+_[a-z]+\Z", first):
            # was an autogenerated name
            return name
        return None


    async def mangle_dockerfile(self, ctx: "Context", content: str) -> str:
        """Mangle repository names in a dockerfile

            `content`       content of the dockerfile
            `ctx`           current request context
        """
        lines = content.splitlines()

        # remove comments
        lines = [l for l in lines if not re.match(r"\s*#", l)]

        # merge lines with a continuation character
        # FIXME: future versions docker allow using a different escape character with:
        #       # escape=`
        lines.append("") # avoid that continuation of the last line raises an exception
        for i in range(len(lines)-1, -1, -1):
            mo = re.match(r"\A(.*)\\[ \t]*\Z", lines[i])
            if mo:
                lines[i] = mo.group(1) + lines.pop(i+1)

        # parse all FROM lines and mangle the image names
        for i, line in enumerate(lines):
            mo = re.match(r"\s*FROM\s+(\S+)((?:\s.*)?)", line, re.I)
            if mo:
                #TODO support build args
                #NOTE only the ARGs before the first FROM have to be taken into account:
                #     https://docs.docker.com/engine/reference/builder/#understand-how-arg-and-from-interact

                # mangle the image name
                mangled, _ = await self.find_image(ctx, mo.group(1),
                        allow_unknown=True)

                lines[i] = "FROM %s%s" % (mangled, mo.group(2))

        lines.append("")
        return "\n".join(lines)


    async def mangle_network_mode(self, ctx: "Context", mode: str) -> str:
        """Mangle the NetworkMode in the container configuration

        - predefined values (default,none,host,bridge) are left as is
        - container references (container:xxxxx) are resolved to the container
          id or mangled (depending on the situation)
        - otherwise the network name is mangled
        """
        if mode in ("default", "none", "host", "bridge"):
            return mode

        mo = re.match(r"container:(.+)\Z", mode)
        if mo:
            # container:xxxxxxxx
            #
            # The docker engine resolves the reference to other container at
            # start time (rather than creation time).
            #
            # Since we cannot modify the config after creation, we have to
            # resolve the name to a container id immediatley when it is not
            # possible to provide a mangled name to docker (i.e. when the ref
            # is a container id or an autogenerated name)
            #
            ctr = mo.group(1)
            cid = None
            if re.match(r"[a-z]+_[a-z]+\Z", ctr):
                # possibly an auto-generated name
                cid = await self.find_container(ctx, ctr)
            elif re.match(r"[0-9a-f]+\Z", ctr):
                # possibly a container id
                cid = await self.find_container(ctx, ctr)
                if not cid.startswith(ctr):
                    cid = None
            return "container:" + (cid or self.mangle_resource_name(ctx, ctr))
        else:
            # user network
            return self.mangle_resource_name(ctx, mode)


    async def find_container(self, ctx: "Context", ctr: str) -> str:
        """Find a container (belonging to the current user)

        `ctr` is an unmangled name (as presented to the user) or a container id

        If the container is found and its label `self.label_prefix` is equal to
        our prefix then we return the container id.

        raise ErrorResponse 404 if invalid or not found
        """
        
        async def find(name: str) -> Optional[Tuple[str, str]]:
          try:
            async with ctx.docker_request("GET", "containers", "TARGET",
                    "json", target=name) as resp:
                if resp.status == 200:
                    js = coerce(dict, await resp.json())
                    # return the id and name but only if the container belongs to us
                    prefix = coerce(dict, coerce(dict, js["Config"])["Labels"]
                            ).get(self.label_prefix)
                    if prefix and ctx.user.can_use_prefix(prefix):
                        return js["Id"], js["Name"][1:]
            return None
          except:
              log.exception("exc")
              raise

        # lookup by raw name and by mangled name (concurrently)
        raw, mangled = await asyncio.gather(find(ctr),
                find(self.mangle_resource_name(ctx, ctr)), 
                return_exceptions=True)

        if isinstance(mangled, tuple):
            # match by name (mangled)
            return mangled[0]

        if isinstance(raw, tuple) and (
                # match by id
                raw[0].startswith(ctr)
                # match by name (autogenerated by docker)
                or (raw[1] == ctr and re.match("[a-z]+_[a-z]+\Z", ctr))):

            return raw[0]

        # we return 404 even if the id exists (and belongs to another user)
        # so that:
        #  - we do not leak any info
        #  - « docker inspect » works correctly
        raise ErrorResponse(404, "container not found: %r" % ctr)


    async def find_exec(self, ctx: "Context", exid: str) -> str:
        """Find and validate the exec id

        The validation consists of ensuring that the underlying container
        id is valid in this context (see find_container)

        raise ErrorResponse 404 if invalid or not found
        """

        # inspect the exec to get its container id
        async with ctx.docker_request("GET", "exec", "TARGET", "json",
                target=exid) as resp:
            if resp.status == 200:
                js = await resp.json()

                # validate the container id
                # raise ErrorResponse(404) if invalid
                await self.find_container(ctx, js["ContainerID"])

                # return the real exec id
                return js["ID"]

        # we return 404 even if the id exists (and belongs to another user)
        # so that:
        #  - we do not leak any info
        #  - « docker inspect » works correctly
        raise ErrorResponse(404, "exec not found: %r" % exid)


    async def find_image(self, ctx: "Context", img: str, *,
            allow_unknown=False) -> Tuple[str, Optional[dict]]:
        """Find an image (belonging to the current user)

        `img` is an unmangled name (as presented to the user) or an image id

        We return an image if one of the conditions is met:
          - `img`, after mangling, resolves to an existing image
          - `img` is the id of an image with at least one tag mangled with the
            user prefix
          - `img` is the id of an image listed in the `anonymous_images`
          - `allow_unknown` is True and `img` is a well-formatted name

        The function returns a tuple (target, json) where
          - `target` is a mangled name or the resolved image id
          - `json` is the json details of the image (or None if not found)

        raise ErrorResponse 404 if:
         - `img` is a malformatted image name
         - `img` is not found and `allow_unknown` is false
        """

        async def inspect(img: str) -> Optional[dict]:
            async with ctx.docker_request("GET", "images", "TARGET", "json",
                    target=img) as resp:
                if resp.status == 200:
                    js = coerce(dict, await resp.json())
                    # demangle the tags & digests and ensure the image is
                    # visible in the current context
                    if self.demangle_image_repotags_repodigests(ctx, js):
                        return js
                return None

        # find by name (mangled)
        try:
            mangled = self.mangle_image_name(ctx, img)
        except ErrorResponse:
            # This may happen if `img` is not a well-formatted image name.
            #
            # We prefer returning a 404 instead of propagating the error
            # 400 because this is the behaviour of dockerd (and this is what
            # « docker inspect » expects)
            mangled = None
        else:
            js = await inspect(mangled)
            if js is not None:
                return mangled, js

        # find by id (if the requested img looks like an id)
        mo = re.match(r"(?:sha256:)?([0-9a-f]+)\Z", img)
        if mo:
            js = await inspect(img)
            if js is not None:
                # does the id match our request
                if js["Id"].startswith("sha256:"+mo.group(1)):
                    return js["Id"], js
        
        # image not found
        if allow_unknown and mangled is not None:
            # return the mangled name anyway
            return mangled, None
        else:
            # we return 404 even if the image exists (and belongs to another user)
            # so that:
            #  - we do not leak any info
            #  - « docker inspect » works correctly
            raise ErrorResponse(404, "image not found: %r" % img)


    async def handle_request(self, ctx: "Context") -> Optional[StreamResponse]:
        if ctx.user is None:
            # no identified user implies no mangling
            return None
        else:
            handler = _routes.get(ctx.docker_command,
                    DockerNamespacesHandler.handle_not_supported)
            return await handler(self, ctx)

    ################ commands that do not need mangling ###########

    @route("GET", "_ping")
    @route("GET", "images", "search")
    @route("GET", "info")
    @route("GET", "version")
    async def handle_pass_through(self, ctx: "Context") -> Optional[StreamResponse]:
        return None

    ################ explicitely unsupported commands #############
    #
    # image get (docker save) and image load (docker load) are not supported
    # because the index in the tarball needs to be demangled
    @route("GET", "images", "TARGET", "get")
    @route("GET", "images", "get")
    @route("POST", "images", "load")
    # prune endpoints not supported because they would remove resources
    # belonging to other users
    @route("POST", "build/prune")
    @route("POST", "containers", "prune")
    @route("POST", "images", "prune")
    @route("POST", "volumes", "prune")
    async def handle_not_supported(self, ctx: "Context") -> Optional[StreamResponse]:
        # NOTE: we return 403 instead of 404 because the docker would interpret
        # a 404 as 'resource not found' and would not display our error message
        raise ErrorResponse(403, "unsupported api endpoint: %s %s"
                % (ctx.request.method, ctx.request.url.path))


    ################ auth endpoint ################################

    @route("POST", "auth")
    async def handle_auth(self, ctx: "Context") -> Optional[StreamResponse]:
        js = await ctx.json()

        # mangle registry name
        registry = js["serveraddress"]
        split = ctx.user.split_prefix("")
        if split is None:
            raise ErrorResponse(403, "/auth requests not allowed for unmangled users")
        prefix = split[0]
        host, port = re.match(r"([^:]*)(?::(\d+))?\Z", registry).groups()
        js["serveraddress"] = self.mangle_registry_name(prefix, host, port)

        # forward the request
        async with ctx.docker_request("POST", "auth", json=js) as resp:
            return json_response(status=resp.status, text=await resp.text())

    ################ image endpoints ##############################

    @route("POST", "images", "create")
    async def handle_image_create(self, ctx: "Context") -> Optional[StreamResponse]:

        fromSrc = ctx.params.get("fromSrc")
        if fromSrc is not None:
            #### image import ####

            if "fromImage" in ctx.params:
                raise ErrorResponse(400, "must provide either 'fromImage' or 'fromSrc' but not both")

            if fromSrc != "-":
                # FIXME: this is a safety precaution to prevent the users
                # sending arbitrary HTTP GET requests into the ci-admin
                # network. Perhaps we could relax this check.
                raise ErrorResponse(403, "URL imports not supported")

            ctx.params["repo"] = self.mangle_image_name(ctx, ctx.params["repo"])
        else:
            #### image pull ####
            
            # TODO: support pull by digest
            
            ctx.params["fromImage"] = self.mangle_image_name(
                    ctx, ctx.params["fromImage"])

            self.demangle_response(ctx,
                    ("status", "message", "error", "errorDetail"))

        return None


    @route("POST", "images", "TARGET", "push")
    async def handle_image_push(self, ctx: "Context") -> Optional[StreamResponse]:

        ctx.docker_target = self.mangle_image_name(ctx, ctx.docker_target)

        self.demangle_response(ctx,
                ("status", "error", "errorDetail"))
        return None


    @route("GET", "images", "json")
    async def handle_image_list(self, ctx: "Context") -> Optional[StreamResponse]:
        # mangle the filter
        filters = json.loads(ctx.params.get("filters", "{}"))
        reference = filters.get("reference")
        if reference:
            # NOTE: the API spec says that this field is a str, but dockerd
            #       engine only accepts lists or dicts. On the client side
            #       docker sends a dict and docker-py sends a list.
            if isinstance(reference, list):
                reference = [self.mangle_image_name(ctx, r) for r in reference]
            else:
                reference = {self.mangle_image_name(ctx, k):v
                        for k,v in reference.items()}
            filters["reference"] = reference
            ctx.params["filters"] = json.dumps(filters)

        # forward the request
        async with ctx.docker_request("GET", "images", "json",
                params=ctx.params) as resp:

            if resp.status != 200:
                return json_response(text=await resp.text(), status=resp.status)

            now = time.monotonic()
            images = []
            for img in await resp.json():
                # demangle and filter the image tags/digests
                # include the image in the list only if it is visible in this context
                if self.demangle_image_repotags_repodigests(ctx, img, now):

                    # The official docker client fails when the RepoTags or
                    # RepoDigests lists are empty, it expects at least the
                    # "<none>:<none>" tag or "<none>@<none>" digest to be
                    # present, otherwise the image will not be displayed at all
                    # (yeah this is messy)
                    if not img["RepoTags"]:
                        img["RepoTags"].append("<none>:<none>")
                    if not img["RepoDigests"]:
                        img["RepoDigests"].append("<none>@<none>")

                    images.append(img)

            return json_response(images, status=resp.status)


    @route("GET", "images", "TARGET", "json")
    async def handle_image_inspect(self, ctx: "Context") -> Optional[StreamResponse]:

        # resolve and validate the image name
        img, js = await self.find_image(ctx, ctx.docker_target)
        assert js is not None
        return json_response(js)


    @route("GET", "images", "TARGET", "history")
    async def handle_image_history(self, ctx: "Context") -> Optional[StreamResponse]:

        # resolve and validate the image name
        img, _ = await self.find_image(ctx, ctx.docker_target)

        async with ctx.docker_request("GET", "images", "TARGET", "history",
                target=img) as resp:
            js = await resp.json()
            if resp.status == 200:
                # demangle the tags & digests
                for layer in js:
                    layer["Tags"] = self.demangle_image_list(ctx, layer.get("Tags"))
            return json_response(js, status=resp.status)


    @route("POST", "images", "TARGET", "tag")
    async def handle_image_tag(self, ctx: "Context") -> Optional[StreamResponse]:

        # resolve and validate the image name
        ctx.docker_target, _ = await self.find_image(ctx, ctx.docker_target)

        # mangle the target repository
        ctx.params["repo"] = self.mangle_image_name(ctx, ctx.params["repo"])

        return None


    @route("DELETE", "images", "TARGET")
    async def handle_image_delete(self, ctx: "Context") -> Optional[StreamResponse]:

        # force is not allowed because it could remove images tagged by other
        # users
        if ctx.params.get("force", "0") not in ("0", "false", "False"):
            raise ErrorResponse(403, "force=true not allowed")

        # resolve the image name
        target, js = await self.find_image(ctx, ctx.docker_target)

        # remove from anonymous images (if present)
        self.del_anonymous_image(ctx, js["Id"])

        # when deleting an anonymous image, we do not forward the command to
        # the engine because it may delete the image even if it is visible by
        # another user
        if target == js["Id"] and not js["RepoTags"]:
            return json_response([{"Deleted": target}])

        # forward the delete command to the docker engine (and demangle the result)
        async with ctx.docker_request("DELETE", "images", "TARGET",
                target=target) as cli_resp:
            js = await cli_resp.json()
            if cli_resp.status == 200:
                for elem in js:
                    img = elem.get("Untagged")
                    if img:
                        elem["Untagged"] = self.demangle_image_name(ctx, img)
                return json_response(js)
            else:
                js["message"] = self.demangle_text(js["message"])
                return json_response(js, status=cli_resp.status)


    @route("POST", "containers", "create")
    async def handle_container_create(self, ctx: "Context") -> Optional[StreamResponse]:

        # FIXME: for admin users we should ensure that all used resources are
        #        located in the same namespace

        ######## process the query string ########

        # mangle the container name
        prefix = None
        name = ctx.params.get("name")
        if name is not None:
            ctx.params["name"] = self.mangle_resource_name(ctx, name)

            spl = ctx.user.split_prefix(name)
            if spl is None:
                # NOTE: this may happen if an admin user provides a name from
                #       which we cannot derive a namespace (because it does not
                #       contain any dot)
                raise ErrorResponse(403, "name not allowed: %r" % name)
            prefix = spl[0]
    
        # if no container name is given, then we try to get a prefix from the
        # context (so that this container really belongs to someone)
        if prefix is None:
            prefix = ctx.user.get_prefix()
            if prefix is None:
                raise ErrorResponse(400, "must provide a container name")

        ######## process the json config ########

        cfg = await ctx.json()

        # resolve and validate the image name
        cfg["Image"], _ = await self.find_image(ctx, cfg["Image"])

        # add a label to the container to store the current prefix
        labels = cfg.setdefault("Labels", {})
        if self.label_prefix in labels:
            raise ErrorResponse(403, f"label not allowed: {self.label_prefix}")
        labels[self.label_prefix] = prefix

        # fix the host config
        hc = cfg.get("HostConfig")
        if hc:
            # mangle the links
            links = hc.get("Links") or () # Type: Sequence
            for i in range(len(links)):
                target, tail = re.match(r"([^:]+)(.*)\Z", links[i]).groups()
                mangled = await self.find_container(ctx, target)
                links[i] = mangled + (tail or (":"+target))

            # mangle the volumes_from names
            vfrom = hc.get("VolumesFrom") or ()
            for i in range(len(vfrom)):
                target, tail = re.match(r"([^:]+)((?::r[ow])?)\Z", vfrom[i]).groups()
                vfrom[i] = (await self.find_container(ctx, target)) + tail

            # mangle the volume names
            binds = hc.get("Binds") or ()
            for i in range(len(binds)):
                # FIXME: should document that raw binds are allowed and not mangled
                if not binds[i].startswith("/"):
                    target, mountpoint = re.match(r"([^:]*):(.*)\Z", binds[i]).groups()
                    binds[i] = "%s:%s" % (
                            self.mangle_resource_name(ctx, target), mountpoint)

            # mangle the network mode
            hc["NetworkMode"] = await self.mangle_network_mode(ctx,
                    hc.get("NetworkMode", "default"))

        # reject NetworkingConfig (not supported)
        if cfg.get("Networking config") not in (
                None, {}, {"EndpointsConfig": {}}):
            raise ErrorResponse(403, "tuning the NetworkingConfig is not yet supported")

        # forward the request
        async with ctx.docker_request("POST", "containers", "create",
                headers={"Content-Type": "application/json"},
                params=ctx.params, data=json.dumps(cfg)) as resp:
            return json_response(status=resp.status, text=await resp.text())


    @route("POST", "build")
    async def handle_build(self, ctx: "Context") -> Optional[StreamResponse]:
        tag = ctx.params.get("t")
        if tag:
            # mangle the resulting image name
            ctx.params["t"] = self.mangle_image_name(ctx, tag)

        # always remove intermediate containers because they are not visible by
        # the user (they do not have the label)
        ctx.params["rm"] = "true"
        ctx.params["forcerm"] = "true"
        
        # FIXME there are possible attacks:
        #   - user providing an image with a LABEL can see tmp containers of
        #     another user making a build based on this image
        #   - user may use the LABEL instruction during the build and make his
        #   containers visible by another user
        # -> we should filter the LABEL instructions in the dockerfile to
        #    prevent setting the label
        # -> we should prepend a LABEL instruction if the base image has the
        #    label
        # -> also we can systematically prepend a LABEL instruction and make
        #    the containers visible to the user

        # mangle the network mode
        ctx.params["networkmode"] = await self.mangle_network_mode(ctx,
                ctx.params.get("networkmode", "default"))

        async with DockerfilePatcher(ctx) as dockerfile:
            mangled = await self.mangle_dockerfile(ctx, dockerfile.read())
            dockerfile.seek(0)
            dockerfile.truncate()
            dockerfile.write(mangled)

        # demangle image names in the streamed response
        self.demangle_response(ctx, ("status", "stream","error","errorDetail"))

        # extract image Ids from the streamed response
        # 
        # during the build we track the ID of the generated images and
        # store them in the `anonymous_images` dict so that tag-less images
        # are still visible by their owner.
        #
        # NOTE: this will store the intermediate images too (in multi-stage
        #       builds)
        @ctx.json_chunked_response_filters.append
        def save_image_id(js):
            if "ID" in js.get("aux", ()):
                self.add_anonymous_image(ctx, js["aux"]["ID"])

        return None

    ################ container endpoints ##########################

    def container_demangle_common(self, ctx, js):
        """demangling code that is common to 'ps' and 'inspect'"""

        hc = js["HostConfig"]

        # demangle mounts
        for mount in js.get("Mounts") or ():
            name = mount.get("Name")
            if name:
                mount["Name"] = self.demangle_resource_name(ctx, name)

        # demangle network mode
        mode = hc.get("NetworkMode")
        if mode not in (None, "none", "bridge", "default"):
            head, name = re.match(r"((?:container:)?)(.*)\Z", mode).groups()
            hc["NetworkMode"] = head + (self.demangle_resource_name(ctx, name) or name)


        ###########################################################################
        # handle keys thar are present in js["Config"] in container_inspect and at
        # the root in container_ps

        config = js.get("Config", js)

        # demangle image name
        name = self.demangle_image_name(ctx, config["Image"])
        if name:
            config["Image"] = name

        # filter ci label
        labels = config.get("Labels")
        if labels and self.label_prefix in labels:
            del labels[self.label_prefix]


    @route("GET", "containers", "TARGET", "json")
    async def handle_container_inspect(self, ctx: "Context") -> Optional[StreamResponse]:

        ctr = await self.find_container(ctx, ctx.docker_target)

        # forward the request
        async with ctx.docker_request("GET", "containers", "TARGET", "json",
                target=ctr) as resp:
            js = await resp.json()
            if resp.status == 200:
                self.container_demangle_common(ctx, js)

                config = js["Config"]
                hc = js["HostConfig"]

                # demangle container name
                name = self.demangle_container_name(ctx, js["Name"])
                if name:
                    js["Name"] = name

                # demangle volume names
                if hc.get("Binds"):
                    def fix_bind(bind):
                        vol, tail = bind.split(":", 1)
                        return f"{self.demangle_resource_name(ctx, vol)}:{tail}"
                    hc["Binds"] = list(map(fix_bind, hc["Binds"]))

                # demangle the links
                def link_repl(mo):
                    head, name = mo.groups()
                    demangled = self.demangle_resource_name(ctx, name)
                    return head + (demangled or name)
                links = hc.get("Links") or ()
                for i in range(len(links)):
                    links[i] = re.sub(r"((?:\A|:)/)([^/:]+)", link_repl, links[i])

            return json_response(js, status=resp.status)


    @route("GET", "containers", "json")
    async def handle_container_list(self, ctx: "Context") -> Optional[StreamResponse]:

        # parse the filters parameter
        try:
            filters = json.loads(ctx.params.get("filters", "{}"))
        except ValueError:
            raise ErrorResponse(400, "malformatted filters")

        # append a filter so that we list only the containers with the right prefix
        # FIXME: this will not be suitable for non-admin users that can view
        #        multiple prefixes
        prefix = ctx.user.get_prefix()
        if prefix is not None:
            # NOTE: the API spec says that this field is a str, but dockerd
            #       engine only accepts lists or dicts. On the client side
            #       docker sends a dict and docker-py sends a list.
            if isinstance(filters.get("label"), list):
                filters["label"].append(f"{self.label_prefix}={prefix}")
            else:
                filters.setdefault("label", {}
                        )[f"{self.label_prefix}={prefix}"] = True

        # remove the name filters (because we must do the pattern matching
        # after demangling) and compile the regexes
        name_filters = filters.pop("name", ())

        # overrride the filters in the params list
        ctx.params["filters"] = json.dumps(filters)


        def demangle_and_filter_container_names(js):
            # iterate through the container list in reverse order (to allow
            # removals during the iteration)
            for i in reversed(range(len(js))):
                names = js[i]["Names"]

                for j in range(len(names)):
                    demangled = self.demangle_container_name(ctx, names[j])

                    # demangle the name if possible, othewise keep the raw name
                    # FIXME: if we need to support non-admin users that can
                    #        access multiple prefixes, then we should continue
                    #        if demangled is empty
                    if demangled:
                        names[j] = demangled

                    # continue if no need to apply any filter
                    if not name_filters:
                        continue

                    # apply the filters only to the container name
                    # (not to the aliases created with legacy links)
                    if "/" in names[j][1:]:
                        continue

                    # apply pattern matchin only on the demangled name
                    # -> always mismatch if demangle fails
                    if demangled:
                        # FIXME: We should use go regexes instead of python
                        # regexes. We my have corner cases with different
                        # results.
                        if any(re.search(p, demangled) for p in name_filters):
                            continue

                    # name mismatch
                    # -> remove the container from the list
                    js.pop(i)
                    break

        # forward the request
        async with ctx.docker_request("GET", "containers", "json",
                params=ctx.params) as resp:
            js = await resp.json()
            if resp.status == 200:
                demangle_and_filter_container_names(js)
                for ctr in js:
                    self.container_demangle_common(ctx, ctr)

            return json_response(js, status=resp.status)


    @route("POST", "commit")
    async def handle_commit(self, ctx: "Context") -> Optional[StreamResponse]:

        ctx.params["container"] = await self.find_container(
                ctx, ctx.params["container"])

        # mangle the image name
        repo = ctx.params.get("repo")
        if repo:
            ctx.params["repo"] = self.mangle_image_name(ctx, repo)

        # forward the request to the engine
        async with ctx.docker_request("POST", "commit", params=ctx.params,
                headers={"Content-Type": "application/json"},
                data = await ctx.request.read()) as resp:
            js = await resp.json()
            if resp.status == 201:
                self.add_anonymous_image(ctx, js["Id"])
            return json_response(js, status=resp.status)


    @route("DELETE", "containers", "TARGET")
    @route("GET", "containers", "TARGET", "archive")
    @route("GET", "containers", "TARGET", "changes")
    @route("GET", "containers", "TARGET", "export")
    @route("GET", "containers", "TARGET", "logs")
    @route("GET", "containers", "TARGET", "stats")
    @route("GET", "containers", "TARGET", "top")
    @route("HEAD", "containers", "TARGET", "archive")
    @route("POST", "containers", "TARGET", "attach")
    @route("POST", "containers", "TARGET", "attach/ws")
    @route("POST", "containers", "TARGET", "exec")
    @route("POST", "containers", "TARGET", "kill")
    @route("POST", "containers", "TARGET", "pause")
    @route("POST", "containers", "TARGET", "resize")
    @route("POST", "containers", "TARGET", "restart")
    @route("POST", "containers", "TARGET", "start")
    @route("POST", "containers", "TARGET", "stop")
    @route("POST", "containers", "TARGET", "unpause")
    @route("POST", "containers", "TARGET", "wait")
    @route("PUT", "containers", "TARGET", "archive")
    async def handle_container_command(self, ctx: "Context") -> Optional[StreamResponse]:
        ctx.docker_target = await self.find_container(ctx, ctx.docker_target)
        return None

    ################ exec endpoints ###############################

    @route("GET", "exec", "TARGET", "json")
    @route("POST", "exec", "TARGET", "resize")
    @route("POST", "exec", "TARGET", "start")
    async def handle_exec_command(self, ctx: "Context") -> Optional[StreamResponse]:
        ctx.docker_target = await self.find_exec(ctx, ctx.docker_target)
        return None


    ################ volume endpoints #############################


    @route("GET", "volumes")
    async def handle_volume_list(self, ctx: "Context") -> Optional[StreamResponse]:
        # parse the filters parameter
        try:
            filters = json.loads(ctx.params.get("filters", "{}"))
        except ValueError:
            raise ErrorResponse(400, "malformatted filters")

        # remove the name filters (because we must do the pattern matching
        # after demangling) and compile the regexes
        name_filters = filters.pop("name", ())
        if name_filters:
            ctx.params["filters"] = json.dumps(filters)

        def demangle_and_filter_volume_names(js):
            # iterate through the container list in reverse order (to allow
            # removals during the iteration)
            for i in reversed(range(len(js))):
                name = js[i]["Name"]
                demangled = self.demangle_resource_name(ctx, name)

                # demangle the name
                if demangled:
                    js[i]["Name"] = demangled

                    # continue if no need to apply any filter
                    if not name_filters:
                        continue

                    # apply pattern matching only on the demangled name
                    # FIXME: We should use go regexes instead of python
                    # regexes. We my have corner cases with different
                    # results.
                    if any(re.search(p, demangled) for p in name_filters):
                        continue

                # name mismatch
                # -> remove the container from the list
                js.pop(i)

        # forward the request
        async with ctx.docker_request("GET", "volumes", params=ctx.params
                ) as resp:

            result = await resp.json()
            if resp.status == 200:
                volumes = result["Volumes"]
                demangle_and_filter_volume_names(volumes or ())

            return json_response(result, status=resp.status)


    @route("GET", "volumes", "TARGET")
    async def handle_volume_inspect(self, ctx: "Context") -> Optional[StreamResponse]:

        mangled = self.mangle_resource_name(ctx, ctx.docker_target)

        async with ctx.docker_request("GET", "volumes", "TARGET",
                target=mangled) as resp:
            js = await resp.json()
            if resp.status==200:
                # demangle volume name in the response
                demangled = self.demangle_resource_name(ctx, js["Name"])
                if demangled:
                    js["Name"] = demangled

            return json_response(js, status=resp.status)


    @route("DELETE", "volumes", "TARGET")
    async def handle_volume_command(self, ctx: "Context") -> Optional[StreamResponse]:

        ctx.docker_target = self.mangle_resource_name(ctx, ctx.docker_target)

        return None

