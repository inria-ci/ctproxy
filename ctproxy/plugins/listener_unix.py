#!/usr/bin/python3

import asyncio
import grp
import logging
import os
import pwd
import socket
from typing import Optional, Tuple

import aiohttp.web

import ctproxy
from ctproxy import ssl
from . import Listener, CfgDict, plugin

log = logging.getLogger("listener_unix")

def _remove_stale_unix_socket(path: str):
    '''Remove 'path' if it is a non-active socket'''
    if os.path.exists(path):
        sock = socket.socket(socket.AF_UNIX)
        try:
            sock.connect(path)
        except ConnectionError:
            log.info("removing stale unix socket: %s", path)
            os.unlink(path)
        finally:
            sock.close()

@plugin
class UnixListener(Listener):
    """A listener pluging that accepts connections on a unix socket

    By default the socket is only usable by the curren user (mode 0600).

    If `allow_user` is set to a valid user, then the socket uid is changed to
    this user.

    If `allow_group` is set to a valid group, then the socket gid is changed to
    this group, and the permissions g+rw are added.

    If `allow_other` is true, then the socket is usable by any user (permission
    o+rw)

    Sample config:
        # required
        type:           tcp
        bind:           "/path/to/the/socket"

        # optional
        allow_user:     "username"
        allow_group:    "groupname"
        allow_other:    false

        tls: ...
        auth: ...
        policies: ...
    """

    _path: str
    _uid: Optional[int]
    _gid: Optional[int]
    _mode: int

    def __init__(self, reloader: ctproxy.ConfigReloader, cfg: CfgDict, **kw) -> None:

        super().__init__(reloader, cfg, **kw)

        self._path = cfg.get("bind", type=str, required=True)
        
        user  = cfg.get("allow_user",  type=str)
        group = cfg.get("allow_group", type=str)
        other = cfg.get("allow_other", type=bool)
        self._uid = None if user  is None else pwd.getpwnam(user ).pw_uid
        self._gid = None if group is None else grp.getgrnam(group).gr_gid
        self._mode =(0o600
                        | (0o060 if group is not None else 0)
                        | (0o006 if other             else 0))


    async def create_server(self) -> None:
        assert not hasattr(self, "_server")

        _remove_stale_unix_socket(self._path)

        sock = socket.socket(socket.AF_UNIX)
        sock.bind(self._path)
        os.chmod(self._path, self._mode)
        os.chown(self._path,
                (-1 if self._uid is None else self._uid),
                (-1 if self._gid is None else self._gid))

        self._server = await asyncio.get_event_loop().create_unix_server(
                self._app_handler, sock=sock, ssl=self._ssl_context)


    async def shutdown(self) -> None:
        try:
            os.unlink(self._path)
        except OSError as e:
            log.warning("failed to remove socket %s (%s)", self._path, e)

        await super().shutdown()


