#!/usr/bin/python3

import asyncio
import collections
import errno
import logging
import os
import re
import socket
import typing
from typing import ClassVar, Dict, Optional, Set, Tuple

from aiohttp import ClientSession, UnixConnector, DummyCookieJar

import ctproxy
from ctproxy import ssl
from ctproxy.utils import random_token
from . import Listener, CfgDict, plugin, User, MangledUser

log = logging.getLogger("listerner_tmp")

_Listener = collections.namedtuple("_Listener", "server user auto_purge")

#FIXME: for the sake of simplicity the current implementation brutally destroys
#       all listeners on shutdown, which is an issue when the daemon is
#       restarted (eg. for a config update). We cannot just recreate the unix
#       sockets on startup because the initial sockets may have been mounted
#       inside some containers. A possible solution would be to transfer the
#       sockets to the new process using SCM_RIGHTS when restarting.
#

@plugin
class TmpListener(Listener):
    """A listener pluging for managing a pool of temporary unix socket

    (NOTE: this is a very experimental plugin)

    This plugin provides a way to dynamically create/remove unix socket
    listeners for receiving docker engine HTTP requests. New sockets are
    created with .create_tmp_listener() and destroyed with
    .remove_tmp_listener().

    The unix sockets are stored inside a private directory (the 'path'
    parameter in the config) and their names (listener_id) are generated
    randomly using a secure PRNG.

    By default no access control is performed by the plugin (apart from storing
    the sockets in a directory with 0700 permissions), it is the responsibility
    of the user to set up proper authentication:
    - either with the `tmp_listener` auth plugin (to match the user according
      to the socket on which the request is received)
    - or with any usual auth plugin

    The main purpose of the plugin is to provide limited access to the docker
    API inside a container:
    - upon container creation, a dedicated tmp listener is created and
      mounted at /var/run/docker.sock inside the container
    - this tmp listener is associated with a user (MangledUser) whose access is
      limited to a given namespace
    - upon container removal, all docker resources in the given namespace are
      destroyed

    This plugin can be instantiated only once per process.

    On shutdown, all listeners are automatically removed (and their resources
    cleared if `auto_purge` is true). 

    Sample config:
        # required
        type:   tmp
        path:   "/var/lib/ctproxy/tmp_listeners"

        # optional
        auth: {"type": "tmp_listener"}
        tls: ...
        policies: ...
    """

    _instance: "ClassVar[TmpListener]"

    # base directory hosting the sockets
    _path: str
    _process_path: str
    _reg_listener_path: typing.re.Pattern[str]
    _listeners: Dict[str, _Listener]
    _closing: Set[str]
    _shutdown: Optional[asyncio.Future]

    def __init__(self, reloader: ctproxy.ConfigReloader, cfg: CfgDict, **kw) -> None:

        super().__init__(reloader, cfg, **kw)

        self._path = cfg.get("path", type=str, required=True)
        if not os.path.isabs(self._path):
            raise ValueError("%r is not an absolute path" % self._path)
        if not os.path.isdir(self._path):
            raise ValueError("%r must be a writable directory" % self._path)
        if not os.access(self._path, os.R_OK | os.W_OK | os.X_OK):
            raise ValueError("current user does not have rwx permissions on %r"
                    % self._path)
        if os.stat(self._path).st_mode & 0o0007:
            raise ValueError("%r must not be readable/writable/accessible by other users"
                    % self._path)

        self._process_path = os.path.join(self._path, str(os.getpid()))
        self._reg_listener_path = re.compile(f"\A{re.escape(self._process_path)}/([A-Za-z0-9_-]+)\Z")
        self._listeners = {}
        self._closing = set()
        self._shutdown = None

    async def __aenter__(self):
        if hasattr(self, "_instance"):
            raise RuntimeError("there can be only one tmp listener")

        mask = os.umask(0o0077)
        try:
            os.mkdir(self._process_path)
        finally:
            os.umask(mask)

        self.__class__._instance = self


    async def __aexit__(self, *exc):
        del self.__class__._instance
        try:
            os.rmdir(self._process_path)
        except FileNotFoundError:
            pass
        except OSError as e:
            log.warning("unable to remove directory %r (%s)",
                    self._process_path, e)

    async def create_server(self) -> None:
        return None

    async def shutdown(self) -> None:
        if self._shutdown is None:
            log.info("shutdown (%d listeners)" % len(self._listeners))
            self._shutdown = asyncio.ensure_future(asyncio.gather(
                *map(self.remove_tmp_listener, self._listeners),
                return_exceptions=True))

    async def finish_shutdown(self) -> None:
        assert self._shutdown is not None
        await self._shutdown
        log.info("shutdown complete")

    @classmethod
    def listener_path(cls, listener_id: str) -> str:
        """Get the unix socket path associated to a given listener_id"""
        assert "/" not in listener_id and "." not in listener_id
        return os.path.join(cls._instance._process_path, listener_id)

    @classmethod
    def match_listener_path(cls, path: str) -> Optional[str]:
        """Return the listener_id associated to a given unix socket
        
        Note: `path` must be the expressed as returned by .listener_path()
        """
        mo = cls._instance._reg_listener_path.match(path)
        return None if mo is None else mo.group(1)

    @classmethod
    async def create_tmp_listener(cls, user: User,
            *, auto_purge : bool = False) -> str:
        """Create a new tmp listener

        This function creates a new unix socket with a random name and start
        accepting HTTP requests on it.

        `user` is the user associated to this tmp listener (incoming requests
        can be authenticated using the `tmp_listener` auth plugin)

        If `auto_purge` is true, then all resources visible through this
        listener will be cleared when the listener is removed (see
        .remove_tmp_listener()).

        Return the listener_id of the newly created listener
        """
        self = cls._instance
        listener_id = random_token(32)
        listener_path = self.listener_path(listener_id)
        sock = socket.socket(socket.AF_UNIX)
        try:
            sock.bind(listener_path)
            os.chmod(listener_path, 0o600)
            log.info("socket created at %r", listener_path)

            server = await asyncio.get_event_loop().create_unix_server(
                    self._app_handler, sock=sock, ssl=self._ssl_context)

            if self._shutdown is not None:
                raise RuntimeError("shutdown in progress")

            self._listeners[listener_id] = _Listener(server, user, auto_purge)
            log.info("new listener: %s -> %s", listener_id, user)
            return listener_id

        except BaseException as e:
            sock.close()
            if getattr(e, "errno", None) != errno.EADDRINUSE:
                try:
                    os.unlink(listener_path)
                except:
                    pass
            raise

    @classmethod
    async def remove_tmp_listener(cls, listener_id: str,
            *, purge: Optional[bool] = None) -> None:
        """Remove a tmp listener

        This function stops a tmp listener and removes the underlying socket
        socket.

        If `purge` is provided, then it will override the `auto_purge` value
        that was provided in .create_tmp_listener(). The purge feature consists
        of clearing all docker resources (containers, images, volumes,...) that
        are visible through this listener. As as safety precaution, the 'purge'
        feature can be only used when the user associated to the listener is a
        MangledUser.
        """
        self = cls._instance
        try:
            self._closing.add(listener_id)
            listener = self._listeners[listener_id]
        except KeyError:
            return
        else:
            log.info("del listener: %s -> %s", listener_id, listener.user)
            listener_path = self.listener_path(listener_id)

            if listener.auto_purge if purge is None else purge:
                await self._purge_tmp_listener(listener_id, listener_path, listener.user)
            try:
                os.unlink(listener_path)
            except:
                log.warning("unable to remove tmp socket %r", listener_path)

            listener.server.close()
            await listener.server.wait_closed()
        finally:
            self._listeners.pop(listener_id, None)
            self._closing.discard(listener_id)

    @classmethod
    def get_user(cls, listener_id: str
            ) -> Optional[Tuple[User, bool]]:
        """Get the user associated to a given listener_id
        
        Returns None if this listener_id is unknown.

        Otherwise returns a tuple (user, is_closing) where:
         - `user` is the User associated to the socket
         - `is_closing` is a boolean set to True if the listener is being
           destroyed and shall no longer be used
        """
        listener = cls._instance._listeners.get(listener_id)
        return None if listener is None else (
                listener.user, listener_id in cls._instance._closing)

    async def _purge_tmp_listener(self, listener_id: str, listener_path: str,
            user: User):
        try:
            if not isinstance(user, MangledUser):
                # FIXME: if the namespaces module is not enabled, then all
                # resources on the docker engine will be wiped out (even with a
                # MangledUser)
                log.warning("unable to purge resources for listener %s (user %r does not have a dedicated namespace)",
                        listener_id, user.name)
                return

            async with ClientSession(connector=UnixConnector(listener_path),
                    cookie_jar=DummyCookieJar()) as session:
                async def get(url) -> list:
                    async with session.get(url) as resp:
                        resp.raise_for_status()
                        return await resp.json()

                containers, images, volumes = await asyncio.gather(
                        get("http://DUMMY/v1.32/containers/json?all=1"),
                        get("http://DUMMY/v1.32/images/json"),
                        get("http://DUMMY/v1.32/volumes"),
                        #TODO delete networks
                        )

                log.info("purging tmp listener %s")
                log.info(" - containers: %s", containers)
                log.info(" - images:     %s", images)
                log.info(" - volumes:    %s", volumes)

                # remove all containers
                for ctr in containers:
                    async with session.delete(
                            f"http://DUMMY/v1.32/containers/{ctr['Id']}?force=1") as resp:
                        if resp.status not in (204, 404):
                            log.warning("failed to remove container %s (%s %s)",
                                    ctr["Id"], resp.status, resp.reason)
                # remove all images
                for img in images:
                    #TODO delete digests too
                    for image in img.get("RepoTags", ()):
                        async with session.delete(
                                f"http://DUMMY/v1.32/images/{image}") as resp:
                            if resp.status not in (200, 404):
                                log.warning("failed to remove image %s (%s %s)",
                                        image, resp.status, resp.reason)
                # remove all volumes
                for vol in volumes["Volumes"]:
                    async with session.delete(
                            f"http://DUMMY/v1.32/volumes/{vol['Name']}?force=1") as resp:
                        if resp.status not in (204, 404):
                            log.warning("failed to remove volume %s (%s %s)",
                                    vol['Name'], resp.status, resp.reason)

        except:
            log.exception("unable to purge rescources for listener %s", listener_id)
