#!/usr/bin/python3

import asyncio
import importlib
import logging
import os
import re
from typing import cast, Callable, Dict, Optional, Tuple, Type, TypeVar, Union
from urllib.parse import ParseResult

import aiohttp
from aiohttp import ClientSession, UnixConnector, DummyCookieJar
from aiohttp.web import StreamResponse
import aiohttp.web
import docker
from config_reader import Mapping as CfgDict

import ctproxy
from .. import ssl
from ..bases import HttpRequestHandler
from ..utils import coerce

log = logging.getLogger("plugins")

PluginType = TypeVar("PluginType", "Plugin", "Plugin")

# default timeout args for creating an aiohttp.ClientSession
#
# By default aiohttp sets a total timeout of 5 minutes, which is unsuitable
# because we may have longer requests.
#
# FIXME: we override it to one hour because this is the max duration for our CI
# jobs, but ultimately this shoud configurable by the user and should be
# enabled only for requests that are prone to be long (eg: 'docker build',
# 'docker logs', 'docker pull', ...)
_DEFAULT_CLIENT_SESSION_TIMEOUT_ARGS = {
            "timeout": aiohttp.ClientTimeout(total=3600, connect=30)
        } if hasattr(aiohttp, "ClientTimeout") else {
            # aiohttp < 3.3
            "read_timeout": 3600, "conn_timeout": 30,
        }

# list of plugin implementations
#   key: module name
#   val: plugin class
_classes: Dict[str, PluginType] = {}

def plugin(cls: "Plugin") -> "Plugin":
    """Decorator for marking the plugin class in a plugin module"""

    assert cls.__module__ not in _classes, "'@plugin' must not be used multiple times in the same module"
    _classes[cls.__module__] = cls
    return cls

class Namespace:
    """Represents a namespace for named resources in the docker engine

    Namespaces are an addition from the ctproxy tool.
    
    All docker resources manipulated by the ctproxy tool belong exactly to
    one namespace.

    A namespace is identified by a name `"NAME"` or a prefix `"NAME."`.

    All namespace *must* be disjoint. It is the responsibility of the `Auth`
    plugins to ensure that this constraint is met.
    """
    def __init__(self, name: str) -> None:
        self._prefix = name + "."

    def __str__(self):
        return "<Namespace %r>" % self._prefix

    @property
    def name(self) -> str:
        """Get the name of the namespace"""
        return self._prefix[:-1]

    @property
    def prefix(self) -> str:
        """Get the namespace prefix
        
        This is equal to `self.name + '.'`
        """
        return self._prefix

    def __eq__(self, other: Union[str, "Namespace", object]) -> bool:
        """Test if both object refer to the same namespace"""
        if isinstance(other, Namespace):
            other = other.name
        elif not isinstance(other, str):
            return False
        return other == self.name

    def __ne__(self, other: Union[str, "Namespace", object]) -> bool:
        """Test if the two object do not refer to the same namespace"""
        return not (self == other)

class User:
    """Abstract base class for user access control

    A `User` object is created for each authenticated user.
    
    This object controls which resources are usable by the user and how they
    are presented to this user (i.e. whether the resource names are mangled or
    not).

    In typical setups:
    - admin users have access to all resources and use the raw resource names
      (prefixed with their namespace)
    - ordinary users only have access to a single namespace and are not
      namespace-aware (all resource names are transparently modified to prefix
      them with the namespace)
    """

    def __init__(self, name: str) -> None:
        """Create the user
    
        `name` is the user name
        """
        self.name       = name

    def mangle_resource_name(self, presented_name: str) -> Optional[str]:
        """Mangle a resource name
        
        `presented_name` is the name of the resource as presented to the user

        Return the raw name of the resource (as used in the docker engine).

        It is expected that the `User` object is able to guess the relevant
        namespace (see `split_prefix()`) for the resource. If this is not
        possible, then this function should return None (to trigger an error).
        """
        raise NotImplementedError()

    def demangle_resource_name(self, raw_name: str) -> Optional[str]:
        """Demangle a resource name

        `raw_name` is the raw name of the resource (as used in the docker engine)

        Return the resource name as it is presented to the user.

        If the user is not allowed to use this resource (eg: because it belongs
        to another user) then the function shall return None.
        """
        raise NotImplementedError()

    def split_prefix(self, presented_name: str) -> Optional[Tuple[str, str]]:
        """Split the prefix from a resource name
        
        `presented_name` is the name of the resource as presented to the user

        Return a tuple `(prefix, name)` where:
        - `prefix` is the prefix of the namespace where this resource is
           located
        - `name` is the name of the resource within this prefix

        It is expected that the `User` object is able to guess the relevant
        namespace for the resource. If this is not possible, then this function
        should return None (to trigger an error).

        In case of success then the value `prefix+name` is expected to be equal
        to the result of `self.mangle_resource_name(presented_name)`.
        """
        raise NotImplementedError()

    def join_prefix(self, prefix: str, name: str) -> Optional[str]:
        """Join the prefix to a resource name
        
        `prefix` is the prefix of the namespace where this resource is located
        `name` is the name of the resource within this prefix

        Return the resource name as it is presented to the user.

        If the user is not allowed to use this resource (eg: because it belongs
        to another user) then the function shall return None.
        """
        raise NotImplementedError()

    def can_use_prefix(self, prefix: str) -> bool:
        """Check if the user is allowed to use a namespace

        `prefix` is the prefix of the relevant namespace

        Return True if this user is allowed to use this namespace
        """
        raise NotImplementedError()

    def get_prefix(self) -> Optional[str]:
        """Get the prefix of the namespace associated to this user

        Returns None if this user may use multiple namespaces

        This function is used to automatically derive a namespace when the user
        creates an unnamed resource (container or image). Users who can use
        multiple namespaces (eg: admin users) are not allowed to create unnamed
        resources (because a resources must be allocated to a namespace).
        """
        raise NotImplementedError()


class MangledUser(User):
    """A user whose resources are mangled

    A MangledUser is associated to a Namespace and is isolated within this
    namespace:
    - it can access only ressources located within this namespace
    - its requests are mangled (resource names are prefixed with the namespace)
      and the responses are demangled (resources belonging to its namespace are
      stripped from their prefix, and resources belonging to other namespaces
      are filtered out)

    For example, given that:
        - user A is configured with namespace="dummy_namespace"`
        - user B is configured with namespace="other_namespace"`
        - the following containers names are present in the docker engine:
            dummy_namespace.ctr1
            dummy_namespace.ctr2
            other_namespace.ctr3
            another_namespace.ctr4
    then:
        - user A sees 'ctr1' and 'ctr2'
        - user B sees 'ctr3'
    """

    def __init__(self, name: str, namespace: Namespace) -> None:
        super().__init__(name)
        self._namespace = namespace

    def __str__(self):
        return "<MangledUser %s %s>" % (self.name, self._namespace)

    def mangle_resource_name(self, name: str) -> Optional[str]:
        return self._namespace.prefix + name

    def demangle_resource_name(self, raw_name: str) -> Optional[str]:
        prefix = self._namespace.prefix
        if raw_name.startswith(prefix):
            return raw_name[len(prefix):]
        return None

    def split_prefix(self, name: str) -> Optional[Tuple[str, str]]:
        return self._namespace.prefix, name

    def join_prefix(self, prefix: str, name: str) -> Optional[str]:
        if prefix == self._namespace.prefix:
            return name
        return None

    def can_use_prefix(self, prefix: str) -> bool:
        return prefix == self._namespace.prefix

    def get_prefix(self) -> Optional[str]:
        return self._namespace.prefix

class AdminUser(User):
    """An admin user

    An admin user can see all the resources present in the docker engine.

    Its requests and responses are not mangled.
    """
    def __init__(self, name: str,
            split_prefix: Callable[[str], Optional[Tuple[str, str]]]) -> None: 
        super().__init__(name)
        self.split_prefix = split_prefix    # type: ignore

    def mangle_resource_name(self, name: str) -> Optional[str]:
        # FIXME: maybe we should return None if `name` cannot be split
        #        with .split_prefix()
        return name

    def demangle_resource_name(self, raw_name: str) -> Optional[str]:
        return raw_name

    def join_prefix(self, prefix: str, name: str) -> Optional[str]:
        return prefix + name

    def can_use_prefix(self, prefix: str) -> bool:
        return True

    def get_prefix(self) -> Optional[str]:
        return None


class Plugin:
    def __init__(self, reloader: "ctproxy.ConfigReloader", config: CfgDict) -> None:
        """Plugin initialisation
        
        `config` is a json-like struct produced by `config_reader` module
        """
        pass

    async def __aenter__(self) -> None:
        """Plugin startup"""
        pass

    async def __aexit__(self, *k: object) -> None:
        """Plugin cleanup"""
        pass


class Auth(Plugin, HttpRequestHandler):
    """Authorization plugin
    
    `Auth` plugins are responsible for identifying the authenticated user and
    controlling the access to the docker resources.

    The access control covers:
    - denying access to the resources that a user is not allowed to use
    - mangling the resource names (to present a different name to the user, for
      example when this user is isolated within a namespace)
    """

    async def handle_request(self, ctx: "ctproxy.Context") -> Optional[StreamResponse]:
        """Identify a user from the auth information provided in the context
        
        Currently the context provides:
        - `auth_cert`: this is the X.509 certificate (in PEM format) that this
          user used to authenticate itself (using TLS client authentication).
          If present, this auth_cert is already verified (using the CA
          certificate configured on the TLS listener).

        If the authentication is successful, the function shall set `ctx.user`
        to a `User` object for controlling the resource accesses by this user.
        """
        raise NotImplementedError


    @staticmethod
    def admin_split_prefix(presented_name: str) -> Optional[Tuple[str, str]]:
        """Default `.split_prefix()` implementation for admin users
        
        The default is to consider that namespaces never contain a dot. This
        function splits the name at the first encountered dot (if present).
        """
        mo = re.match(r"([a-z0-9_-]+\.)(.*)\Z", presented_name)
        return cast(Tuple[str, str], mo.groups()) if mo else None


class Policy(Plugin, HttpRequestHandler):
    """Policy plugin

    Policy plugins are responsible of enforcing rules when creating new
    resources.
    """

    async def handle_request(self, ctx: "ctproxy.Context") -> Optional[StreamResponse]:
        """Handle a docker request

        raise ErrorResponse in case the request contains invalid/disallowed
        parameters

        return None otherwise
        """
        raise NotImplementedError

class DockerClient(Plugin):
    """Abstract base class for the DockerClient plugins"""

    def route_request(self, command: Tuple[str,...], target: Optional[str]
            ) -> Tuple[ClientSession, Optional[ssl.SSLContext],
                    ParseResult]:
        """Route a docker request to an appropriate docker engine

        Returns a tuple `(session, context, engine_url)`
        - `session` is the aiohttp client session to be used to contact the
          engine
        - `context` is the ssl context
        - `engine_url` is the base url of the docker engine to be used

        NOTE: the fields of `engine_url` have restrictions:
                - .query, .params and .fragment must be empty
                - .path must not end with a slash
        """
        raise NotImplementedError("")


    @staticmethod
    def load_client_ssl_context(cfg: CfgDict) -> ssl.SSLContext:
        """Create a docker client ssl context from config
        
        `cfg` is a dict that may contain:
        - `cert_path`   path to a directory containing docker client key and
                        certificates `ca.pem`, `cert.pem` and `cert.key`
                        (just like DOCKER_CERT_PATH)

        alternatively it may contain the explicit path for each file:
        - `ca`          path to the server CA certificate
        - `cert`        path to the client certificate
        - `key`         path to the client key
        

        Return a `ssl.SSLContext` object
        """

        # directory containing the docker certificates (DOCKER_CERT_PATH)
        cert_path = cfg.get("cert_path", type=str)  # type: Optional[str]
        def load(name: str) -> str:
            val = cfg.get(name, type=str) # type: Optional[str]
            if val is None and cert_path is not None:
                val = os.path.join(cert_path, name+".pem")
            return val
        ca   = load("ca")
        cert = load("cert")
        key  = load("key")
        ctx = ssl.create_default_context(cafile=ca)
        if not (cert is None and key is None):
            ctx.load_cert_chain(cert, key)
        return ctx


    @staticmethod
    def make_aiohttp_client(host: str) -> Tuple[ClientSession, str]:
        """Create a aiohttp session for connecting to a docker engine

        `host` is a docker host url (DOCKER_HOST) it may be either a `tcp://`
        or `unix://` url

        Return a tuple `(session, url)` where `session` is a aiohttp client
        session and `url` is the http(s) url of the docker engine
        """
        url: str = docker.utils.parse_host(host)
        mo = re.match(r"http\+unix:/+(/.*)", url)
        if mo is None:
            # HTTP over TCP
            connector = None
            log.info("upstream docker engine is at %r", url)
        else:
            # HTTP over unix socket
            url = "http://DUMMY.localhost"
            path = mo.group(1)
            connector = UnixConnector(path)
            log.info("upstream docker engine is on unix socket %r", path)

        # we use DummyCookieJar to discard cookies (this session object is
        # shared by multiple users)
        session = ClientSession(connector=connector,
                cookie_jar=DummyCookieJar(),
                **_DEFAULT_CLIENT_SESSION_TIMEOUT_ARGS)
        return session, url

class Listener(Plugin):
    """Listener plugin
    
    `Listener` plugins are responsible for setting up a socket that accepts new
    HTTP connections

    At creation time, along with the configuration dict, two additional
    arguments are provided to the plugin:
     - `app_handler` is the asyncio.Protocol factory to be used for handling
       the incoming connections
     - `app` is the underlying aiohttp.web.Application instance
    """

    _ssl_context: Optional[ssl.SSLContext]
    _app: aiohttp.web.Application
    _app_handler: aiohttp.web.Server

    _server: asyncio.AbstractServer

    def __init__(self, reloader: "ctproxy.ConfigReloader", config: CfgDict,
            *, ssl_context: Optional[ssl.SSLContext],
            app: aiohttp.web.Application, app_handler: aiohttp.web.Server) -> None:

        self._ssl_context = ssl_context
        self._app = app
        self._app_handler = app_handler

    async def create_server(self) -> None:
        """Creates the asyncio Server and stores it into self._server"""
        raise NotImplementedError

    async def shutdown(self) -> None:
        """Initiate listener shutdown
        
        This will stop accepting new connections and initiate shutdown
        """
        if hasattr(self, "_server"):
            self._server.close()
            await self._server.wait_closed()
        await self._app.shutdown()

    async def finish_shutdown(self) -> None:
        """Finish listener shutdown
        
        This will wait until all pending requests are completed, then cleanup
        the aiohttp.web application
        """
        await self._app_handler.shutdown()
        await self._app.cleanup()

    def active_connections(self) -> int:
        """Return the number of active connections"""
        return len(self._app_handler.connections)


def new(kind: Type[PluginType], name: str, reloader: "ctproxy.ConfigReloader",
        config: Optional[CfgDict] = None, **kw: object) -> PluginType:
    """Instantiate a plugin
    
    `kind`: plugin kind
    `name`: the name of the plugin implementation
    `reloader`: is the notification channel for reloading the plugin config on
                SIGHUP
    `config`: the configuration of the plugin (typically a json tree)
    `**kw`: extra named parameter for initialising the plugin

    This function will attempt to import the `ctproxy.plugins.{kind}_{name}`
    module. If unsuccessful it will attempt to load the `{name}` module instead.

    Then it will instantiate the plugin using the class with `@plugin`.
    """

    if not re.match(r"[A-Z][A-Za-z0-9]*\Z", kind.__name__):
        raise ValueError("invalid plugin kind: %r" % kind.__name__)

    if not re.match(r"[a-z][a-z0-9_.]*\Z", name):
        raise ValueError("invalid plugin name: %r" % name)

    snake_kind = (kind.__name__[:1] + re.sub("([A-Z])", r"_\1", kind.__name__[1:])).lower()

    module = None
    if "." not in name:
        # try native plugin
        try:
            module = importlib.import_module(f"{__name__}.{snake_kind}_{name}", __name__)
        except ModuleNotFoundError as e:
            # FIXME: unable to distinguish if this error is caused by this
            #        module or by a sub module
            #log.exception("import error")
            pass
    if module is None:
        # try external plugin
        try:
            module = importlib.import_module(name)
        except ModuleNotFoundError as e:
            # FIXME: unable to distinguish if this error is caused by this
            #        module or by a sub module
            raise ValueError("unknown %s plugin: %r (not a native plugin and not a python module)"
                    % (snake_kind, name)) from None

    cls = _classes.get(module.__name__)
    if cls is None:
        raise ValueError("module %s does not provide any plugin" % module.__name__)

    plugin: object = cls(reloader, config, **kw)

    return coerce(kind, plugin)

