#!/usr/bin/python3

import asyncio
import logging
import re
from typing import Optional, Tuple

import aiohttp.web

import ctproxy
from ctproxy import ssl
from . import Listener, CfgDict, plugin

log = logging.getLogger("listener_tcp")

def _parse_socketaddr(txt: str) -> Tuple[str, int]:
    mo = re.match("([0-9.]+):([0-9]+)", txt)
    if not mo:
        raise ValueError("malformatted socket address %r" % txt)
    return mo.group(1), int(mo.group(2))


@plugin
class TcpListener(Listener):
    """A listener pluging that accepts TCP connections

    Sample config:
        # required
        type: tcp
        bind: "1.2.3.4:5678"

        # optional
        tls:
            cert:       "/path/to/the/server/cert.pem"
            key:        "/path/to/the/server/key.pem"
            client_ca:  "/path/to/the/client/ca.pem"
        auth: ...
        policies: ...
    """

    _host: str
    _port: int

    def __init__(self, reloader: ctproxy.ConfigReloader, cfg: CfgDict, **kw) -> None:

        super().__init__(reloader, cfg, **kw)

        self._host, self._port = cfg.get("bind", type=str, required=True,
                cast=_parse_socketaddr)


    async def create_server(self) -> None:
        assert not hasattr(self, "_server")

        self._server = await asyncio.get_event_loop().create_server(
                self._app_handler, self._host, self._port,
                ssl=self._ssl_context)
