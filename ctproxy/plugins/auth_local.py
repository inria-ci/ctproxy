#!/usr/bin/python3

import asyncio
import logging
import re
import os
from typing import Callable, Dict, Optional, Union

from aiohttp.web import Request, StreamResponse

import ctproxy
from . import Auth, User, MangledUser, AdminUser, Namespace, CfgDict, plugin

log = logging.getLogger("auth_local")


@plugin
class LocalAuth(Auth):
    """An auth plugin that stores user certificates in a local directory

    Sample config:
        # required
        type:               local
        user_certs_dir:     "/PATH/TO/THE/USER/CERTS/"

        # optional
        admin:              false

    For each user, the `user_certs_dir` shall contain a `<USERNAME>.crt` file
    containing the user certificate in the PEM format.

    All user certificates are loaded on startup and stored in memory in a hash
    table. They are reloaded on `reloader` events.

    `admin` tells if the users are admin or not.
    - if false (the default), users only have access to their own namespace
      (and mangling is enabled)
    - if true, users have total access
    """

    # True if these users are admins
    _admin: bool

    # directory where the user authentication certificates are stored
    # (stored in PEM format and named 'USERNAME.crt')
    user_certs_dir: str

    # dictionary of user certificates
    # key:   str = certificate (PEM format)
    # value: str = user name
    user_certs: Dict[str, str]

    _listener: ctproxy.ConfigReloader.Listener

    def __init__(self, reloader: ctproxy.ConfigReloader, config: CfgDict) -> None:

        self.user_certs_dir = config.get("user_certs_dir", type=str,
                required=True)
        if not os.path.isdir(self.user_certs_dir):
            raise RuntimeError("user certs dir %r not present" % self.user_certs_dir)


        self._admin      = config.get("admin",      type=bool, default=False)

        self.user_certs = {}

        self._listener = reloader.register(self.reload)


    async def reload(self) -> None:
        self.user_certs.clear()
        for filename in os.listdir(self.user_certs_dir):
            path = os.path.join(self.user_certs_dir, filename)
            user, ext = os.path.splitext(filename)
            if ext == ".crt":
                try:
                    if not re.match(r"[a-z][0-9a-z-]*\Z", user):
                        raise Exception("malformatted user name: %r" % user)
                    with open(path) as fp:
                        self.user_certs[fp.read().strip()] = user
                except Exception as e:
                    log.exception("failed to read user certificate %r (%s)", path, e)
            elif ext == ".key":
                pass
            else:
                log.warning("ignored unexpected file: %r", path)
        log.info("user certificates reloaded (%d users)", len(self.user_certs))

    async def __aenter__(self) -> None:
        await self.reload()
        await self._listener.__aenter__()

    async def __aexit__(self, *k: object) -> None:
        await self._listener.__aexit__(*k)

    async def handle_request(self, ctx: ctproxy.Context) -> Optional[StreamResponse]:
        if ctx.auth_cert:
            name = self.user_certs.get(ctx.auth_cert)
            if name:
                # FIXME: if "." is allowed in user names then, we will have to
                #        mangle them
                assert "." not in name, "not supported"

                if self._admin:
                    ctx.user = AdminUser(name, self.admin_split_prefix)
                else:
                    ctx.user = MangledUser(name, Namespace(name))
        return None
