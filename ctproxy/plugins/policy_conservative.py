#!/usr/bin/python3

import json
import logging
import re
from   types        import MappingProxyType
import typing
from   typing       import Awaitable, Callable, Dict, List, Mapping, Optional, Tuple, Type, Union

from aiohttp.web    import StreamResponse

import ctproxy
from   ctproxy    import ErrorResponse
from   .            import Policy, CfgDict, plugin

T = typing.TypeVar("T")

log = logging.getLogger("policy_conservative")

def ensure(key: object, value: object, expected_type: Type[T]) -> T:
    """Ensure a key-value is well typed provide a default value

    - raise error 400 if `key` is not a string
    - raise error 400 if `value` is not a `expected_type` or None
    - return:
        - `expected_type()` if `value` is None
        - `value` otherwise

    """
    if not isinstance(key, str):
        raise ErrorResponse(400,
                "invalid key name %r, expected a string" % (key,))
    if value is None:
        return expected_type()
    if isinstance(value, expected_type):
        return value

    raise ErrorResponse(400, "invalid type for key %r, expected: %s, got: %s"
                % (key, expected_type.__name__,
                                 value.__class__.__name__))


def get(dct: Dict[object, object], key: object, expected_type: Type[T]) -> T:
    """Get a dict item with type checking + default value

    - raise error 400 if `key` is not a string
    - raise error 400 if the item exists and is not a `expected_type`

    - return:
        - `expected_type()` if the item is absent or None
        - the item otherwise
    """
    return ensure(key, dct.get(key), expected_type)


async def handle_create_request(ctx: ctproxy.Context
        ) -> Optional[StreamResponse]:
    validate(await ctx.json())


async def handle_build_request(ctx: ctproxy.Context
        ) -> Optional[StreamResponse]:

    # list of parameters that do not need to be checked
    ALLOW_OPT = (
            "buildargs",
            "cachefrom",
            "dockerfile",
            "extrahosts",
            "forcerm",
            "labels",
            "nocache",
            "pull",
            "q",
            "remote",
            "rm",
            "squash",
            "t",
            "version",  # undocumented
            )

    # list of parameters that map to a HostConfig option
    HOST_CONFIG_OPT = {
            "memory":       ("Memory", int),
            "memswap":      ("MemorySwap", int),
            "networkmode":  ("NetworkMode", str),
            "cgroupparent": ("CgroupParent", str),
            "cpushares":    ("CpuShares", int),
            "cpusetcpus":   ("CpusetCpus", str),
            "cpusetmems":   ("CpusetMems", str),
            "cpuperiod":    ("CpuPeriod", int),
            "cpuquota":     ("CpuQuota", int),
            "shmsize":      ("ShmSize", int),
            "ulimits":      ("Ulimits", json.loads),
            }

    hc = {}
    for key, val in ctx.params.items():
        log.info("%r %r", key, val)
        if val == "":
            # empty val means parameter not present
            # (the docker client include all params systematically)
            continue

        # host config keys
        hckey, convert = HOST_CONFIG_OPT.get(key, (None, None))
        if hckey is not None:
            assert hckey not in hc
            try:
                hcval = convert(val)
            except ValueError:
                raise ErrorResponse(400,
                        "malformatted build parameter %s=%s" % (key, val))
            hc[hckey] = hcval
            continue

        if key in ALLOW_OPT:
            # allowed keys
            continue

        raise ErrorResponse(403, "build parameter not allowed: %s=%s"
                % (key, val))

    validate_host_config(hc)

    return None


def validate(cfg: Dict[object, object]) -> None:

    #TODO:  check latest version of the API https://docs.docker.com/engine/api/latest/
    #       we may have missing keys

    #FIXME: maybe we should silently ignore (or remove) unknown keys with an empty value

    # list of config keys that do not need to be checked
    ALLOW_OPT = (
            'AttachStderr',
            'AttachStdin',
            'AttachStdout',
            'Cmd',
            'Domainname',
            'Entrypoint',
            'ExposedPorts',
            'Env',
            'Hostname',
            'Labels',
            'NetworkDisabled',
            'OnBuild',
            'OpenStdin',
            'StdinOnce',
            'Tty',
            'User',
            'Volumes',
            'WorkingDir',
            )
    # list of config keys that shall be removed from the config
    REMOVE_OPT = (
            'PortSpecs',    # was present in API <= 1.19
            'name',         # docker plugin >= 1.1 sends it but it is not part of the API
    )

    # set of config keys that are not yet processed
    # (must be empty before forwarding the request)
    keys = set(cfg)
    def local_get(key: object, expected_type: Type[T]) -> T:
        keys.discard(key)
        return get(cfg, key, expected_type)

    # ignore allowed options
    keys.difference_update(ALLOW_OPT)

    # remove options to be removed
    for key in REMOVE_OPT:
        if key in cfg:
            del cfg[key]
    keys.difference_update(REMOVE_OPT)

    # Image: must be a string
    local_get("Image", str)

    # NetworkingConfig
    validate_networking_config(local_get("NetworkingConfig", dict))

    # HostConfig
    validate_host_config(local_get("HostConfig", dict))

    # abort if we have unprocessed keys
    if keys:
        raise ErrorResponse(403, "unknown config keys: %s"
                % (", ".join(map(repr, keys))))


def validate_networking_config(cfg: Dict[object, object]) -> None:
    for key in cfg:
        if key == "EndpointsConfig":
            endpoints = get(cfg, key, dict)
            for epkey in endpoints:
                epname = ensure("EndpointConfig key", epkey, str)
                validate_endpoint_config(epname,
                        get(endpoints, epname, dict))
        else:
            raise ErrorResponse(403, "unknown NetworkingConfig key: %r" % key)


def validate_endpoint_config(name: str, cfg: Dict[object, object]) -> None:

    validate_network_mode(name)
    if name.startswith("container:"):
        raise ErrorResponse(400, "bad endpoint name")

    EMPTY_STRING = ('MacAddress', 'IPv6Gateway', 'Gateway', 'EndpointID',
            'NetworkID', 'GlobalIPv6Address', 'IPAddress')
    ZERO = 'IPPrefixLen', 'GlobalIPv6PrefixLen',
    NONE = 'IPAMConfig', 'Aliases', 'DriverOpts'

    for key, val in cfg.items():
        # These parameters (EMPTY_STRING/ZERO/NONE) must not be set (they
        # must have their default value)
        if (    (val == ""   and key in EMPTY_STRING)
            or  (val == 0    and key in ZERO)
            or  (val is None and key in NONE)):
            pass
        elif key == "Links":
            validate_links(ensure("Links", val, list))
            if val:
                # FIXME: this parameter is not yet mangled by
                #        handle_container_create()
                raise ErrorResponse(400, "not supported: Links inside EndpointConfig")
        else:
            raise ErrorResponse(403, "endpoint parameter not allowed: %s=%r" % (key, val))


def validate_links(links: List[object]) -> None:
    for link in links:
        # we just ensure that links are well formatted
        if not re.match(r"[^:]+(?::[^:]+)?\Z",
                ensure("Links", link, str)):
            raise ErrorResponse(400, "malformatted link: %r" % link)


_MANDATORY_HOST_CONFIG_VALUES: Mapping[object, object] = MappingProxyType({
    'BlkioDeviceReadBps':   [],
    'BlkioDeviceReadIOps':  [],
    'BlkioDeviceWriteBps':  [],
    'BlkioDeviceWriteIOps': [],
    'BlkioWeight': 0,
    'BlkioWeightDevice':    [],
    'CapAdd': [],                   # dangerous
    'Cgroup': '',                   # dangerous
    'CgroupParent': '',             # dangerous
    'ContainerIDFile': '',          # FIXME: is it written on server-side?
    'CpuCount': 0,
    'CpuPercent': 0,
    'CpuPeriod': 0,
    'CpuQuota':  0,
    'CpuRealtimePeriod': 0,
    'CpuRealtimeRuntime': 0,
    'CpuShares': 0,
    'CpusetCpus': '',
    'CpusetMems': '',
    'DeviceCgroupRules': None,
    'Devices': [],                  # dangerous
    'DiskQuota': 0,
    'IOMaximumBandwidth': 0,
    'IOMaximumIOps': 0,
    'IpcMode': '',                  # dangerous
    'Isolation': '',                # dangerous
    'KernelMemory': 0,
    'MaskedPaths': None,            # undocumented
    'LogConfig': {'Type': '', 'Config': {}},
    'Memory': 0,
    'MemoryReservation': 0,
    'MemorySwap': 0,
    'MemorySwappiness': -1,
    'NanoCpus': 0,
    'PidMode': '',                  # dangerous
    'Privileged': False,            # dangerous
    'ReadonlyPaths': None,          # undocumented
    'SecurityOpt': [],              # dangerous
    'ShmSize':   0,
    'UTSMode': '',                  # dangerous
    'UsernsMode': '',               # dangerous
    'VolumeDriver': '',             # dangerous
    })

def validate_host_config(cfg: Dict[object, object]) -> None:

    ALLOW_OPT=(
            'ConsoleSize',
            'DnsSearch',
            'CapDrop',
            'DnsOptions',
            'AutoRemove',
            'ExtraHosts',
            'ReadonlyRootfs',
            'Dns',
            'GroupAdd',
            'OomKillDisable',
            'OomScoreAdj',
            'RestartPolicy',
            'PidsLimit',            # should set a max value
            'Ulimits',              # should set a max value
            'PublishAllPorts',
            )

    for key, val in cfg.items():
        # we check each parameter on a white-list basis
        #
        # valid parameter           => continue
        # invalid/unknown parameter => raise error
        #

        # mandatory values
        expect = _MANDATORY_HOST_CONFIG_VALUES.get(key, ())
        if expect != ():
            if val is None or ensure(key, val, type(expect)) == expect:
                continue

        # ignored values
        elif key in ALLOW_OPT:
            continue

        # binds
        elif key == "Binds":
            validate_binds(ensure(key, val, list))
            continue

        elif key == "NetworkMode":
            validate_network_mode(ensure(key, val, str))
            continue

        elif key == "Links":
            validate_links(ensure(key, val, list))
            continue

        elif key == "VolumesFrom":
            validate_volumes_from(ensure(key, val, list))
            continue

        # published ports
        elif key == "PortBindings":
            validate_port_bindings(ensure(key, val, dict))
            continue

        # parameter is unknown or invalid
        raise ErrorResponse(403, "host config parameter not allowed: %s=%s"
                % (key, val))


def validate_network_mode(val: str) -> None:
    if val == "host":
        raise ErrorResponse(403, "network mode not allowed: %r" % val)


def validate_binds(binds: List[object]) -> None:
    for bind in binds:
        mo = re.match(r"([^:]+):[^:]+(?::[^:]+)?\Z",
                ensure("Binds", bind, str))
        if mo is None:
            raise ErrorResponse(400, "malformatted bind: %r" % bind)
        
        # we allow only named volumes (direct binds of the host filesystem
        # are forbidden)
        vol = mo.group(1)
        if not re.match(r"[a-zA-Z0-9][a-zA-Z0-9_.-]*\Z", vol):
            raise ErrorResponse(403, "volume not allowed: %r" % vol)


def validate_volumes_from( volumes_from: List[object]) -> None:
    for vfrom in volumes_from:
        mo = re.match(r"([^:]+)(?::r[ow])?\Z",
                ensure("VolumesFrom", vfrom, str))
        if mo is None:
            raise ErrorResponse(400, "malformatted VolumesFrom: %r" % vfrom)


def validate_port_bindings(port_bindings: Dict[object, object]) -> None:
    expect = {"HostIp": "", "HostPort": ""}
    for key in port_bindings:
        lst = get(port_bindings, key, list)
        for val in lst:
            if val != expect:
                raise ErrorResponse(403, "invalid port binding: %r->%r (note: all port bindings must be anonymous)" % (key, val))



ALLOW = True
DENY  = False

_POLICY_TABLE: Mapping[Tuple[str, ...],
        Union[bool, Callable[[ctproxy.Context],
                Awaitable[Optional[StreamResponse]]]]
    ] = MappingProxyType({

        # global endpoints
        ("GET", "_ping"):                               ALLOW,
        ("GET", "info"):                                ALLOW,
        ("GET", "version"):                             ALLOW,
        ("POST", "session"):                            DENY,

        # containers endpoints
        ("GET", "containers", "json"):                  ALLOW,
        ("POST", "containers", "create"):               handle_create_request,
        ("POST", "containers", "prune"):                ALLOW,

        # containers endpoints with target
        ("DELETE", "containers", "TARGET"):             ALLOW,
        ("GET", "containers", "TARGET", "archive"):     ALLOW,
        ("GET", "containers", "TARGET", "changes"):     ALLOW,
        ("GET", "containers", "TARGET", "export"):      ALLOW,
        ("GET", "containers", "TARGET", "json"):        ALLOW,
        ("GET", "containers", "TARGET", "logs"):        ALLOW,
        ("GET", "containers", "TARGET", "stats"):       ALLOW,
        ("GET", "containers", "TARGET", "top"):         ALLOW,
        ("HEAD", "containers", "TARGET", "archive"):    ALLOW,
        ("POST", "containers", "TARGET", "attach"):     ALLOW,
        ("POST", "containers", "TARGET", "attach/ws"):  ALLOW,
        ("POST", "containers", "TARGET", "exec"):       ALLOW,
        ("POST", "containers", "TARGET", "exec"):       ALLOW,
        ("POST", "containers", "TARGET", "kill"):       ALLOW,
        ("POST", "containers", "TARGET", "pause"):      ALLOW,
        ("POST", "containers", "TARGET", "resize"):     ALLOW,
        ("POST", "containers", "TARGET", "restart"):    ALLOW,
        ("POST", "containers", "TARGET", "start"):      ALLOW,
        ("POST", "containers", "TARGET", "stop"):       ALLOW,
        ("POST", "containers", "TARGET", "unpause"):    ALLOW,
        ("POST", "containers", "TARGET", "wait"):       ALLOW,
        ("PUT", "containers", "TARGET", "archive"):     ALLOW,

        # exec endpoints
        ("GET", "exec", "TARGET", "json"):              ALLOW,
        ("POST", "exec", "TARGET", "start"):            ALLOW,
        ("POST", "exec", "TARGET", "resize"):           ALLOW,
        
        # images endpoints
        ("GET", "images", "get"):                       ALLOW,
        ("GET", "images", "json"):                      ALLOW,
        ("GET", "images", "search"):                    ALLOW,
        ("POST", "auth"):                               ALLOW,
        ("POST", "build"):                              handle_build_request,
        ("POST", "build/prune"):                        ALLOW,
        ("POST", "commit"):                             ALLOW,
        ("POST", "images", "create"):                   ALLOW,
        ("POST", "images", "load"):                     ALLOW,
        ("POST", "images", "prune"):                    ALLOW,

        # images endpoints with target
        ("DELETE", "images", "TARGET"):                 ALLOW,
        ("GET", "images", "TARGET", "history"):         ALLOW,
        ("GET", "images", "TARGET", "json"):            ALLOW,
        ("GET", "images", "TARGET", "get"):             ALLOW,
        ("POST", "images", "TARGET", "push"):           ALLOW,
        ("POST", "images", "TARGET", "tag"):            ALLOW,

        # volume endpoints
        ("GET", "volumes"):                             ALLOW,
        ("GET", "volumes", "create"):                   DENY, # TODO: implement specific handler
        ("POST", "volumes", "prune"):                   ALLOW,


        # volume endpoints with target
        ("GET", "volumes", "TARGET"):                   ALLOW,
        ("DELETE", "volumes", "TARGET"):                ALLOW,

        #TODO: support networks
        #TODO: support events
        #TODO: other endpoints: swarm|nodes|services|tasks|secrets|plugins|configs|system|distribution
        })

@plugin
class ConservativePolicy(Policy):

    async def handle_request(self, ctx: ctproxy.Context
            ) -> Optional[StreamResponse]:

        policy = _POLICY_TABLE.get(ctx.docker_command)

        if policy is None:
            # we block unknown api endpoints even if the docker engine supports
            # it (this is conservative)
            #
            # NOTE: we return 403 instead of 404 because the docker would
            # interpret a 404 as 'resource not found' and would not display our
            # error message
            raise ErrorResponse(403, "unknown api endpoint: %s %s"
                    % (ctx.request.method, ctx.request.url.path))
        elif isinstance(policy, bool):
            if policy == ALLOW:
                return None
            else:
                raise ErrorResponse(403, "api endpoint not allowed: %s %s"
                        % (ctx.request.method, ctx.request.url.path))
        else:
            return await policy(ctx)

