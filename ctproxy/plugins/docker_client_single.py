#!/usr/bin/python3
import logging
from   typing import Optional, Tuple
from   urllib.parse import urlparse, ParseResult

import aiohttp
from   aiohttp.web  import Response

import ctproxy
from .. import ssl
from . import CfgDict, DockerClient, plugin


log = logging.getLogger("docker_single_client")

@plugin
class SingleDockerClient(DockerClient):
    """Docker client using a single engine
    
    This implementation forwards all HTTP requests to a single docker engine
    (identified by `host` key in the docker config)

    Sample config:
    (plain text)
        type:   single
        host:   "unix:///PATH/TO/THE/DOCKER.SOCK"

    (tls)
        type:   single
        host:   "https://SOME.DOCKER.ENGINE:2376"
        ca:     "/path/to/the/server/ca.pem"
        cert:   "/path/to/the/client/cert.pem"
        key:    "/path/to/the/client/private-key.pem"

    (tls, using "cert path" convention (just like DOCKER_CERT_PATH), the
    directory is expected to contain the client key and certificate named as:
    "cert.key" and "cert.pem" and the server certificate authority as "ca.pem")

        type:       single
        host:       "https://SOME.DOCKER.ENGINE:2376"
        cert_path:  "/path/to/the/certs"
    """

    host: str
    ssl_context: Optional[ssl.SSLContext]
    
    def __init__(self, reloader: ctproxy.ConfigReloader, cfg: CfgDict) -> None:
        # url of the docker host (DOCKER_HOST)
        self.host = cfg.get("host", type=str, required=True)

        # ssl context for securing the communication with the docker engine
        self.ssl_context = self.load_client_ssl_context(cfg)

        log.info("using docker engine at %s", self.host)

    async def __aenter__(self) -> None:
        self.session, self.url = self.make_aiohttp_client(self.host)
        self.parsed_url = urlparse(self.url)
        assert not self.parsed_url.query
        assert not self.parsed_url.params
        assert not self.parsed_url.fragment
        assert not self.parsed_url.path.endswith("/")


    async def __aexit__(self, *k: object) -> None:
        await self.session.close()


    def route_request(self,
            docker_command: Tuple[str, ...],
            docker_target: Optional[str]) -> Tuple[
                    aiohttp.ClientSession, Optional[ssl.SSLContext],
                    ParseResult]:
        return self.session, self.ssl_context, self.parsed_url

