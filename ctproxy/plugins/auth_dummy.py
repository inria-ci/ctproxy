#!/usr/bin/python3

from typing import Optional

from aiohttp.web import Request, StreamResponse

import ctproxy
from . import Auth, User, MangledUser, AdminUser, Namespace, CfgDict, plugin

@plugin
class DummyAuth(Auth):
    """An auth plugin that lets everybody in

    Sample config:
        # required
        type:       dummy
        user:       "USERNAME"

        # optional
        admin:      false
    """

    _user: User

    def __init__(self, reloader: ctproxy.ConfigReloader, config: CfgDict) -> None:

        name = config.get("user", type=str, required=True)
        # FIXME: if "." is allowed in user names then, we will have to mangle
        #        them
        assert "." not in name

        if config.get("admin", type=bool, default=False):
            self._user = AdminUser(name, self.admin_split_prefix)
        else:
            self._user = MangledUser(name, Namespace(name))

    async def handle_request(self, ctx: ctproxy.Context) -> Optional[StreamResponse]:

        ctx.user = self._user

        return None

