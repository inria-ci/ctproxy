#!/usr/bin/python3

from typing import Optional

from aiohttp.web import Request, StreamResponse

import ctproxy
from . import auth_dummy, plugin, User, AdminUser, MangledUser


@plugin
class ValidCertAuth(auth_dummy.DummyAuth):
    """An auth plugin that ensures the client used a valid X.509 to open the
    TLS session

    Sample config:
        # required
        type:       valid_cert
        user:       "USERNAME"

        # optional
        admin:      false
    """
    async def handle_request(self, ctx: ctproxy.Context) -> Optional[StreamResponse]:
        if ctx.auth_cert is None:
            return None
        return await super().handle_request(ctx)

