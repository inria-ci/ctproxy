#!/usr/bin/python3

import asyncio
import logging
import re
import os
from typing import Callable, Dict, Optional, Union

from aiohttp.web import Request, StreamResponse

import ctproxy
from   ctproxy    import ErrorResponse
from . import Auth, User, MangledUser, AdminUser, Namespace, CfgDict, plugin
from .listener_tmp import TmpListener

log = logging.getLogger("auth_local")

_ALLOW_WHEN_CLOSING = set((
        # global endpoints
        ("GET", "_ping"),
        ("GET", "info"),
        ("GET", "version"),

        # list resources
        ("GET", "containers", "json"),
        ("GET", "images",     "json"),
        ("GET", "volumes"),

        # delete resources
        ("DELETE", "containers", "TARGET"),
        ("DELETE", "images",     "TARGET"),
        ("DELETE", "volumes",    "TARGET"),
))

@plugin
class TmpListenerAuth(Auth):
    """Authentication plugin for the tmp listeners

    This authentication plugin is to be used together with the `tmp` listener.
    
    The user is authenticated according to socket on which the request is
    received.

    NOTE: this plugin is very experiental
    """

    async def handle_request(self, ctx: ctproxy.Context) -> Optional[StreamResponse]:
        # FIXME: is there a way to falsify the sockname ?
        listener_id = os.path.basename(
                ctx.request.transport.get_extra_info("sockname"))
        listener_info = TmpListener.get_user(listener_id)
        if listener_info is not None:
            ctx.user, closing = listener_info
            if closing and ctx.docker_command not in _ALLOW_WHEN_CLOSING:
                # we do not allow creating new resources when the listener is
                # closing (so that we can guarantee that all resources are
                # destroyed when the listener is closed)
                raise ErrorResponse(403, "listener is closing")
        return None
