#!/usr/bin/python3

import asyncio
import logging
import secrets
import subprocess
import time
from   typing   import Dict, Optional, Tuple, Type, TypeVar

log = logging.getLogger("utils")

T = TypeVar('T')


def coerce(type: Type[T], value: object) -> T:
    if isinstance(value, type):
        return value
    raise TypeError("expect: %r, got %r" % (type.__name__, value.__class__.__name__))


def split_tag(image: str) -> Tuple[str, Optional[str]]:
    """Remove the tag part form an image name if present
    
    examples:
        "FOO"                   -> ("FOO", None)
        "FOO:TAG"               -> ("FOO", "TAG")
        "REPO:PORT/FOO/BAR"     -> ("REPO:PORT/FOO/BAR", None)
        "REPO:PORT/FOO/BAR:TAG" -> ("REPO:PORT/FOO/BAR", "TAG")
    """
    return re.match(r"(.*?)(?::([^:/]*))?\Z", image).groups()   # type: ignore


def try_set_result(fut: "asyncio.Future[T]", result: T) -> bool:
    """Try to set the result of a future

    Returns False if `fut` is already done. Otherwise set the result and return
    True.
    """
    if fut.done():
        return False
    else:
        fut.set_result(result)
        return True


class Subprocess:
    """Wrap asyncio.create_subprocess_exec() inside an async context manager

    Usage:
        async with Subprocess(cmd, ...) as proc:
            ...

    __aenter__ starts the subprocess

    __aexit__ 
      on normal exit (no exception): waits until the process exits then raise
      subprocess.CalledProcessError if the returncode is non-zero

      finally (w/ or w/o exception): sends a SIGKILL to the process (in case it
      is still running)
    """

    # process command and extra parameters
    cmd: Tuple[str, ...]
    kw: Dict[str, object]

    # process
    proc: Optional[asyncio.subprocess.Process]

    def __init__(self, *cmd: str, **kw) -> None:
        self.cmd = cmd
        self.kw  = kw
        self.proc = None

    def __getattr__(self, name: str) -> object:
        """proxy to the `asyncio.subprocess.Process` attributes"""
        return getattr(self.proc, name)

    async def __aenter__(self) -> "Subprocess":
        assert self.proc is None
        self.proc = await asyncio.create_subprocess_exec(*self.cmd, **self.kw)
        return self

    async def __aexit__(self, exc_type: type, exc_val: BaseException, tb: object) -> None:
        if self.proc is None:
            return None

        try:
            if exc_type is None:
                # normal exit -> wait for process termination
                code = await self.proc.wait()
                if code != 0:
                    raise subprocess.CalledProcessError(code, self.cmd)
        finally:
            # wipe the process if not yet terminated
            if self.proc.returncode is None:
                try:
                    self.proc.kill()
                except OSError:
                    pass

class RateLimiter:
    """Async iterator for limiting frequency of events
    
    This rate limiter is configured with a `period` in seconds which is the
    minimal delay allowed between two successive iterations.

    When iterating, the rate limiter checks the time elapsed since the last
    iteration. If it is lower than `period` then it will wait until this period
    is observed. The iterator always return None.

    The primary purpose is to space out the reconnections attempts when a
    remote server is down, eg:

    for _ in RateLimiter(60):
        try:
            connect_to_server()
            do_something()
            ...
        except ...:
            ...
    """
    def __init__(self, period: float):
        self._period = period
        self._timestamp = time.monotonic() - period

    def __aiter__(self) -> "RateLimiter":
        return self

    async def __anext__(self) -> None:
        now = time.monotonic()
        delay = self._period - (now - self._timestamp)
        if delay <= 0:
            self._timestamp = now
            log.debug("rate limiter nowait (delay = %s)", delay)
        else:
            log.debug("rate limiter wait %s seconds", delay)
            await asyncio.sleep(delay)
            log.debug("rate limiter done")
            self._timestamp = time.monotonic()
        return None

def random_token(length: int):
    """Generate a secure random token with alphanumeric characters"""
    return "".join(secrets.choice(
        '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
        ) for _ in range(length))

def parse_bool(val: str) -> bool:
    """Parse a boolean value as provided in the docker api"""

    c = val[:1].upper() or "F"
    if c == "F":
        return False
    elif c == "T":
        return True
    else:
        return bool(int(val))

