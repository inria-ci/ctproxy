#!/usr/bin/python3

import collections
import re
import shlex
from typing import Dict, Generator, List, Mapping, Match, NamedTuple, Optional, Tuple, Union

WHITESPACE = " \t"

#TODO: args starting with a number must be ignored

ARG_EXPANSION_PATTERN=r"\$([A-Za-z0-9_]+)|\$\{([^}:]*)(?::([+-])([^}]*))?\}"
def _replace_expanded_arg(mo: Match[str], args: Dict[str, str]) -> str:
    key1, key2, modifier, replacement = mo.groups()
    key = key1 or key2
    val = args.get(key, "")
    if modifier is None:
        return val
    elif modifier=="+":
        return replacement if val else ""
    elif modifier=="-":
        return val or replacement
    else:
        raise Exception("unreachable") # pragma: nocover

class DockerfileCommand(NamedTuple):
    name: str
    args: Tuple[str, ...]
    scope: int
    start_line: int
    end_line: int

class ParseError(Exception):
    args: Tuple[int, str]
    def __init__(self, lineno: Union[int, DockerfileCommand], msg: str) -> None:
        super().__init__(
                (lineno if isinstance(lineno, int) else lineno.end_line-1),
                msg)
    def __str__(self) -> str:
        lineno, msg = self.args
        return "line %d: %s" % (lineno+1, msg)

class DockerfileParser:
    # current args for each scope
    # (0 -> global scope, 1 -> stage_1 scope, ... , n -> stage_n scope)
    args: List[Dict[str, str]]

    # escape character "\\" or "`"
    escape: str

    # build-time arguments provided by the user
    buildtime_args: Mapping[str, str]

    # parser output
    commands: List[DockerfileCommand]

    def __init__(self, lines: List[str], args: Mapping[str, str]) -> None:
        self.lines = lines
        self.buildtime_args = args
        self.args = [{}]
        self.commands = []

        # read the escape directive (if any)
        mo = re.match(r"\s*#\s*escape\s*=\s*(\S)", lines[0] if lines else "")
        self.escape = mo.group(1) if mo else "\\"
        if self.escape not in ("\\", "`"):
            raise ParseError(0, "invalid ESCAPE %r. Must be ` or \\" % self.escape)

        scope = 0
        for line in self.line_reader(lines):
            cmd = self.parse_command(*line, scope)
            if cmd is not None:
                if cmd.name == "FROM":
                    scope += 1
                    self.args.append({})
                elif cmd.name == "ARG":
                    if len(cmd.args) != 1:
                        raise ParseError(cmd, "ARG requires exactly one argument")
                    key, *val = cmd.args[0].split("=", 1)
                    self.args[scope][key] = self.buildtime_args.get(key,
                            (val[0] if val else ""))

                self.commands.append(cmd)
    
    def line_reader(self, lines: List[str]) -> Generator[
            Tuple[int, int, str], None, None]:

        result = []
        start_line = 0
        for i, line in enumerate(lines):
            mo = re.match(r"(\s*(?:[^\s#].*)?)([\\`])[ \t]*\Z", line)
            if mo and mo.group(2) == self.escape:
                result.append(mo.group(1))
            else:
                result.append(line)
                yield start_line, i+1, "".join(result)
                result.clear()
                start_line = i+1
        if result:
            yield start_line, i+1, "".join(result)

    def parse_command(self, start_line: int, end_line: int, line: str,
            scope: int) -> Optional[DockerfileCommand]:
        mo = re.match("\s*(?:#|$|(\S+))", line)
        assert mo is not None
        name = mo.group(1)
        if name is None:
            # empty/comment line
            return None
        name = name.upper()
        if name == "FROM":
            scope = 0
        elif name == "ARG":
            pass
        else:
            # other commands not supported
            return None
        i = mo.end(1)

        arg_list = []
        current: List[str] = []
        def flush() -> None:
            if current:
                arg_list.append("".join(current))
                current.clear()
        while i < len(line):
            c = line[i]
            if c in ("'", '"'):
                end = line.index(c, i+1)
                item = line[i+1:end]
                if c == '"':
                    item = self.expand_args(item, scope)
                current.append(item)
                i = end+1
            elif c in WHITESPACE:
                flush()
                i += 1
            else:
                item, i = self.parse_token(line, i, scope)
                current.append(item)
        flush()
        return DockerfileCommand(name, tuple(arg_list), scope, start_line, end_line)

    def expand_args(self, item: str, scope: int) -> str:
        return re.sub(ARG_EXPANSION_PATTERN,
                lambda mo: _replace_expanded_arg(mo, self.args[scope]), item)

    def parse_token(self, line: str, index: int, scope: int) -> Tuple[str, int]:
        lst = []
        i = index
        while i < len(line):
            c = line[i]
            if c in WHITESPACE or c in "\"'":
                break
            elif c == self.escape:
                try:
                    lst.append(line[i+1])
                except IndexError:
                    pass
                i+=2
            else:
                if c == "$":
                    mo = re.match(ARG_EXPANSION_PATTERN, line[i:])
                    if mo is not None:
                        lst.append(_replace_expanded_arg(mo, self.args[scope]))
                        i+=len(mo.group(0))
                        continue

                lst.append(c)
                i+=1
        return "".join(lst), i

