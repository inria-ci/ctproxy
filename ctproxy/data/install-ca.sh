#!/bin/sh

set -e
#set -x

die()
{
	echo "error: $*" >&2
	exit 1
}

patch_ca_certificates()
{
	echo "patch ca-certificates"
	mkdir -p /usr/local/share/ca-certificates/
	install -m 0644 /tmp/ca/ctproxy-ca.crt /usr/local/share/ca-certificates/

	# rebuild the ca bundle (if ca-certificates is installed)
	if which update-ca-certificates > /dev/null; then
		# '--fresh' is needed because the ca-certificates-java does not update
		# the keystore if a certificate is updated (it just adds new
		# certificates)
		update-ca-certificates --fresh
	fi
}

patch_ca_certificates_fedora()
{
	echo "patch ca-certificates fedora"

	mkdir -p /etc/pki/ca-trust/source/anchors/
	install -m 0644 /tmp/ca/ctproxy-ca.crt /etc/pki/ca-trust/source/anchors/

	# rebuild the ca bundle (if ca-certificates is installed)
	if [ -f /usr/bin/update-ca-trust ] ; then
		/usr/bin/update-ca-trust
	fi
}

patch_pip_bundles()
{
	for bundle in 									\
		/usr/local/lib/python*/*-packages/pip/_vendor/certifi/cacert.pem	\
		/opt/python*/lib/python*/*-packages/pip/_vendor/certifi/cacert.pem	
	do
		[ ! -f "$bundle" ] || patch_bundle "$bundle"
	done
}

patch_bundle()
{
	echo "patch bundle $1"
	local bundle="$1"
	local dn="`dirname "$bundle"`"
	local bn="`basename "$bundle"`"
	local orig="$dn/.ctproxy-cache-orig.$bn"

	[   -f "$bundle" ] || die "file not found: $bundle"
	[ ! -L "$bundle" ] || die "file is a symbolic link: $bundle"
	

	if [ ! -f "$orig" ] ; then
		cp -a -- "$bundle" "$orig"
	fi

	cat -- "$orig" /tmp/ca/ctproxy-ca.crt > "$bundle"
}


if [ -f "/etc/debian_version" ]
then
	patch_ca_certificates

elif [ -f "/etc/alpine-release" ]
then
	# In alpine packages providing an https-client do not depend on 'ca-certificates', we need to force its installation.
	apk add ca-certificates

	patch_ca_certificates

elif [ -f "/etc/fedora-release" ] || [ -f /etc/redhat-release ]
then
	patch_ca_certificates_fedora

else
	# unsupported image
	echo "IMAGE NOT SUPPORTED"
	exit 242
fi

patch_pip_bundles

rm -f /tmp/ca/ctproxy-ca.crt
rm -f /tmp/ca/install-ca.sh
rmdir /tmp/ca  2>/dev/null || true
