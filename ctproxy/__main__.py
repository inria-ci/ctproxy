#!/usr/bin/env python3

import argparse
import asyncio
import logging
import os
import re
import signal
import typing

import docker
from . import CTproxy

import config_reader

DEFAULT_CONFIG="/etc/ctproxy/config.yml"


parser = argparse.ArgumentParser("ctproxy", "Docker ctproxy")
parser.add_argument("-c", "--config", metavar="FILENAME", default=DEFAULT_CONFIG,
        help="path to the config file (default: %s)" % DEFAULT_CONFIG)
parser.add_argument("-d", "--debug", action="count", default=0,
        help="increase verbosity (may be used multiple times)")
parser.add_argument("-q", "--quiet", action="store_true",
        help="decrease verbosity")

args = parser.parse_args()

# debug level may be configured either with '-d' or with the env var CTPROXY_DEBUG
# we use the highest value
try:
    debug = max(args.debug, int(os.environ.get("CTPROXY_DEBUG", "0")))
except ValueError:
    debug = args.debug

if debug:
    level = "DEBUG"
    if debug == 1:
        logging.getLogger("aio_gnutls_transport").setLevel("INFO")
elif args.quiet:
    level = "WARNING"
else:
    level = "INFO"

logging.basicConfig(level=level, format="%(asctime)s%(levelname)8s [%(process)d]:%(name)s:%(message)s")

loop = asyncio.get_event_loop()

with    open(args.config) as fp,   \
        config_reader.ConfigReader(fp) as cfg:
    ctproxy = CTproxy(cfg)

try:
    loop.add_signal_handler(signal.SIGINT,  ctproxy.shutdown)
    loop.add_signal_handler(signal.SIGTERM, ctproxy.shutdown)
    loop.add_signal_handler(signal.SIGHUP,  ctproxy.reload)

    loop.run_until_complete(ctproxy.run())
#    import cProfile
#    cProfile.run('loop.run_until_complete(ctproxy.run())', sort="tottime")
finally:
    loop.remove_signal_handler(signal.SIGINT)
    loop.remove_signal_handler(signal.SIGTERM)
    loop.remove_signal_handler(signal.SIGHUP)


