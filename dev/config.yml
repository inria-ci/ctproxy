---
# sample configuration file for ctproxy
#
#

###############
#  Listeners  #
###############
#
# This section lists the sockets on which CTproxy accepts incoming Docker
# API requests.
#
# Listeners are implemented as plugins. Documentation about each listener
# can be found in its respective module
# (ctproxy/plugins/listener_{TYPE}.py).
#
# Common config keys (valid for any listener type):
#   type:   plugin type
#   tls:    TLS configuration
#   auth:   authentication sources
#
#
#   policies:   list of policies specific to this listener
#
# Specific config keys:
# - tcp listener:
#       bind:   socket address to be bound (IP:PORT)
# - unix listener
#       bind:           path to the unix socket to be bound
#       allow_user:     uid of the socket (default 0)
#       allow_group:    gid of the socket (default 0)
#       allow_other:    allow any user to connect to this socket
#

############################
#  Authentication sources  #
############################
#
# Each listener may liste zero or more authentication source
# - if the 'auth' key is not present, then authentication is disabled. All
#   requests are accepted.
# - if present, requests must be authenticated with at least source (which are
#   tried in the same order as listed in the configuration)
#
# Authentication sources Listeners are implemented as plugins. Documentation
# about each auth source can be found in its respective module
# (ctproxy/plugins/auth_{TYPE}.py).
# 
# Common config keys (valid for any auth type):
#   type:   plugin type
#   admin:  boolean indicating if the users authenticated through this
#           source have access to the root namespace (this key has no
#           effects if namespaces are disabled)
#
# Specific auth keys:
#
# - local auth plugin (checks client certificate against certificates
#   listed in a local folder)
#
#       user_certs_dir: path to the directory storing the known client
#                       certificates
#
# - dummy auth (accepts all requests)
#       user:       name of the authenticated user
# 
#
# - valid_cert auth (checks that the client certificate is valid, but
#   does not identify each client certtificate -> the returns the same
#   username for every valid cert)
#
#       user:       name of the authenticated user

######################
#  Policies section  #
######################
#
# Policies are arbtrary plugins that are invoqued after a request is
# successfully authenticated, and before applying the namespaces/cache
# features.
#
# Policies may be defined globally or on a per-listener basis:
# - global policies are listed in the root 'policies' config key
# - listener-specific policies are listed in the 'policies' key of the
#   relevant listener
# 
# Incoming requests are processed through the global policies first, then
# through the listener-specific policies.
#
# Policies are implemented as plugins. Documentation about each policy can
# be found in its respective module (ctproxy/plugins/policy_{TYPE}.py).
#

################
#  Namespaecs  #
################
#
# When namespaces are enabled, all non-admin user are isolated within their
# own namespace, and cannot see/use the resources of the other users.
#
# Currently, the namespace is named after the user.
#
# Since images names contains the location of the hosting registry,
# push/pull operations require CTproxy to implement the Docker Registry
# API. CTproxy mangles the names so that the same name used by two
# different users will resolve to two different names in the engine.
#
# The namespaces section contains 3 config keys:
#   base_registry:  base registry name (eg: ci-registry.localhost)
#                   -> image names will be mangled as:
#                      XXXXXXXXXXXXXXXXX.ci-registry.localhost
#   bind:           socket address to be listened  for registry requests
#   tls:            TLS certificate+key for the registry. This must be a
#                   wildcard certificate for the 'base_registry' sub
#                   domains (eg: *.ci-registry.localhost)
#
# Note: namespaces are not compatible with the transparent cache

#######################
#  Transparent cache  #
#######################
#
# The 'cache' section configures the transparent cache.
#
# Config keys:
#   proxy_host      hostname of the transparent cache server
#
# CTproxy gets the details of the proxy configuration from the proxy
# itself. The proxy is expected to serve the following urls:
#   - https://{proxy_host}/cache/ca.crt
#     -> current CA certificate to be installed inside the containers (to
#        allow caching HTTPS traffic)
#   - https://{proxy_host/cache/domains.txt
#     -> current list of cached domains
#
# The CA certificate used for proxied traffic is useful only as long as the
# containers using them are running. It is strongly advised to create this
# CA with a very short lifetime and to rotate automatically (so as to avoid
# possible security issues if an image with this CA installed is pushed to
# a registry).
#
# CTproxy polls the two urls periodically (every hour) or on SIGHUP and it
# dynamically applies any config change.
#
# Note: the cache is not compatible with the namespaces

###################
#  Docker client  #
###################
#
# The 'docker_client section' configures where the requests received on the
# listeners are forwarded.
#
# Currently there is only one plugin implementation 'single', which
# forwards all requests to the same Docker engine.
#
#


listeners:
  # TCP listener on 0.0.0.0:2376
  # - with TLS is enabled, including client certificate validation
  # - with user authentification against certificates listed in
  #   /etc/ctproxy/certs/users
  - type: tcp
    bind: "0.0.0.0:2376"
    tls:
        cert:       "/etc/ctproxy/certs/server.crt"       
        key:        "/etc/ctproxy/certs/server.key"
        client_ca:  "/etc/ctproxy/certs/users-ca.crt"
    auth:
      - type: local
        user_certs_dir: "/etc/ctproxy/certs/users"

  # dummy listeners for debugging purpose
  # - listen on unix socket '/opt/src/ctproxy.sock'
  # - dummy authentication (all requests accepted under the 'admin' user)
  - type: unix
    bind: "/opt/src/ctproxy.sock"
    #allow_group:    "adm"
    auth:
      - type: dummy
        user: "admin"
        admin: true
  - type: tcp
    bind: "0.0.0.0:2377"
    auth:
      - type: dummy
        user: "admin"
        admin: true

# Apply the 'conservative' policy to requests received on all listeners
policies:
  - type: conservative

#namespaces:
#    base_registry:      "ci-registry.localhost"
#    bind:               "0.0.0.0:4443"
#    tls:
#        cert:   "/etc/ctproxy/certs/registry.crt"
#        key:    "/etc/ctproxy/certs/registry.key"

cache:
    proxy_host: "dockerhost"

docker_client:
        type:       single
        # note: standard config is over TLS on port 2400
        # (we may use port 2399 for debugging purpose)
        #host:       "http://docker:2399"
        host:       "https://docker:2400"
        cert_path:  "/etc/ctproxy/certs/docker"


# vim:et:nosta:sw=4:sts=4:
