CTproxy - Container Tuning Proxy
================================

This repository contains the sources of CTproxy, a HTTP reverse-proxy that
understands the Docker Engine API and presented at
JRES 2019 ([read the article](https://conf-ng.jres.org/2019/document_revision_5451.html?download)).

It is expected to be used in front of a docker engine (or possibly multiple of
docker engines) to alter/tune the requests sent by the client.

Note: experimental software
===========================

For me moment this tool shall be considered experimental, we may break the API at any time and we do not guarantee any support. 

License
=======

GNU AGPL3+

Features
========

* **Multiple listeners**

  It can be configured to listen on multiple sockets, each listener may have a
  different configuration.

* **TLS**
 
  Listeners can be secured with TLS, with optional validation of the client certificate.

* **Authentication**
  
  Client authentication may rely either on the HTTP Authorization, TLS client
  certificate or remote user id (for unix sockets).
  Authentication modules are implemented as plugins. Thus any authentication
  source can be supported by implementing the appropriate plugin.


* **Policy enforcement**
  
  Policies are implemented as plugins, and a listener may be configured with 0 or
  more policies. Policy plugins can be used for two purposes:
    - implementing access control;
    - injecting additional parameters in Docker commands.

  The tool comes with a built-in policy named 'conservative' which blocks all
  Docker commands or container options that elevate privileges (eg: --privileged, --
  cap-add, ...).


* **Transparent caching**
  
  The transparent cache feature, when enabled, alters the configuration of containers
  created through the Docker API. A selection of domains to be cached are
  redirected towards a local HTTP caching proxy (using docker's --add-host option)
  and a custom certificate authority is installed inside the container to allow caching
  the HTTPS traffic as well. The custom CA is generated with a short lifetime and
  rotated automatically.
  This feature is intended to be used with bandwidth hungry sites. In our continuous
  integration context, these will be the most popular package repositories (eg:
  Debian, Fedora, Maven, ...).

* **Namespaces**

  When namespaces are enabled, all resources names (containers, images,
  volumes, ...) are mangled to provide isolation between users.
  On the implementation side most resource names (containers, volumes, ...) are just
  prefixed with the namespace, whereas the mangled images use a more complex
  format to allow push/pull operations (the registry API requests go through another
  reverse proxy which demangles the name and forward them to the original
  registry).

* **Temporary listener** (very experimental)

  The purpose of temporary listeners is to provide a limited access to the Docker
  API from inside a container running a GitLab-CI job. This allows users to build
  and push Docker images without having to provide their own dedicated runner.
  When a container is created, a new unix socket is created and mounted at /var/run/
  docker.sock inside the container and this socket is associated with a temporary
  namespace. When the container terminates, all resources created through this
  listener are destroyed.


Getting started
===============

CTproxy is not properly packaged yet, but the repository provides a `docker-compose` file that deploys a development environment a test_suite with 70% coverage.

To set up the development environment:
1. run the bootstrap script
   ```sh
   ./bootstrap
   ```
2. if needed, you may edit `.env` file
3. deploy the containers
   ```sh
   docker compose up
   ```
4. send your first request through the proxy:
   ```sh
   docker -H unix://ctproxy.sock info
   ```

The sample configuration for the development environment is in `dev/config.yml`
and is self-documented.

You may also run the test suite:
```sh
./run_tests
```
